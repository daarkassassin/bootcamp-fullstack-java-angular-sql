
console.log("aaa " + comprovarTexto("aaa"));
console.log("Aaa " + comprovarTexto("Aaa"));
console.log("AAA " + comprovarTexto("AAA"));

function comprovarTexto(texto) {    
    if (texto == texto.toUpperCase()) {
        return "La cadena de texto contiene solo mayusculas";
    } else if (texto == texto.toLowerCase()) {
        return "La cadena de texto contiene solo minisculas";
    } else {
        return "La cadena de texto contiene mayusculas y minisculas";
    }
}