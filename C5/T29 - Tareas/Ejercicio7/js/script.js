console.log(calcFactorial1(-10));
console.log(calcFactorial1(0));
console.log(calcFactorial1(5));
console.log(calcFactorial2(7));

function calcFactorial1 (numero) {
    // Si numero < 0 return -1
    if (numero < 0) {
        return -1;
    }
    // Si numero == 0 return 1
    else if (numero == 0) {
        return 1;
    }
    // Si no se cumple ninguna de las condiciones anteriores calcular el factorial
    else {
        return (numero * calcFactorial1(numero - 1));
    }    
}

function calcFactorial2 (numero) {
    let r = 1;
    for(let i = numero; i>0; i--){
        r *= i;
    }
    return r;
}