window.onload = function(){
    var letras = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];

    var button = document.getElementById('enviar').onclick = msg;

    function msg() {
        var entrada = document.getElementById('txt').value;

        var dniNumero =  entrada.slice(0, -1);
        var dniLetra = entrada.slice(-1);
        dniLetra = dniLetra.toUpperCase();
        var testLetra = "";

        if(!isNaN(dniNumero)){
            if(isNaN(dniLetra)){
                if(dniNumero > 0 && dniNumero < 99999999 && dniNumero.length == 8){
                    var pos = calcular(dniNumero);
                    testLetra = ""+letras[pos];
                    if (testLetra == dniLetra){
                        alert('El numero y la letra son correctos!');
                    }else{
                        alert('La letra no es correcta');
                    }
                }else{
                    alert('Hay que introducir un numero de 8 digitos valido + Letra');
                }
            }else{
                alert('Hay que introducir una letra');
            }
        }else{
            alert('Hay que introducir un numero de 8 digitos valido');
        }

    }

    function calcular(entrada){
        var x = 0;
        x = entrada % 23;
        return x;
    }
}

