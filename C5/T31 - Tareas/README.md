![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD31 – Jquery

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| David Bonet Daga | Master | Team Member | 18/03/2021 |   |   |  |
| Xavier Bonet Daga |  | Team Member | 18/03/2021 |   |   |  |


#### 2. Description
```
Ejercicios UD31 – Jquery
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql

```
UD31 – Jquery / https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
visual studio code - https://code.visualstudio.com/
--------
Plugins:
vscode-icons
Beautify
markdown-preview
live server
--------
chrome - https://www.google.com/intl/es_es/chrome/
```
