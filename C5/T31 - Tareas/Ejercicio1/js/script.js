var x = ""; //número en pantalla
var y = ""; //guardar el numero que hay en la x al introducir un operador
var operacion = "no"; //operacion en curso, si 'no' significa que no hay operacion en curso.
var coma = false; //false = no hay coma, true si hay coma.

// Introducir un numero
function introducirNumero (numero) {
    if (x == "") {
        x = numero;
        $("#resultado").text(x);
    } else {
        x += numero;
        $("#resultado").text(x);
    }
}

// Introducir el operador
function operar(s) {
    igualar();
    y = x;
    $("#resultado").text("");
    x = "";
    operacion = s;
    coma = false;
}

// Realizar la operacion
function igualar() {
    if (operacion == "no") {
        //no ocurre nada.
    }else { 
        operacion = y+operacion+x;
        solucion = eval(operacion);
        $("#resultado").text(solucion);
        x = solucion;
        operacion = "no";
    }
}

$(document).ready(function(){
    // Borrar la ultima cifra o coma introducida
    $("#retr").click(function(){
        cifras = x.length;
        ultimaCifra = x.substr(cifras-1,cifras)
        x = x.substr(0,cifras-1)
        if (x == "") {
            x = "0";
        }
        if (ultimaCifra == ".") {
            coma = false;
        }
        $("#resultado").text(x); 
    });

    // Borrar la ultima entrada
    $("#CE").click(function(){
        $("#resultado").text("");
        x = "";
        coma = false;
    });

    // Borrar todo
    $("#C").click(function(){
        $("#resultado").text("");
        x = "";
        y = "";
        coma = false;
        operacion="no" 
    });

    // Introducir una coma
    $("#coma").click(function(){
        if (x == "") {
            coma = true;
            x = "0.";
            $("#resultado").text(x);
        } else if (coma == false) {
            coma = true;
            x += ".";
            $("#resultado").text(x);
        }
    });

    // Calcular la raiz
    $("#raizc").click(function(){
        x = Math.sqrt(x);
        $("#resultado").text(x);
        operacion="no";
    });
    
    // Calcular porcentaje
    $("#porcent").click(function(){
        x = x/100;
        $("#resultado").text(x);
        igualar();
    });
    
    // Poner un numero en negativo o positivo
    $("#opuest").click(function(){
        var nx = Number(x);
        nx=-nx;
        x=String(nx);
        $("#resultado").text(x);
    });
    
    // Calcular 1/x
    $("#inve").click(function(){
        var nx=Number(x);
        nx = (1/nx);
        x = String(nx);		 
        $("#resultado").text(x);
    });
});