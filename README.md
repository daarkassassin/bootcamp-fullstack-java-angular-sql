![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)
# Proyectos - FullStack Java Angular SQL

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| David B. | Master | Project Manager | 08/02/2021 | all  |   |   |
| Xavi B. | :---: | Dev | 10/02/2021 | Team3  |   |   |

#### 2. Description
```
Este repositorio contiene todos los ejercicios realizados durante el curso |FullStack Java Angular SQL| 
Está dividido en ramas C0, C1, C2, C3, C4 y C5. 

```
#### 3. Link a un demo con el proyecto desplegado: https://github.com/
```
* Nombre de la App: [GITTT] (https://github.com/)
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```

```
###### Command line 
```

```

#### 5. Screenshot imagen que indique cómo debe verse el proyecto.
![]()
    
----

### Lista de contenidos

+ C0 - PDF's / **Main** - *Todos los PDF's de teoria y ejercicios del curso*
	+ Tareas - Todos los pdf's de las tareas que se han realizado en este repositorio.
	+ Teoria - Todos los pdf's de teoria + videos.
+ C1 / **Main** - *Introducción a la Programación i al diseño de Software.*
	+ TA1 - Introducción a JAVA.
	+ TA2 - JAVA IDE
	+ TA3 - GIT
+ C2 / **Main** -  *JAVA Basics.*
	+ TA4 - JAVA Basics
	+ TA5 - Flujo de datos
	+ TA6 - Métodos y Arrays
	+ TA7 - Arraylist y Hashtable
	+ TA8 - POO en Java
	+ TA9 - Herència en Java *Ej extra: M1, M2, M3*
	+ TA10 - Java Exceptions
	+ TA18 - Conexion JAVA_MYSQL
	+ TA19 - Swing AWT
+ C3 / **Main** - *Bases de Datos (MySQL)*
	+ TA11 - Introducción a BD
	+ TA12 - Diagramas ER
	+ TA13 - Modelo Relacional
	+ TA14 - DDL
	+ TA15 - DML1
	+ TA16 - DML2
	+ TA17 - MongoDB
+ C4 / **Main** - *Desarrollo Backend*
	+ TA20 - Maven
	+ TA21 - Junit
	+ TA22 - patron MVC
	+ TA23 - SPRING Basics
	+ TA24 - SPRING REST
	+ TA25 - SPRING REST ER
	+ TA26 - SPRING REST MySQL(n/n)
+ C5 / **Main** - *Desarrollo Frontend*
	+ TA27 - Introducción a HTML/CSS
	+ TA28 - Tablas_DIV
	+ TA29 - JavaScript
	+ TA30 - JavaScript (II)
	+ TA31 - JQuery
	+ TA32 - BOOTSTRAP
	+ TA33 - Frontal_AJAX


----

### End
