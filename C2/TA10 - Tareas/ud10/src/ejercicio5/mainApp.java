package ejercicio5;

import javax.swing.JOptionPane;

/**
 * @author David
 *
 */
public class mainApp {
	
	public static void main(String[] args) {
		try {
			
			int num1 = Integer.parseInt(JOptionPane.showInputDialog("Longitud de la lista de contraseñas: "));
			int num2 = Integer.parseInt(JOptionPane.showInputDialog("Longitud de las contraseñas: "));
	
			Password listaPasswords[] = new Password[num1];
			boolean contraseaFuerte[] = new boolean[num1];
			
			for(int i = 0; i < listaPasswords.length; i++) {
				listaPasswords[i] = new Password(num2);
				contraseaFuerte[i] = listaPasswords[i].esFuerte();
				System.out.println("Contraseña: " + listaPasswords[i].getContraseña() + " | Es fuerte: " + listaPasswords[i].esFuerte());
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}


	}

}
