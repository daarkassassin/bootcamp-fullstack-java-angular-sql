package ejercicio5;

import java.util.Random;

class Password {
	///CONSTANTES
	private final int FLONGITUD = 8;

	///ATRIBUTOS
	private String contraseña;
	private int longitud;

	//METODOS CONSTRUCTOR
	public Password() {
		setLongitud(FLONGITUD);
		setContraseña(null);
	}

	public Password(int longitud) {
		setLongitud(longitud);
		setContraseña(generarContrasea(longitud));
	}

	/*
	 *  METODOS SET
	 */
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	/*
	 *  METODOS GET
	 */
	public String getContraseña() {
		return contraseña;
	}

	public int getLongitud() {
		return longitud;
	}

	/*
	 *  METODOS
	 */
	public String generarContrasea(int longitud) {
        String asciiChars = "12345678901234567890AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
        Random random = new Random();
        while (i < longitud) {
        	stringBuilder.append(asciiChars.charAt(random.nextInt(asciiChars.length())));
            i++;
        }
        return stringBuilder.toString();
    }
	
	public boolean esFuerte() {
		boolean esFuerte = false;
		String contraseña = this.contraseña;
		int longitud = this.longitud;
		String pattern1 = "[0-9]";
		String pattern2 = "[A-Z]";
		String pattern3 = "[a-z]";
		int totalDigitos = 0;
		int totalMayusculas = 0;
		int totalMinusculas = 0;
		for(int i = 0 ; i < longitud ; i++) {
			String letra = Character.toString(contraseña.charAt(i));
			if (letra.matches(pattern1)) {
				totalDigitos++;
			}
			if (letra.matches(pattern2)) {
				totalMayusculas++;
			}
			if (letra.matches(pattern3)) {
				totalMinusculas++;
			}
		}
		if (totalDigitos > 5 && totalMayusculas > 2 && totalMinusculas > 1) {
			esFuerte = true;
		}
		return esFuerte;
	}



}
