package ejercicio1;

import javax.swing.JOptionPane;

public class mainApp {
	public static int intentos = 0;

	public static void main(String[] args) {
		Numero numero = new Numero();
		JOptionPane.showMessageDialog(null, "Intenta adivinar el numero! :D");

		boolean acerto = false;
		while (!acerto) {
			try {
				int num = Integer
						.parseInt(JOptionPane.showInputDialog("Escribe un numero :D "));
				if (num == numero.getNumero()) {
					JOptionPane.showMessageDialog(null, "Lo lograste! :D, Intentos Totales:" + intentos + "");
					acerto = true;
				} else if (num > numero.getNumero()) {
					JOptionPane.showMessageDialog(null, "El numero que has de introducir es mas peque�o! :o");
				} else if (num < numero.getNumero()) {
					JOptionPane.showMessageDialog(null, "El numero que has de introducir es mas grande! :O");
				}

			} catch (Exception e) {
				System.out.println(e.toString());
				JOptionPane.showMessageDialog(null, "Tienes que escribir un numero entre 0 y 500");
				
			} finally {
				intentos++;
			}
		}
	}
}
