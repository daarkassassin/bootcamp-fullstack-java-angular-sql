package ejercicio2;

@SuppressWarnings("serial")
public class MiExcepcion extends Exception{
	
	private int codigoExcepcion;

	public MiExcepcion() {
		super();
	}
	public MiExcepcion(int codigoError) {
		super();
		this.codigoExcepcion = codigoError;
	}
	
	@Override
	public String getMessage() {
		String mensaje = "";
		switch(codigoExcepcion) {
		case 111:
			mensaje = "Excepcion 111. Test";
			break;
		case 112:
			mensaje = "Excepcion 112. Test";
			break;
		default:
			mensaje = "Excepcion capturada con mensaje: Esto es un objeto Exception";
		}
		
		return mensaje;
	}
	
	

}
