package ejercicio3;

import java.util.Random;

public class Numero {
	//Atributos y Constantes
	private final int MAX = 999;
	private int numero;
	
	//Constructor
	public Numero() {
		this.numero = setNumero();
	}
	
	public Numero(int numero) {
		this.numero = numero;
	}

	//Getters y setters
	public int getNumero() {
		return numero;
	}
	
	//metodos
	private int setNumero() {
		Random r = new Random();
		return r.nextInt(MAX);
	}

	
}
