package ejercicio3;

public class mainApp {

	public static void main(String[] args) {
		System.out.println("Generando n�mero aleatorio...");
		Numero x = new Numero();
		
		try {
			System.out.println("El n�mero aleatorio generado es: "+x.getNumero());
			if (x.getNumero() % 2 == 0) {
				throw new MiExcepcion(2);
			}else {
				throw new MiExcepcion(1);
			}
		}catch(MiExcepcion ex) {
			
			System.out.println(ex.getMessage());
		}
		
	}

}
