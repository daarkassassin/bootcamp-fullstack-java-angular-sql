package ejercicio3;

@SuppressWarnings("serial")
public class MiExcepcion extends Exception{
	
	private int codigoExcepcion;

	public MiExcepcion() {
		super();
	}
	public MiExcepcion(int codigoError) {
		super();
		this.codigoExcepcion = codigoError;
	}
	
	@Override
	public String getMessage() {
		String mensaje = "";
		switch(codigoExcepcion) {
		case 1:
			mensaje = "Es impar";
			break;
		case 2:
			mensaje = "Es par";
			break;
		default:
			mensaje = "Excepcion capturada con mensaje: Esto es un objeto Exception";
		}
		
		return mensaje;
	}
	
	

}
