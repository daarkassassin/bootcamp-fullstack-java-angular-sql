package ejercicio4.Clases;

public class Potencia {
	//Atributos y Constantes
	private double a;
	private double b;
	private double res;
	
	//Constructor
	public Potencia() {
		this.a = 0;
		this.b = 0;
		this.res = 0;
	}
	
	public Potencia(double a, double b) {
		this.a = a;
		this.b = b;
		this.res = potenciar(a, b);
	}

	//Getters y setters
	public double getResultado() {
		return res;
	}
	public double getA() {
		return a;
	}
	public double getB() {
		return b;
	}
	
	//metodos
	private double potenciar(double a, double b) {
		double x = Math.pow(a,b);
		return x;
	}

	
}
