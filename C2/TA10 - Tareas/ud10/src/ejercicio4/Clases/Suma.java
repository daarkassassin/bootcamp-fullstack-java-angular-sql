package ejercicio4.Clases;

public class Suma {
	//Atributos y Constantes
	private double a;
	private double b;
	private double res;
	
	//Constructor
	public Suma() {
		this.a = 0;
		this.b = 0;
		this.res = 0;
	}
	
	public Suma(double a, double b) {
		this.a = a;
		this.b = b;
		this.res = sumar(a, b);
	}

	//Getters y setters
	public double getResultado() {
		return res;
	}
	public double getA() {
		return a;
	}
	public double getB() {
		return b;
	}
	
	//metodos
	private double sumar(double a, double b) {
		double x = a + b;
		return x;
	}

	
}
