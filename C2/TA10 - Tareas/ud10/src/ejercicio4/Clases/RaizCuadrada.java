package ejercicio4.Clases;

public class RaizCuadrada {
	//Atributos y Constantes
	private double a;
	private double res;
	
	//Constructor
	public RaizCuadrada() {
		this.a = 0;
		this.res = 0;
	}
	
	public RaizCuadrada(double a) {
		this.a = a;
		this.res = raizcuadrada(a);
	}

	//Getters y setters
	public double getResultado() {
		return res;
	}
	public double getA() {
		return a;
	}
	
	//metodos
	private double raizcuadrada(double a) {
		double x = Math.sqrt(a);
		return x;
	}

	
}
