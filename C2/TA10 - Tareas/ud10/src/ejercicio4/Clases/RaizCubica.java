package ejercicio4.Clases;

public class RaizCubica {
	//Atributos y Constantes
	private double a;
	private double res;
	
	//Constructor
	public RaizCubica() {
		this.a = 0;
		this.res = 0;
	}
	
	public RaizCubica(double a) {
		this.a = a;
		this.res = raizcubica(a);
	}

	//Getters y setters
	public double getResultado() {
		return res;
	}
	public double getA() {
		return a;
	}
	
	//metodos
	private double raizcubica(double a) {
		double x = Math.cbrt(a);
		return x;
	}

	
}
