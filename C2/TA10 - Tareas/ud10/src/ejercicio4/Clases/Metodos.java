package ejercicio4.Clases;

import javax.swing.JOptionPane;

public class Metodos {
	//Atributos
	private Double num1;
	private Double num2;
	private boolean salir;

	//Constructor
	public Metodos() {
		num1 = Double.NaN;
		num2 = Double.NaN;
		salir = false;
	}
	
	//Metodo principal [ Menu ]
	public void menu() {
		
		while (!salir) {
			
			Object seleccion = getSeleccion(); //Obtenemos la seleccion del usuario.
			
			if(seleccion == null) { 
				salir = true; //Si le damos a Cancelar [seleccion = null] y se cierra el programa.
			}else {
				
				//Imprimimos por consola la eleccion del usuario.
				System.out.println("El usuario ha elegido " + seleccion.toString());
				
				//Realizamos la operacion que solicita el usuario.
				switch (seleccion.toString()) {
				
				case "Sumar":
					sumar();
					break;
	
				case "Restar":
					restar();
					break;
	
				case "Multiplicar":
					multiplicar();
					break;
	
				case "Dividir":
					dividir();
					break;
	
				case "Potenciar":
					potenciar();
					break;
	
				case "Ra�z cuadrada":
					raizCuadrada();
					break;
	
				case "Ra�z Cubica":
					raizCubica();
					break;
					
				}
				
				//Seteamos los valores num1 y num2 NaN por si el usuario quiere realizar otra operaci�n, el programa este a punto para realizarla.
				setNum1Num2ToNaN();
				
			}
		}
		
	}
	
	//Metodos "setNum1Num2ToNaN", "getSeleccion", "getDouble", "Sumar", "Restar", "Multiplicar", "Dividir", "Potenciar", "Ra�z cuadrada", "Ra�z Cubica"
	public void setNum1Num2ToNaN() {
		this.num1 = Double.NaN;
		this.num2 = Double.NaN;
	}
	
	public Object getSeleccion() {
		Object seleccion = null;
		try {
			seleccion = JOptionPane.showInputDialog(
				null, 
				"Seleccione opcion", 
				"Selector de opciones",
				JOptionPane.QUESTION_MESSAGE, 
				null, // null para icono defecto
				new Object[] { "Sumar", "Restar", "Multiplicar", "Dividir", "Potenciar", "Ra�z cuadrada", "Ra�z Cubica"},
				"Sumar"// Opcion por defecto
			);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return seleccion;
	}
	
	
	public double getDouble(String s) {
		double n = 0;
		try {
			n = Double.parseDouble(JOptionPane.showInputDialog(s));
		}catch(Exception e) {
			System.out.println(e.getMessage());
			n = Double.NaN;
		}
		return n;
	}
	
	public void sumar() {
		while (num1.isNaN()) {
			num1 = getDouble("Primer numero para sumar");
		}

		while (num2.isNaN()) {
			num2 = getDouble("Segundo numero para sumar");
		}

		Suma suma = new Suma(num1, num2);
		JOptionPane.showMessageDialog(null, num1 + " + " + num2 + " = " + suma.getResultado());
	}
	
	public void restar() {
		while (num1.isNaN()) {
			num1 = getDouble("Primer numero para restar");
		}

		while (num2.isNaN()) {
			num2 = getDouble("Segundo numero para restar");
		}

		Resta resta = new Resta(num1, num2);
		JOptionPane.showMessageDialog(null, num1 + " - " + num2 + " = " + resta.getResultado());
	}
	
	public void multiplicar() {
		while (num1.isNaN()) {
			num1 = getDouble("Primer numero para multiplicar");
		}

		while (num2.isNaN()) {
			num2 = getDouble("Segundo numero para multiplicar");
		}

		Multiplicacion multiplicacion = new Multiplicacion(num1, num2);
		JOptionPane.showMessageDialog(null, num1 + " * " + num2 + " = " + multiplicacion.getResultado());
		
	}
	
	public void dividir() {
		while (num1.isNaN()) {
			num1 = getDouble("Primer numero para dividir");
		}

		while (num2.isNaN()) {
			num2 = getDouble("Segundo numero para dividir");
		}

		Division division = new Division(num1, num2);
		JOptionPane.showMessageDialog(null, num1 + " / " + num2 + " = " + division.getResultado());
	}
	
	public void potenciar() {
		while (num1.isNaN()) {
			num1 = getDouble("Primer numero para potenciar");
		}

		while (num2.isNaN()) {
			num2 = getDouble("Segundo numero para potenciar");
		}

		Potencia potencia = new Potencia(num1, num2);
		JOptionPane.showMessageDialog(null, num1 + " elevado a " + num2 + " = " + potencia.getResultado());
	}
	
	public void raizCuadrada() {
		while (num1.isNaN()) {
			num1 = getDouble("Introduce un numero");
		}
		
		RaizCuadrada raizCuadrada = new RaizCuadrada(num1);
		JOptionPane.showMessageDialog(null, " La raizCuadrada de " + num1 + " = " + raizCuadrada.getResultado());
	}
	
	public void raizCubica() {
		while (num1.isNaN()) {
			num1 = getDouble("Introduce un numero");
		}
		
		RaizCubica raizCubica = new RaizCubica(num1);
		JOptionPane.showMessageDialog(null, " La raizCubica de " + num1 + " = " + raizCubica.getResultado());
	}

}
