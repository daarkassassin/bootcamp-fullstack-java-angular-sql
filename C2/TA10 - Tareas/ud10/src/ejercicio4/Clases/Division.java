package ejercicio4.Clases;

public class Division {
	//Atributos y Constantes
	private double a;
	private double b;
	private double res;
	
	//Constructor
	public Division() {
		this.a = 0;
		this.b = 0;
		this.res = 0;
	}
	
	public Division(double a, double b) {
		this.a = a;
		this.b = b;
		this.res = dividir(a, b);
	}

	//Getters y setters
	public double getResultado() {
		return res;
	}
	public double getA() {
		return a;
	}
	public double getB() {
		return b;
	}
	
	//metodos
	private double dividir(double a, double b) {
		double x = a / b;
		return x;
	}

	
}
