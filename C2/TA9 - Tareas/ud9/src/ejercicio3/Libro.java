package ejercicio3;

class Libro {

	/*
	 * ATRIBUTOS
	 */
	private String isbn;
	private String titulo;
	private String autor;
	private int numPaginas;

	/*
	 * METODOS CONSTRUCTOR
	 */

	public Libro() {
		this.isbn = "";
		this.titulo = "";
		this.autor = "";
		this.numPaginas = 0;

	}

	public Libro(String isbn, String titulo, String autor, int numPaginas) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.numPaginas = numPaginas;
	}

	/*
	 * METODOS SET
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}

	/*
	 * METODOS GET
	 */
	public String getIsbn() {
		return isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getAutor() {
		return autor;
	}

	public int getNumPaginas() {
		return numPaginas;
	}

	/*
	 * METODOS
	 */
	@Override
	public String toString() {
		return "El libro "+titulo+" con ISBN: "+isbn+" creado por el autor "+autor+" tiene "+numPaginas+"paginas";
	}

	public void compareTo(Libro libro) {
		// TODO Auto-generated method stub
		if (getNumPaginas()>libro.getNumPaginas()) {
			System.out.println ("El libro "+titulo+" con ISBN: "+isbn+" creado por el autor "+autor+" es el que tiene m�s p�ginas.");
		}else {
			System.out.println ("El libro "+libro.titulo+" con ISBN: "+libro.isbn+" creado por el autor "+libro.autor+" es el que tiene m�s p�ginas.");
		}
	}
	

	

}
