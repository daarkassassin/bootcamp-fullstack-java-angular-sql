/**
 * 
 */
package ejercicio3;

/**
 * @author David Bonet
 *
 */
public class mainApp {
	public static void main(String[] args) {
		//Generamos 2 libros
		Libro libro1 = new Libro("ABC12345","El String","Eclipse",300);
		Libro libro2 = new Libro("ABD12346","El Array","NetBeans",200);
		
		//Mostramos por pantalla los datos de los 2 libros
		System.out.println(libro1.toString());
		System.out.println(libro2.toString());

		//Comparamos el libro 1 con el 2 para saber qual tiene 
		//mas paginas y lo mostramos por pantalla
		libro1.compareTo(libro2);
		
	}

}
