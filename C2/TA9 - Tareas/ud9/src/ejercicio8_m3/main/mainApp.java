package ejercicio8_m3.main;

import java.util.Random;

import javax.swing.JOptionPane;

import ejercicio8_m3.clases.Persona.Conductor;
import ejercicio8_m3.clases.Persona.Licencia;
import ejercicio8_m3.clases.Persona.Titular;
import ejercicio8_m3.clases.Vehiculo.Camion;
import ejercicio8_m3.clases.Vehiculo.Coche;
import ejercicio8_m3.clases.Vehiculo.Moto;
import ejercicio8_m3.clases.Vehiculo.Ruedas;
import ejercicio8_m3.clases.Vehiculo.Vehiculo;

/**
 * @author David B.
 * @author Xavi B.
 *
 */
public class mainApp {

	// Variables, constantes y objeto random que usaremos en el programa.
	static Random r = new Random();
	static final int MAX_CONDUCTORES = 100;
	static final int MAX_VEHICULOS = 100;
	static int conductoresCreados = 0;
	static int vehiculosCreados = 0;
	static boolean usuarioCreado = false;

	// Main.
	public static void main(String[] args) {
		menuPrincipal(); // Llamamos al menu para iniciar la aplicaci�n
	}

	public static void menuPrincipal() {
		// Variable con la que sabemos si ya se ha creado el titular o no.
		boolean titularCreado = false;

		// Objetos instanciados que vamos a usar
		Titular t = new Titular();
		Conductor c[] = new Conductor[MAX_CONDUCTORES];
		Vehiculo v[] = new Vehiculo[MAX_VEHICULOS];

		// Menu
		boolean salir = false;
		while (!salir) {
			String opcion = JOptionPane.showInputDialog(
					"[1]nuevo Titular, [2]nuevo vehiculo, [3]Lista de Personas, [4]Lista de Vehiculos, [5]Salir");

			switch (opcion) {
			case "5": // - Salir
				salir = true;
				break;

			case "1": // - Crear usuario
				if (titularCreado) {
					boolean sino = validar("Modificar el titular existente?");
					if (sino) {
						t = crearTitular();
						System.out.println(t.toString());
					}
				} else {
					t = crearTitular();
					titularCreado = true;
					System.out.println(t.toString());
				}
				break;

			case "2": // - Crear Vehiculo
				if (usuarioCreado) {
					menuCrearVehiculo(t, c, v);
				} else {
					JOptionPane.showMessageDialog(null, "Primero has de crear un nuevo Titular!");
				}
				break;

			case "3": // - Lista de Personas
				mostrarTitularConductores(t, c);
				break;

			case "4": // - Lista de Vehiculos
				mostrarVehiculos(v);
				break;

			default:
				JOptionPane.showMessageDialog(null, "Lo opci�n introducida no es valida!");
			}
		}
	}

	// Metodo con el que creamos un titular.
	public static Titular crearTitular() {
		String nombre = JOptionPane.showInputDialog("Nombre del titular:");
		String apellido = JOptionPane.showInputDialog("Apellido del titular:");
		String fechaNacimiento = JOptionPane.showInputDialog("Fecha de nacimiento del conductor:");
		Licencia licenciaConducir = crearLicencia(nombre, apellido);
		boolean garage = validar("Tiene garage?");
		boolean seguro = validar("Tiene seguro?");
		Titular titular = new Titular(nombre, apellido, fechaNacimiento, licenciaConducir, garage, seguro);
		usuarioCreado = true;
		return titular;

	}

	// Metodo con el que creamos un conductor.
	public static Conductor crearConductor(String licencia) {
		String nombre = JOptionPane.showInputDialog("Nombre del conductor:");
		String apellido = JOptionPane.showInputDialog("Apellido del conductor:");
		String fechaNacimiento = JOptionPane.showInputDialog("Fecha de nacimiento del conductor:");
		Licencia licenciaConducir = crearLicencia(nombre, apellido, licencia);
		Conductor conductor = new Conductor(nombre, apellido, fechaNacimiento, licenciaConducir);
		usuarioCreado = true;
		return conductor;
	}

	// Metodo con el que creamos una licencia a elegir entre las 3 opciones (coche,
	// moto, camion);
	public static Licencia crearLicencia(String nombre, String apellido) {
		int id = r.nextInt(9999);
		String tipoDeLicencia = "";
		while (true) {
			tipoDeLicencia = JOptionPane.showInputDialog("Tipo de licencia: [coche, moto, camion]");
			if (tipoDeLicencia.equals("coche")) {
				break;
			} else if (tipoDeLicencia.equals("moto")) {
				break;
			} else if (tipoDeLicencia.equals("camion")) {
				break;
			} else {
				JOptionPane.showMessageDialog(null, "Tipo de licencia no admitido!");
			}
		}
		String nombreCompleto = "" + nombre + " " + apellido;
		String fechaCaducidad = JOptionPane.showInputDialog("Fecha de caducidad de la licencia: ");
		Licencia licencia = new Licencia(id, tipoDeLicencia, nombreCompleto, fechaCaducidad);
		return licencia;
	}

	// Metodo con el que creamos una licencia de un tipo especificado pasandolo por string.
	public static Licencia crearLicencia(String nombre, String apellido, String licencia) {
		int id = r.nextInt(9999);
		String tipoDeLicencia = "";
		while (true) {
			tipoDeLicencia = JOptionPane.showInputDialog("Tipo de licencia: [" + licencia + "]");
			if (tipoDeLicencia.equals(licencia)) {
				break;
			} else {
				JOptionPane.showMessageDialog(null, "Tipo de licencia ha de ser [" + licencia + "]!");
			}
		}
		String nombreCompleto = "" + nombre + " " + apellido;
		String fechaCaducidad = JOptionPane.showInputDialog("Fecha de caducidad de la licencia: ");
		Licencia licenciaConductor = new Licencia(id, tipoDeLicencia, nombreCompleto, fechaCaducidad);
		return licenciaConductor;
	}

	// Menu vehiculo.
	public static void menuCrearVehiculo(Titular t, Conductor[] c, Vehiculo[] v) {
		boolean salirMenuCrearVehiculo = false;
		boolean titularTieneLicencia = true;
		while (!salirMenuCrearVehiculo) {
			String opcion = JOptionPane
					.showInputDialog("[0]Salir, [1]Pedir un coche, [2]Pedir una moto, [3]Pedir un camion");

			switch (opcion) {

			case "0": // - Salir
				salirMenuCrearVehiculo = true;
				break;

			case "1": // - Pedir un coche
				titularTieneLicencia = titularTieneLicencia("coche", t);

				// Si el titular no tiene licencia, Avisamos y obligamos a a�adir un conductor.
				if (!titularTieneLicencia) {
					if (conductoresCreados >= MAX_CONDUCTORES) {
						JOptionPane.showMessageDialog(null, "Llegamos al limite de conductores!");
						conductoresCreados++;

					} else {
						c[conductoresCreados] = crearConductor("coche");
						System.out.println(c[conductoresCreados].toString());
						conductoresCreados++;

					}
				}

				// Creamos el vehiculo
				v[vehiculosCreados] = (Coche) crearVehiculo(1, t, c, titularTieneLicencia);
				((Coche) v[vehiculosCreados]).setRuedas(pedirRuedas(1));
				System.out.println(((Coche) v[vehiculosCreados]).toString());

				// Si el titular tiene licencia preguntamos si quiere a�adir un segundo
				// conductor.
				if (titularTieneLicencia) {
					if (validar("A�adir conductor?")) {
						c[conductoresCreados] = crearConductor("coche");
						System.out.println(c[conductoresCreados].toString());
						((Coche) v[vehiculosCreados]).setConductor(c[conductoresCreados]);
						conductoresCreados++;
					}
				}

				vehiculosCreados++;
				break;

			case "2": // - Pedir una moto
				titularTieneLicencia = titularTieneLicencia("moto", t);

				// Si el titular no tiene licencia, Avisamos y obligamos a a�adir un conductor.
				if (!titularTieneLicencia) {
					if (conductoresCreados >= MAX_CONDUCTORES) {
						JOptionPane.showMessageDialog(null, "Llegamos al limite de conductores!");
						conductoresCreados++;
					} else {
						c[conductoresCreados] = crearConductor("moto");
						System.out.println(c[conductoresCreados].toString());
						conductoresCreados++;
					}
				}

				// Creamos el vehiculo
				v[vehiculosCreados] = (Moto) crearVehiculo(2, t, c, titularTieneLicencia);
				((Moto) v[vehiculosCreados]).setRuedas(pedirRuedas(2));
				System.out.println(((Moto) v[vehiculosCreados]).toString());

				// Si el titular tiene licencia preguntamos si quiere a�adir un segundo
				// conductor.
				if (titularTieneLicencia) {
					if (validar("A�adir conductor?")) {
						c[conductoresCreados] = crearConductor("moto");
						System.out.println(c[conductoresCreados].toString());
						((Moto) v[vehiculosCreados]).setConductor(c[conductoresCreados]);
						conductoresCreados++;
					}
				}

				vehiculosCreados++;
				break;

			case "3": // - Pedir un camion
				titularTieneLicencia = titularTieneLicencia("camion", t);

				// Si el titular no tiene licencia, Avisamos y obligamos a a�adir un conductor.
				if (!titularTieneLicencia) {
					if (conductoresCreados >= MAX_CONDUCTORES) {
						JOptionPane.showMessageDialog(null, "Llegamos al limite de conductores!");
						conductoresCreados++;
					} else {
						c[conductoresCreados] = crearConductor("camion");
						System.out.println(c[conductoresCreados].toString());
						conductoresCreados++;
					}
				}

				// Creamos el vehiculo
				v[vehiculosCreados] = (Camion) crearVehiculo(2, t, c, titularTieneLicencia);
				((Camion) v[vehiculosCreados]).setRuedas(pedirRuedas(2));
				System.out.println(((Camion) v[vehiculosCreados]).toString());

				// Si el titular tiene licencia preguntamos si quiere a�adir un segundo
				// conductor.
				if (titularTieneLicencia) {
					if (validar("A�adir conductor?")) {
						c[conductoresCreados] = crearConductor("camion");
						System.out.println(c[conductoresCreados].toString());
						((Camion) v[vehiculosCreados]).setConductor(c[conductoresCreados]);
						conductoresCreados++;
					}
				}

				vehiculosCreados++;
				break;

			default:
				JOptionPane.showMessageDialog(null, "Lo opci�n introducida no es valida!");
			}
		}

	}

	// Metodo que comprueba si el titular tiene la licencia que pasamos por string.
	public static boolean titularTieneLicencia(String s, Titular t) {
		Licencia licenciaTitular = t.getLicenciaConducir();
		if (licenciaTitular.getTipoDeLicencia().equals(s)) {
			return true;

		} else {
			JOptionPane.showMessageDialog(null, "El titular no tiene licencia de moto!");
			return false;
		}
	}

	// Metodo con el que creamos el vehiculo. (Coche, Moto y Camion)
	public static Object crearVehiculo(int opc, Titular t, Conductor[] c, boolean tieneLicencia) {
		String matricula;
		while (true) {
			matricula = JOptionPane.showInputDialog("Matricula del vehiculo? '0000xx'");
			if (Vehiculo.testMatricula(matricula)) {
				break;
			} else {
				JOptionPane.showMessageDialog(null,
						"La matricula del vehiculo ha de tener 4 numeros seguido de 2 letras.");
			}
		}
		String marca = JOptionPane.showInputDialog("Marca del vehiculo?");
		String color = JOptionPane.showInputDialog("Color del vehiculo?");
		Object devolver = null;
		if (opc == 1) {
			if (tieneLicencia) {
				devolver = new Coche(matricula, marca, color, t);
			} else {
				devolver = new Coche(matricula, marca, color, t, tieneLicencia, c[conductoresCreados - 1]);
			}

		} else if (opc == 2) {
			if (tieneLicencia) {
				devolver = new Moto(matricula, marca, color, t);
			} else {
				devolver = new Moto(matricula, marca, color, t, tieneLicencia, c[conductoresCreados - 1]);
			}

		} else if (opc == 3) {
			if (tieneLicencia) {
				devolver = new Camion(matricula, marca, color, t);
			} else {
				devolver = new Camion(matricula, marca, color, t, tieneLicencia, c[conductoresCreados - 1]);
			}
		}

		return devolver;
	}

	// Metodo con el que asignamos las ruedas al vehiculo. (Coche, Moto y Camion)
	public static Ruedas pedirRuedas(int opc) {
		Ruedas devolver = null;
		String[] frasesMarca = { "Marca de la rueda delantera derecha ?", "Marca de la rueda delantera izquierda?",
				"Marca de la rueda trasera derecha ?", "Marca de la rueda trasera izquierda ?" };
		String[] frasesDiametro = { "Diametro de la rueda delantera derecha ?",
				"Diametro de la rueda delantera izquierda?", "Diametro de la rueda trasera derecha ?",
				"Diametro de la rueda trasera izquierda ?" };
		String[] frasesMarcaMoto = { "Marca de la rueda delantera ?", "Marca de la rueda trasera ?" };
		String[] frasesDiametroMoto = { "Diametro de la rueda delantera ?", "Diametro de la rueda trasera ?" };

		Double[] diametroRuedas = { null, null, null, null };
		String[] marcaRuedas = { "", "", "", "" };

		switch (opc) {

		case 1: // - Ruedas coche
			for (int i = 0; i <= 3; i++) {
				String marca = JOptionPane.showInputDialog(frasesMarca[i]);
				Double diam = Double.parseDouble(JOptionPane.showInputDialog(frasesDiametro[i]));
				while (!Ruedas.testDiametro(diam)) {
					JOptionPane.showMessageDialog(null, "El diametro ha de ser mayor que 0.4 y inferior que 4.");
					diam = Double.parseDouble(JOptionPane.showInputDialog(frasesDiametro[i]));
				}
				marcaRuedas[i] = marca;
				diametroRuedas[i] = diam;
			}

			devolver = new Ruedas(marcaRuedas[0], diametroRuedas[0], marcaRuedas[1], diametroRuedas[1], marcaRuedas[2],
					diametroRuedas[2], marcaRuedas[3], diametroRuedas[3]);
			break;

		case 2: // - Ruedas moto
			for (int i = 0; i <= 1; i++) {
				String marca = JOptionPane.showInputDialog(frasesMarcaMoto[i]);
				Double diam = Double.parseDouble(JOptionPane.showInputDialog(frasesDiametroMoto[i]));
				while (!Ruedas.testDiametro(diam)) {
					JOptionPane.showMessageDialog(null, "El diametro ha de ser mayor que 0.4 y inferior que 4.");
					diam = Double.parseDouble(JOptionPane.showInputDialog(frasesDiametroMoto[i]));
				}
				marcaRuedas[i] = marca;
				diametroRuedas[i] = diam;
			}
			devolver = new Ruedas(marcaRuedas[0], diametroRuedas[0], marcaRuedas[1], diametroRuedas[1]);
			break;

		case 3: // - Ruedas camion
			for (int i = 0; i <= 3; i++) {
				String marca = JOptionPane.showInputDialog(frasesMarca[i]);
				Double diam = Double.parseDouble(JOptionPane.showInputDialog(frasesDiametro[i]));
				while (!Ruedas.testDiametro(diam)) {
					JOptionPane.showMessageDialog(null, "El diametro ha de ser mayor que 0.4 y inferior que 4.");
					diam = Double.parseDouble(JOptionPane.showInputDialog(frasesDiametro[i]));
				}
				marcaRuedas[i] = marca;
				diametroRuedas[i] = diam;
			}
			devolver = new Ruedas(marcaRuedas[0], diametroRuedas[0], marcaRuedas[1], diametroRuedas[1], marcaRuedas[2],
					diametroRuedas[2], marcaRuedas[3], diametroRuedas[3]);
			break;
		}

		return devolver;
	}

	// Mostraremos Todos los vehiculos creados por consola.
	public static void mostrarVehiculos(Vehiculo[] v) {
		System.out.println("Lista de vehiculos");
		if (vehiculosCreados > 0) {
			for (int i = 0; vehiculosCreados > i; i++) {
				System.out.println(v[i].toString());
			}
		}

	}

	// Mostraremos el Titular y los Conductores creados por consola.
	public static void mostrarTitularConductores(Titular t, Conductor[] c) {
		System.out.println("Titular");
		System.out.println(t.toString());
		if (conductoresCreados > 0) {
			System.out.println("Conductores");
			for (int i = 0; conductoresCreados > i; i++) {
				System.out.println(c[i].toString());
			}
		}

	}

	// Metodo con el que validamos con si o no a una preguntada pasada por string.
	public static boolean validar(String s) {
		boolean validar = false;
		String dato = "";
		while (true) {
			dato = JOptionPane.showInputDialog(s + " ( si o no )");
			if (dato.equals("si")) {
				validar = true;
				break;
			} else if (dato.equals("no")) {
				validar = false;
				break;
			} else {
				JOptionPane.showMessageDialog(null, "Opcion no valida!");
			}
		}
		return validar;
	}

}
