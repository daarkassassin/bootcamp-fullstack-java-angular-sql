package ejercicio8_m3.clases.Persona;

/**
 * @author David B.
 * @author Xavi B.
 *
 */

public class Titular extends Persona {
	// ATRIBUTOS
	private Licencia licenciaConducir;
	private boolean garage;
	private boolean seguro;

	// METODOS CONSTRUCTORES
	public Titular() {
		super();
		this.licenciaConducir = null;
		this.garage = false;
		this.seguro = false;
	}
	
	public Titular(String nombre, String apellido, String fechaNacimiento, Licencia licenciaConducir, boolean garage, boolean seguro) {
		super(nombre, apellido, fechaNacimiento);
		this.licenciaConducir = licenciaConducir;
		this.garage = garage;
		this.seguro = seguro;
	}

	// GETERS Y SETERS
	public Licencia getLicenciaConducir() {
		return licenciaConducir;
	}

	public void setLicenciaConducir(Licencia licenciaConducir) {
		this.licenciaConducir = licenciaConducir;
	}

	public boolean isGarage() {
		return garage;
	}

	public void setGarage(boolean garage) {
		this.garage = garage;
	}

	public boolean isSeguro() {
		return seguro;
	}

	public void setSeguro(boolean seguro) {
		this.seguro = seguro;
	}

	//METODOS
	@Override
	public String toString() {
		return "Titular [licenciaConducir=" + licenciaConducir + ", garage=" + garage + ", seguro=" + seguro
				+ ", Persona= " + super.toString() + "]";
	}
	
	
	
}
