package ejercicio8_m3.clases.Persona;

/**
 * @author David B.
 * @author Xavi B.
 *
 */

public class Licencia {
	// ATRIBUTOS
	private int id;
	private String tipoDeLicencia;
	private String nombreCompleto;
	private String fechaCaducidad;

	// METODOS CONSTRUCTORES
	public Licencia() {
		this.id = 0;
		this.tipoDeLicencia = null;
		this.nombreCompleto = "";
		this.fechaCaducidad = "";
	}
	
	public Licencia(int id, String tipoDeLicencia, String nombreCompleto, String fechaCaducidad) {
		this.id = id;
		this.tipoDeLicencia = tipoDeLicencia;
		this.nombreCompleto = nombreCompleto;
		this.fechaCaducidad = fechaCaducidad;
	}
	
	// GETERS Y SETERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipoDeLicencia() {
		return tipoDeLicencia;
	}

	public void setTipoDeLicencia(String tipoDeLicencia) {
		this.tipoDeLicencia = tipoDeLicencia;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getFechaCaducidad() {
		return fechaCaducidad;
	}

	public void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	//METODOS
	@Override
	public String toString() {
		return "Licencia [id=" + id + ", tipoDeLicencia=" + tipoDeLicencia + ", nombreCompleto=" + nombreCompleto
				+ ", fechaCaducidad=" + fechaCaducidad + "]";
	}
	
	//Metodo para comprobar la licencia
	public boolean comprobarLicencia(String licencia) {
		String licenciaCliente = getTipoDeLicencia();
		if (licencia.equals(licenciaCliente)) {
			return true;
		}else {
			return false;
		}
		
	}
	
	
	
	

}
