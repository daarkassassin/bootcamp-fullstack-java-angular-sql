package ejercicio8_m3.clases.Persona;

/**
 * @author David B.
 * @author Xavi B.
 *
 */

public class Conductor extends Persona {
	// ATRIBUTOS
	private Licencia licenciaConducir;

	// METODOS CONSTRUCTORES
	public Conductor() {
		super();
		this.licenciaConducir = null;
	}

	public Conductor(String nombre, String apellido, String fechaNacimiento, Licencia licenciaConducir) {
		super(nombre, apellido, fechaNacimiento);
		this.licenciaConducir = licenciaConducir;
	}

	// GETERS Y SETERS
	public Licencia getLicenciaConducir() {
		return licenciaConducir;
	}

	public void setLicenciaConducir(Licencia licenciaConducir) {
		this.licenciaConducir = licenciaConducir;
	}

	// METODOS
	@Override
	public String toString() {
		return "Conductor [licenciaConducir=" + licenciaConducir + ", Persona" + super.toString() + "]";
	}

}
