package ejercicio8_m3.clases.Persona;

/**
 * @author David B.
 * @author Xavi B.
 *
 */

public class Persona {
	// ATRIBUTOS
	protected String nombre;
	protected String apellido;
	protected String fechaNacimiento;

	// METODOS CONSTRUCTORES
	public Persona() {
		this.nombre = "";
		this.apellido = "";
		this.fechaNacimiento = "";
	}

	public Persona(String nombre, String apellido, String fechaNacimiento) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaNacimiento = fechaNacimiento;
	}

	// GETERS Y SETERS
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	//METODOS
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellido=" + apellido + ", fechaNacimiento=" + fechaNacimiento + "]";
	}
	
	
}
