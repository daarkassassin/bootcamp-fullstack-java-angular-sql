package ejercicio8_m3.clases.Vehiculo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ejercicio8_m3.clases.Persona.Conductor;
import ejercicio8_m3.clases.Persona.Titular;

/**
 * @author David B.
 * @author Xavi B.
 *
 */
public class Vehiculo {

	// ATRIBUTOS Y CONSTANTES
	protected String matricula;
	protected String marca;
	protected String color;
	protected Titular titular;
	protected boolean isTitularConductor;
	protected Conductor conductor;

	// CONSTRUCTORES
	public Vehiculo() {
		this.matricula = "";
		this.marca = "";
		this.color = "";
		this.titular = null;
		this.isTitularConductor = true;
		this.conductor = null;
	}

	public Vehiculo(String matricula, String marca, String color) {
		this.matricula = matricula;
		this.marca = marca;
		this.color = color;
		this.titular = null;
		this.isTitularConductor = true;
		this.conductor = null;
	}

	public Vehiculo(String matricula, String marca, String color, Titular titular) {
		this.matricula = matricula;
		this.marca = marca;
		this.color = color;
		this.titular = titular;
		this.isTitularConductor = true;
		this.conductor = null;
	}

	public Vehiculo(String matricula, String marca, String color, Titular titular, boolean isTitularConductor,
			Conductor conductor) {
		this.matricula = matricula;
		this.marca = marca;
		this.color = color;
		this.titular = titular;
		this.isTitularConductor = isTitularConductor;
		this.conductor = conductor;
	}

	// GETERS Y SETTERS
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMatricula() {
		return matricula;
	}

	public Titular getTitular() {
		return titular;
	}

	public void setTitular(Titular titular) {
		this.titular = titular;
	}

	public boolean isTitularConductor() {
		return isTitularConductor;
	}

	public void setTitularConductor(boolean isTitularConductor) {
		this.isTitularConductor = isTitularConductor;
	}

	public Conductor getConductores() {
		return conductor;
	}

	public void setConductor(Conductor conductor) {
		this.conductor = conductor;
	}

	//METODOS
	@Override
	public String toString() {
		return "Vehiculo [matricula=" + matricula + ", marca=" + marca + ", color=" + color + ", titular=" + titular
				+ ", isTitularConductor=" + isTitularConductor + ", conductor=" + conductor + "]";
	}

	//Metodo para comprobar el formato de la matricula.
	public static boolean testMatricula(String matricula) {
		Pattern p = Pattern.compile("^\\d{4}[A-Z]{2,3}");
		Matcher m = p.matcher(matricula);
		if (m.matches()) {
			return true;
		} else {
			return false;
		}
	}

}
