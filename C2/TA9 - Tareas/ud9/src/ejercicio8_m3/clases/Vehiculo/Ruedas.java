package ejercicio8_m3.clases.Vehiculo;

/**
 * @author David B.
 * @author Xavi B.
 *
 */
public class Ruedas extends Coche{
	
	//ATRIBUTOS Y CONSTANTES
	private String rdd_Marca; 
	private double rdd_Diametro;
	
	private String rdi_Marca;
	private double rdi_Diametro;
	
	private String rtd_Marca;
	private double rtd_Diametro;
	
	private String rti_Marca;
	private double rti_Diametro;
	
	//CONSTRUCTORES
	public Ruedas() {
		
		this.rdd_Marca = "";
		this.rdd_Diametro = 0;
		this.rdi_Marca = "";
		this.rdi_Diametro = 0;
		this.rtd_Marca = "";
		this.rtd_Diametro = 0;
		this.rti_Marca = "";
		this.rti_Diametro = 0;
	}
	public Ruedas(String rdd_Marca, double rdd_Diametro, String rtd_Marca,
			double rtd_Diametro) {
		
		this.rdd_Marca = rdd_Marca;
		this.rdd_Diametro = rdd_Diametro;
		this.rdi_Marca = "";
		this.rdi_Diametro = 0;
		this.rtd_Marca = rtd_Marca;
		this.rtd_Diametro = rtd_Diametro;
		this.rti_Marca = "";
		this.rti_Diametro = 0;
	}
	public Ruedas(String rdd_Marca, double rdd_Diametro, String rdi_Marca, double rdi_Diametro, String rtd_Marca,
			double rtd_Diametro, String rti_Marca, double rti_Diametro) {
		
		this.rdd_Marca = rdd_Marca;
		this.rdd_Diametro = rdd_Diametro;
		this.rdi_Marca = rdi_Marca;
		this.rdi_Diametro = rdi_Diametro;
		this.rtd_Marca = rtd_Marca;
		this.rtd_Diametro = rtd_Diametro;
		this.rti_Marca = rti_Marca;
		this.rti_Diametro = rti_Diametro;
	}

	// GETERS Y SETTERS
	public String getRdd_Marca() {
		return rdd_Marca;
	}

	public void setRdd_Marca(String rdd_Marca) {
		this.rdd_Marca = rdd_Marca;
	}

	public double getRdd_Diametro() {
		return rdd_Diametro;
	}

	public void setRdd_Diametro(double rdd_Diametro) {
		this.rdd_Diametro = rdd_Diametro;
	}

	public String getRdi_Marca() {
		return rdi_Marca;
	}

	public void setRdi_Marca(String rdi_Marca) {
		this.rdi_Marca = rdi_Marca;
	}

	public double getRdi_Diametro() {
		return rdi_Diametro;
	}

	public void setRdi_Diametro(double rdi_Diametro) {
		this.rdi_Diametro = rdi_Diametro;
	}

	public String getRtd_Marca() {
		return rtd_Marca;
	}

	public void setRtd_Marca(String rtd_Marca) {
		this.rtd_Marca = rtd_Marca;
	}

	public double getRtd_Diametro() {
		return rtd_Diametro;
	}

	public void setRtd_Diametro(double rtd_Diametro) {
		this.rtd_Diametro = rtd_Diametro;
	}

	public String getRti_Marca() {
		return rti_Marca;
	}

	public void setRti_Marca(String rti_Marca) {
		this.rti_Marca = rti_Marca;
	}

	public double getRti_Diametro() {
		return rti_Diametro;
	}

	public void setRti_Diametro(double rti_Diametro) {
		this.rti_Diametro = rti_Diametro;
	}
	
	//METODOS
	@Override
	public String toString() {
		return "Ruedas [rdd_Marca=" + rdd_Marca + ", rdd_Diametro=" + rdd_Diametro + ", rdi_Marca=" + rdi_Marca
				+ ", rdi_Diametro=" + rdi_Diametro + ", rtd_Marca=" + rtd_Marca + ", rtd_Diametro=" + rtd_Diametro
				+ ", rti_Marca=" + rti_Marca + ", rti_Diametro=" + rti_Diametro + "]";
	}
	
	public static boolean testDiametro(Double diametro) {
		if(diametro >= 0.4 && diametro <= 4) {
			return true;
		}else {
			return false;
		}
	}
	
	

}
