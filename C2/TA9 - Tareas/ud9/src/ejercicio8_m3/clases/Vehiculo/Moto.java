package ejercicio8_m3.clases.Vehiculo;

import ejercicio8_m3.clases.Persona.Conductor;
import ejercicio8_m3.clases.Persona.Titular;

/**
 * @author David B.
 * @author Xavi B.
 *
 */
public class Moto extends Vehiculo {

	// ATRIBUTOS Y CONSTANTES
	private Ruedas ruedas;

	// CONSTRUCTORES
	public Moto() {
		super();
		this.ruedas = null;
	}

	public Moto(String matricula, String marca, String color) {
		super(matricula, marca, color);
		this.ruedas = null;
	}

	public Moto(String matricula, String marca, String color, Titular titular) {
		super(matricula, marca, color, titular);
		this.ruedas = null;
	}

	public Moto(String matricula, String marca, String color, Titular titular, boolean isTitularConductor,
			Conductor conductor) {
		super(matricula, marca, color, titular, isTitularConductor, conductor);
		this.ruedas = null;
	}

	// GETERS Y SETTERS
	public Ruedas getRuedas() {
		return ruedas;
	}

	public void setRuedas(Ruedas ruedas) {
		this.ruedas = ruedas;
	}

	// METODOS
	@Override
	public String toString() {
		return "Moto [ruedas=" + ruedas + " " + super.toString() + "]";
	}

}
