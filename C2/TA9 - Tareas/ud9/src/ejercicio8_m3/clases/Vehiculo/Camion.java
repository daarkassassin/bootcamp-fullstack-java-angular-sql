package ejercicio8_m3.clases.Vehiculo;

import ejercicio8_m3.clases.Persona.Conductor;
import ejercicio8_m3.clases.Persona.Titular;

/**
 * @author David B.
 * @author Xavi B.
 *
 */
public class Camion extends Vehiculo {

	// ATRIBUTOS Y CONSTANTES
	private Ruedas ruedas;

	// CONSTRUCTORES
	public Camion() {
		super();
		this.ruedas = null;
	}

	public Camion(String matricula, String marca, String color) {
		super(matricula, marca, color);
		this.ruedas = null;
	}

	public Camion(String matricula, String marca, String color, Titular titular) {
		super(matricula, marca, color, titular);
		this.ruedas = null;
	}

	public Camion(String matricula, String marca, String color, Titular titular, boolean isTitularConductor,
			Conductor conductor) {
		super(matricula, marca, color, titular, isTitularConductor, conductor);
		this.ruedas = null;
	}

	public Camion(String matricula, String marca, String color, Ruedas ruedas) {
		super(matricula, marca, color);
		this.ruedas = ruedas;
	}

	// GETERS Y SETTERS
	public Ruedas getRuedas() {
		return ruedas;
	}

	public void setRuedas(Ruedas ruedas) {
		this.ruedas = ruedas;
	}

	// METODOS
	@Override
	public String toString() {
		return "Camion [ruedas=" + ruedas + " " + super.toString() + "]";
	}

}
