package ejercicio2;

public interface Entregable {

	public void entregar(); // cambia el atributo prestado a true

	public void devolver(); // cambia el atributo prestado a false

	public boolean isEntregado(); // devuelve el estado del atributo prestado.

	public boolean compareTo(Object a);// compara las horas estimadas en los videojuegos y en las series el numero de
									// temporadas.Como parámetro que tenga un objeto, no es necesario que
									// implementes la interfaz Comparable. Recuerda el uso de los casting de
									// objetos.

}
