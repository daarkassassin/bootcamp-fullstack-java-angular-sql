package ejercicio2;

class Serie implements Entregable{

	/*
	 * CONSTANTES
	 */
	private final boolean FENTREGADO = false;
	private final int FNUMERO_TEMPORADAS = 3;

	/*
	 * ATRIBUTOS
	 */
	private String titulo;
	private int numeroTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Serie() {
		setTitulo("");
		setNumeroTemporadas(FNUMERO_TEMPORADAS);
		this.entregado = FENTREGADO;
		setGenero("");
		setCreador("");
	}
	public Serie(String titulo, String creador) {
		setTitulo(titulo);
		setNumeroTemporadas(FNUMERO_TEMPORADAS);
		this.entregado = FENTREGADO;
		setGenero("");
		setCreador(creador);
	}
	public Serie(String titulo, String creador, int numeroTemporadas, String genero) {
		setTitulo(titulo);
		setNumeroTemporadas(numeroTemporadas);
		this.entregado = FENTREGADO;
		setGenero(genero);
		setCreador(creador);
	}

	/*
	 * METODOS SET
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setNumeroTemporadas(int numeroTemporadas) {
		this.numeroTemporadas = numeroTemporadas;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	/*
	 * METODOS GET
	 */
	public String getTitulo() {
		return titulo;
	}

	public int getNumeroTemporadas() {
		return numeroTemporadas;
	}

	public String getGenero() {
		return genero;
	}

	public String getCreador() {
		return creador;
	}
	
	/*
	 * METODOS
	 */
	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroTemporadas=" + numeroTemporadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
	
	@Override
	public void entregar() {
		this.entregado = true;
		
	}
	@Override
	public void devolver() {
		this.entregado = false;
		
	}
	@Override
	public boolean isEntregado() {
		return entregado;
	}
	@Override
	public boolean compareTo(Object a) {
		Serie s = (Serie)a;
		int value1 = getNumeroTemporadas();
		int value2 = s.getNumeroTemporadas();
		
		if (value1 > value2) {
			return true; //Si el numero de temporadas de este objeto es mayor que el de "a" devuelve true
		}else {
			return false; //Si el numero de temporadas de este objeto es menor que el de "a" devuelve false
		}
		
	}

}
