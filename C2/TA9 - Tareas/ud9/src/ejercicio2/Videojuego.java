package ejercicio2;

public class Videojuego implements Entregable{
	/*
	 * CONSTANTES
	 */
	private final int DEF_HORAS = 10;
	private final boolean DEF_ENTREGADO = false;

	/*
	 * ATRIBUTOS
	 */
	private String titulo;
	private int horasEstimadas;
	private boolean entregado;
	private String genero;
	private String compa�ia;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Videojuego() {
		this.titulo = "";
		this.horasEstimadas = DEF_HORAS;
		this.entregado = DEF_ENTREGADO;
		this.genero = "";
		this.compa�ia = "";
	}

	public Videojuego(String titulo, int horasEstimadas) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = DEF_ENTREGADO;
		this.genero = "";
		this.compa�ia = "";
	}

	public Videojuego(String titulo, int horasEstimadas, String genero, String compa�ia) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = DEF_ENTREGADO;
		this.genero = genero;
		this.compa�ia = compa�ia;
	}

	/*
	 * METODOS SET
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}

	/*
	 * METODOS SET
	 */
	public String getTitulo() {
		return titulo;
	}

	public int getHorasEstimadas() {
		return horasEstimadas;
	}

	public String getGenero() {
		return genero;
	}

	public String getCompa�ia() {
		return compa�ia;
	}

	/*
	 * METODOS
	 */
	@Override
	public String toString() {
		return "Videojuego [titulo=" + titulo + ", horasEstimadas=" + horasEstimadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", compa�ia=" + compa�ia + "]";
	}

	@Override
	public void entregar() {
		this.entregado = true;
		
	}

	@Override
	public void devolver() {
		this.entregado = false;
		
	}

	@Override
	public boolean isEntregado() {
		return entregado;
	}

	@Override
	public boolean compareTo(Object a) {
		Videojuego s = (Videojuego)a;
		int value1 = getHorasEstimadas();
		int value2 = s.getHorasEstimadas();
		
		if (value1 > value2) {
			return true; //Si el numero de Horas estimadas de este objeto es mayor que el de "a" devuelve true
		}else {
			return false; //Si el numero de Horas estimadas de este objeto es menor que el de "a" devuelve false
		}
		
	}

}
