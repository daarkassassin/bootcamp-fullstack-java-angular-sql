package ejercicio2;

public class mainApp {
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Serie arraySeries[]=new Serie[5];
		Videojuego arrayVideojuego[]=new Videojuego[5];
		
		arraySeries[0] = new Serie("El array","eclipse",10,"Terror");
		arraySeries[1] = new Serie("El string","eclipse",2,"Terror");
		arraySeries[2] = new Serie("El bidimensional","eclipse",1,"Terror");
		arraySeries[3] = new Serie("El teclado","eclipse",8,"Terror");
		arraySeries[4] = new Serie("El raton","eclipse",7,"Terror");
		
		arrayVideojuego[0] = new Videojuego("a",200,"uno","valve");
		arrayVideojuego[1] = new Videojuego("b",100,"dos","valve");
		arrayVideojuego[2] = new Videojuego("c",150,"tres","valve");
		arrayVideojuego[3] = new Videojuego("d",250,"cuatro","valve");
		arrayVideojuego[4] = new Videojuego("e",50,"cinco","valve");

		
		arraySeries[2].entregar();
		arraySeries[4].entregar();
		arraySeries[0].entregar();
		
		arrayVideojuego[1].entregar();
		arrayVideojuego[3].entregar();
		
		int seriesEntregadas = 0;
		for(int i = 0; arraySeries.length > i; i++) {
			if(arraySeries[i].isEntregado()) {
				arraySeries[i].devolver();
				seriesEntregadas++;
			}
		}
		
		int videojuegosEntregados = 0;
		for(int i = 0; arrayVideojuego.length > i; i++) {
			if(arrayVideojuego[i].isEntregado()) {
				arrayVideojuego[i].devolver();
				videojuegosEntregados++;
			}
		}
		
		
		int pos = 0;
		for(int i = 0; arrayVideojuego.length > i; i++) {
			if(!arrayVideojuego[pos].compareTo(arrayVideojuego[i])) {
				pos = i;
			}
		}
		System.out.println(arrayVideojuego[pos].toString());
		
		pos = 0;
		for(int i = 0; arraySeries.length > i; i++) {
			if(!arraySeries[pos].compareTo(arraySeries[i])) {
				pos = i;
			}
		}
		System.out.println(arraySeries[pos].toString());
		
		
	}
	
}
