package ejercicio1;

class Electrodomestico {
	/*
	 * CONSTANTES
	 */
	protected final static float PRECIO_BASE = 100;
	protected final static String FCOLOR = "blanco";
	protected final static char CONSUMO_ENERGETICO = 'F';
	protected final static float PESO = 5;

	/*
	 * ATRIBUTOS
	 */
	protected float precioBase;
	protected String color;
	protected char consumoEnergetico;
	protected float peso;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Electrodomestico() {
		setPrecioBase(PRECIO_BASE);
		setColor(FCOLOR);
		setConsumoEnergetico(CONSUMO_ENERGETICO);
		setPeso(PESO);
	}

	public Electrodomestico(float precioBase, float peso) {
		setPrecioBase(precioBase);
		setColor(FCOLOR);
		setConsumoEnergetico(CONSUMO_ENERGETICO);
		setPeso(peso);
	}

	public Electrodomestico(float precioBase, float peso, String color, char consumoEnergetico) {
		setPrecioBase(precioBase);
		comprobarColor(color);
		comprobarConsumoEnergetico(consumoEnergetico);
		setPeso(peso);
	}

	/*
	 * METODOS SET
	 */
	public void setPrecioBase(float precio_base) {
		this.precioBase = precio_base;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setConsumoEnergetico(char consumo_energetico) {
		this.consumoEnergetico = consumo_energetico;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	/*
	 * METODOS GET
	 */
	public float getPrecioBase() {
		return precioBase;
	}

	public String getColor() {
		return color;
	}

	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	public float getPeso() {
		return peso;
	}

	/*
	 * METODOS PRIVADOS
	 */

	private void comprobarConsumoEnergetico(char letra) {
		char opc = Character.toUpperCase(letra);
		switch (opc) {
		case 'A':
			setConsumoEnergetico(letra);
			break;

		case 'B':
			setConsumoEnergetico(letra);
			break;

		case 'C':
			setConsumoEnergetico(letra);
			break;

		case 'D':
			setConsumoEnergetico(letra);
			break;

		case 'E':
			setConsumoEnergetico(letra);
			break;
			
		case 'F':
			setConsumoEnergetico(letra);
			break;

		default:
			setConsumoEnergetico(CONSUMO_ENERGETICO);
		}

	}

	private void comprobarColor(String color) {
		String opc = color.toLowerCase();
		switch (opc) {
		case "blanco":
			setColor(color);
			break;

		case "negro":
			setColor(color);
			break;

		case "rojo":
			setColor(color);
			break;

		case "azul":
			setColor(color);
			break;

		case "gris":
			setColor(color);
			break;

		default:
			setColor(FCOLOR);
		}

	}

	/*
	 * METODOS
	 */

	public float precioFinal() {
		char letra = getConsumoEnergetico();
		float tama�o = getPeso();
		float precio = getPrecioBase();
		
		if (letra == 'A') {
			precio = precio + 100;
		}else if (letra == 'B') {
			precio = precio + 80;
		}else if (letra == 'C') {
			precio = precio + 60;
		}else if (letra == 'D') {
			precio = precio + 50;
		}else if (letra == 'E') {
			precio = precio + 30;
		}else if (letra == 'F') {
			precio = precio + 10;
		}
		
		if (tama�o <= 19) {
			precio = precio + 10;
		}else if (tama�o <= 49) {
			precio = precio + 50;
		}else if (tama�o <= 79) {
			precio = precio + 80;
		}else {
			precio = precio + 100;
		}
		
		return precio;
	}

}
