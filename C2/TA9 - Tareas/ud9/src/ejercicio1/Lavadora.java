package ejercicio1;

class Lavadora extends Electrodomestico{
	/*
	 * CONSTANTES
	 */
	private final int FCARGA = 5;
	/*
	 * ATRIBUTOS
	 */
	private int carga;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Lavadora() {
		super(PRECIO_BASE,PESO,FCOLOR,CONSUMO_ENERGETICO);
		setCarga(FCARGA);
	}

	public Lavadora(float precioBase, float peso) {
		super(precioBase,peso,FCOLOR,CONSUMO_ENERGETICO);
		setCarga(FCARGA);
	}

	public Lavadora(int carga, float precioBase, float peso, String color, char consumoEnergetico) {
		super(precioBase,peso,color,consumoEnergetico);
		setCarga(carga);
	}

	/*
	 * METODOS SET
	 */
	public void setCarga(int carga) {
		this.carga = carga;
	}

	/*
	 * METODOS GET
	 */
	public float getCarga() {
		return carga;
	}


	/*
	 * METODOS
	 */

	public float precioFinal() {
		float precio = super.precioFinal();		
		if (carga > 30) {
			precio = precio + 50;
		}
		return precio;
	}

}
