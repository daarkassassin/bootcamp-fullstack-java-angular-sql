package ejercicio1;

class Television extends Electrodomestico {
	/*
	 * CONSTANTES
	 */
	private final int FRESOLUCION = 20;
	private boolean FTDT = false;
	/*
	 * ATRIBUTOS
	 */
	private int resolucion;
	private boolean tdt;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Television() {
		super(PRECIO_BASE, PESO, FCOLOR, CONSUMO_ENERGETICO);
		setResolucion(FRESOLUCION);
		setTdt(FTDT);
	}

	public Television(float precioBase, float peso) {
		super(precioBase, peso, FCOLOR, CONSUMO_ENERGETICO);
		setResolucion(FRESOLUCION);
		setTdt(FTDT);
	}

	public Television(int resolucion, boolean tdt, float precioBase, float peso, String color, char consumoEnergetico) {
		super(precioBase, peso, color, consumoEnergetico);
		setResolucion(resolucion);
		setTdt(tdt);
	}

	/*
	 * METODOS SET
	 */
	public void setResolucion(int resolucion) {
		this.resolucion = resolucion;
	}

	public void setTdt(boolean tdt) {
		this.tdt = tdt;
	}

	/*
	 * METODOS GET
	 */
	public float getResolucion() {
		return resolucion;
	}

	public boolean getTdt() {
		return tdt;
	}

	/*
	 * METODOS
	 */

	public float precioFinal() {
		float precio = super.precioFinal();
		if (resolucion > 40) {
			precio = (float)(precio + (precio * 0.3));
		}
		if (tdt) {
			precio = precio + 50;
		}
		return precio;
	}

	
	

}
