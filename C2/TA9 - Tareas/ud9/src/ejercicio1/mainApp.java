package ejercicio1;

public class mainApp {

	public static void main(String[] args) {
		
		Electrodomestico arrayElectrodomesticos[]=new Electrodomestico[10];
		
		arrayElectrodomesticos[0]=new Lavadora();
        arrayElectrodomesticos[1]=new Lavadora(400, 100);
        arrayElectrodomesticos[2]=new Lavadora(45, 400, 50,"negro", 'C');
        arrayElectrodomesticos[3]=new Lavadora(20, 400, 85,"blanco", 'A');
        arrayElectrodomesticos[4]=new Lavadora(400, 64);
        arrayElectrodomesticos[5]=new Television();
        arrayElectrodomesticos[6]=new Television(600, 20);
        arrayElectrodomesticos[7]=new Television(65, true, 600, 105, "negro", 'A');
        arrayElectrodomesticos[8]=new Television(20, false, 600, 30, "negro", 'F');
        arrayElectrodomesticos[9]=new Television(600, 45);
        
        float precioElectrodomesticos = 0;
        float precioLavadoras = 0;
        float precioTelevisiones = 0;
        
        for(int i = 0; arrayElectrodomesticos.length > i; i++) {
        	if(arrayElectrodomesticos[i] instanceof Lavadora) {
        		precioLavadoras = precioLavadoras + arrayElectrodomesticos[i].precioFinal();
        		precioElectrodomesticos = precioElectrodomesticos + arrayElectrodomesticos[i].precioFinal();
        	}
        	
        	if(arrayElectrodomesticos[i] instanceof Television) {
        		precioTelevisiones = precioTelevisiones + arrayElectrodomesticos[i].precioFinal();
        		precioElectrodomesticos = precioElectrodomesticos + arrayElectrodomesticos[i].precioFinal();	
        	}
        	
        }
        
        System.out.println();
        System.out.println("Precio Electrodomesticos: "+precioElectrodomesticos+"�");
        System.out.println("Precio Lavadoras: "+precioLavadoras+"�");
        System.out.println("Precio Televisiones: "+precioTelevisiones+"�");
	}

}
