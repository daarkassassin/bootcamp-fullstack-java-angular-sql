package ejercicio6;

public class Asiento extends Cine{
	
	/*
	 * ATRIBUTOS y CONSTANTES
	 */
	private boolean disponible;
	private int fila;
	private char columna;
	private Espectador espectador;

	/*
	 * CONSTRUCTORES
	 */
	public Asiento(char letra, int fila) {
		super();
		this.disponible = true;
		this.fila = fila;
		this.columna = letra;
		this.espectador = null;
	}

	/*
	 * GETERS y SETERS
	 */
	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public char getColumna() {
		return columna;
	}

	public void setColumna(char columna) {
		this.columna = columna;
	}

	public Espectador getEspectador() {
		return espectador;
	}

	public void setEspectador(Espectador espectador) {
		this.espectador = espectador;
	}

	@Override
	public String toString() {
		return "Asiento [disponible=" + disponible + ", fila=" + fila + ", Letra=" + columna + ", espectador="
				+ espectador + ", precioEntrada=" + precioEntrada + "]";
	}
	
	/*
	 * METODOS
	 */
	
	

	
	

	
	

}
