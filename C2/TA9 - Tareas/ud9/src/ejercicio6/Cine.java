package ejercicio6;

public class Cine {
	
	/*
	 * ATRIBUTOS y CONSTANTES
	 */
	private final double PRECIO = 10;
	protected double precioEntrada;
	
	/*
	 * CONSTRUCTORES
	 */
	public Cine() {
		this.precioEntrada = PRECIO;
	}

	/*
	 * GETERS y SETERS
	 */
	public double getPrecioEntrada() {
		return precioEntrada;
	}
	
	/*
	 * METODOS
	 */

}
