package ejercicio6;

import java.util.Random;

public class mainApp {

	public static void main(String[] args) {
		final int FILAS = 8;
		final int COLUMNAS = 9;
		final int HUECOS = FILAS * COLUMNAS;
		int huecoslibres = HUECOS;
		Random r = new Random();
		
		
		//Generamos la sala del cine.
		Asiento asientos[] [] = new Asiento [FILAS] [COLUMNAS];
		for(int i = 0; i < asientos.length; i++){
			int z = 1;
			for(int x = 0; x < asientos[i].length; x++){
				
				char letra = '\u0000';
                int numFila = i + 1;
                
                if (z == 1) {
                    letra = 'A';
                } else if (z == 2) {
                    letra = 'B';
                } else if (z == 3) {
                    letra = 'C';
                } else if (z == 4) {
                    letra = 'D';
                } else if (z == 5) {
                    letra = 'E';
                } else if (z == 6) {
                    letra = 'F';
                } else if (z == 7) {
                    letra = 'G';
                } else if (z == 8) {
                    letra = 'H';
                } else if (z == 9) {
                    letra = 'I';
                }
                
                asientos[i][x] = new Asiento(letra, numFila);
                z++;
                
			}
		}
		//Generamos la pelicula.
		Pelicula pelicula = new Pelicula("El bucle infinito",99999,18,"Eclipse");
		
		//Generamos los espectadores y los sentamos aleatoriamente si tienen dinero.
		boolean cineLleno = false;
		int contador = 0;
		while(!cineLleno) {
			boolean sentado = false;
			
			//Entra una persona
			Espectador espectador = new Espectador("persona "+contador,r.nextInt(90),r.nextInt(40));
			contador++;
			
			//Intentamos Sentarla
			while(!sentado) {
				if(huecoslibres > 0) { //Solo mientras hayan huecos en el cine.
					if(espectador.getDinero() >= pelicula.getPrecioEntrada() && espectador.getEdad() > pelicula.getEdadMinima()) { //El espectador ha de tener dinero y una edad minima.
						espectador.setDinero(espectador.getDinero()-pelicula.getPrecioEntrada());
						int fila = r.nextInt(FILAS);
						int columna = r.nextInt(COLUMNAS);
						if(asientos [fila][columna].isDisponible()) {
							asientos [fila][columna].setEspectador(espectador);
							asientos [fila][columna].setDisponible(false);
							huecoslibres--;
							sentado = true;
						}
					}else{
						sentado = true;
					}
				}else {
					sentado = true;
				}
			}
			
			//Si llenamos el cine. Salimos del bucle.
			if (huecoslibres <= 0) {cineLleno = true;} 
		}
		
		
		//Imprimir asientos llenos
		for(int i1 = 0; i1 < asientos.length; i1++){
			for(int x = 0; x < asientos[i1].length; x++){
				System.out.println(asientos [i1][x].toString());
			}
		}
	}

}
