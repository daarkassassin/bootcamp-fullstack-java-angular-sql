package ejercicio6;

public class Pelicula extends Cine {
	
	/*
	 * ATRIBUTOS y CONSTANTES
	 */
	private String titulo;
	private int duracion;
	private int edadMinima;
	private String director;
	
	/*
	 * CONSTRUCTORES
	 */
	public Pelicula() {
		super();
		this.titulo = "";
		this.duracion = 0;
		this.edadMinima = 0;
		this.director = "";
	}

	public Pelicula(String titulo, int duracion, int edadMinima, String director) {
		super();
		this.titulo = titulo;
		this.duracion = duracion;
		this.edadMinima = edadMinima;
		this.director = director;
	}

	/*
	 * GETERS y SETERS
	 */
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getEdadMinima() {
		return edadMinima;
	}

	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}
	
	/*
	 * METODOS
	 */
	
	
	
	

}
