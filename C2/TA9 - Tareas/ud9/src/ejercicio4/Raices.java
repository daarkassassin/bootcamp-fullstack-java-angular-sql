package ejercicio4;

/**
 * @author David Bonet
 *
 */
import static java.lang.Math.*;

public class Raices {

	private double a;
	private double b;
	private double c;

	public Raices(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public void obtenerRaices() {
		double x1, x2;
		if(tieneRaices()){
			x1 = (-b + sqrt(getDiscriminante())) / (2 * a);
            x2 = (-b - sqrt(getDiscriminante())) / (2 * a);
            System.out.printf("La ra�z real x1 es %.3f%n", x1);
            System.out.printf("La ra�z real x2 es %.3f%n", x2);
		}
	}

	public void obtenerRaiz() {
		double x;
		if(tieneRaiz()){
			x = -b / (2 * a);
	        System.out.printf("La ra�z �nica es %.3f%n", x);
		}
		
	}
	
	public double getDiscriminante() {
		double d = (pow(b, 2) - 4 * a * c);
		return d;
	}
	
	public boolean tieneRaices() { // Si D > 0, entonces la ecuaci�n tiene dos soluciones reales distintas.
		double d = getDiscriminante();
		if(d>0){
			return true;
		}else {
			return false;
		}
	}
	
	public boolean tieneRaiz() { //Si D = 0, entonces la ecuaci�n tiene exactamente una soluci�n real.
		double d = getDiscriminante();
		if(d == 0){
			return true;
		}else {
			return false;
		}
	}
	
	public void calcular() {
		if (getDiscriminante() >= 0) {
            if (tieneRaiz()) { //Si D = 0, entonces la ecuaci�n tiene exactamente una soluci�n real.
                double x = -b / (2 * a);
                System.out.printf("La ra�z �nica es %.3f%n", x);
            }
            
            if(tieneRaices()){ // Si D > 0, entonces la ecuaci�n tiene dos soluciones reales distintas.
                double x1, x2;
                x1 = (-b + sqrt(getDiscriminante())) / (2 * a);
                x2 = (-b - sqrt(getDiscriminante())) / (2 * a);
                System.out.printf("La ra�z real x1 es %.3f%n", x1);
                System.out.printf("La ra�z real x2 es %.3f%n", x2);
            }
        } else { // Si D < 0, entonces la ecuaci�n no tiene soluci�n real. (pero si soluci�n compleja)
        	System.out.println("No tiene soluci�n real");
        }
		
	}
	
}
