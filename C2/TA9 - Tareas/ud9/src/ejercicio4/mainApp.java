package ejercicio4;

/**
 * @author David Bonet
 *
 */

public class mainApp {

	public static void main(String[] args) {
		System.out.println("> 2, 4, 8");
		Raices raiz1 = new Raices(2,4,8);
		raiz1.calcular();
		
		System.out.println("> 1, 6, 1");
		Raices raiz2 = new Raices(1,6,1);
		raiz2.calcular();

	}

}
