package ejercicio7_m2.Clases;

public class Volunteer extends Empleado {

	private double ayudaLimpiaMensual;
	private double ayudaLimpiaAnual;

	public Volunteer() {
		super();
	}

	public Volunteer(String nom, String tipus, String dni, double ajutNetMensual) {
		super(nom, tipus, dni);
		this.ayudaLimpiaMensual = ajutNetMensual;
	}

	public double getAyudaLimpiaMensual() {
		return ayudaLimpiaMensual;
	}

	public void setAyudaLimpiaMensual(double ayudaLimpiaMensual) {
		this.ayudaLimpiaMensual = ayudaLimpiaMensual;
	}

	public double getAyudaLimpiaAnual() {
		return ayudaLimpiaAnual;
	}

	public void setAyudaLimpiaAnual(double ayudaLimpiaAnual) {
		this.ayudaLimpiaAnual = ayudaLimpiaAnual;
	}

	@Override
	public String toString() {
		return "Volunteer [ Nombre = " + nombre + " | Apellido = " + apellido + " | DNI = " + dni
				+ " | Ayuda Limpia Mensual = " + ayudaLimpiaMensual + " | Ayuda Limpia Anual = " + ayudaLimpiaAnual
				+ " ]";
	}

	public void calcSalario() {
		double ajutNetMensual = this.ayudaLimpiaMensual;
		this.ayudaLimpiaAnual = ajutNetMensual * 12;
	}

	public static boolean validarAyudaVolunteer(double ayuda) {
		if (ayuda <= 300 && ayuda >= 0) {
			return true;
		}
		return false;
	}

	public double getSalarioLimpioAnual() {
		return 0;
	}

}
