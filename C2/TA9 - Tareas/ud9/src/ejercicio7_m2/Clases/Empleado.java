package ejercicio7_m2.Clases;

public abstract class Empleado {

	protected String nombre;
	protected String apellido;
	protected String dni;

	public Empleado() {
		this.nombre = "";
		this.apellido = "";
		this.dni = "";
	}

	public Empleado(String nom, String cognom, String dni) {
		this.nombre = nom;
		this.apellido = cognom;
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nom) {
		this.nombre = nom;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String cognom) {
		this.apellido = cognom;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	@Override
	public String toString() {
		return "Empleado [Nombre=" + nombre + ", Apellido=" + apellido + ", DNI=" + dni + "]";
	}

	public abstract void calcSalario();

	public abstract double getSalarioLimpioAnual();

}
