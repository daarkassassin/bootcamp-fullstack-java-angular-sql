package ejercicio7_m2.Clases;

public class Boss extends Empleado {

	private double salarioLimpioMensual;
	private double salarioSucioMensual;
	private double salarioLimpioAnual;
	private double salarioSucioAnual;

	private final double INCREMENT_SALARI_BRUT = 0.50;
	private final double REDUCCIO_IRPF_SALARI_BRUT = 0.32;

	public Boss() {
		super();
	}

	public Boss(String nombre, String apellido, String dni, double x) {
		super(nombre, apellido, dni);
		this.salarioSucioMensual = x;
	}

	public double getSalarioLimpioMensual() {
		return salarioLimpioMensual;
	}

	public void setSalarioLimpioMensual(double salarioLimpioMensual) {
		this.salarioLimpioMensual = salarioLimpioMensual;
	}

	public double getSalarioSucioMensual() {
		return salarioSucioMensual;
	}

	public void setSalarioSucioMensual(double salarioSucioMensual) {
		this.salarioSucioMensual = salarioSucioMensual;
	}

	public double getSalarioLimpioAnual() {
		return salarioLimpioAnual;
	}

	public void setSalarioLimpioAnual(double salarioLimpioAnual) {
		this.salarioLimpioAnual = salarioLimpioAnual;
	}

	public double getSalarioSucioAnual() {
		return salarioSucioAnual;
	}

	public void setSalarioSucioAnual(double salarioSucioAnual) {
		this.salarioSucioAnual = salarioSucioAnual;
	}

	@Override
	public String toString() {
		return "Boss [ Nombre = " + nombre + " | Apellido = " + apellido + " | DNI = " + dni
				+ " | Salario Sucio Mensual = " + salarioSucioMensual + " | Salario Limpio Mensual = "
				+ salarioLimpioMensual + " | Salari Sucio Anual = " + salarioSucioAnual + " | Salario Limpio Anual="
				+ salarioLimpioAnual + " ]";
	}

	public void calcSalario() {
		double sbm = this.salarioSucioMensual;
		double nou_sbm = (sbm * INCREMENT_SALARI_BRUT) + sbm;
		this.salarioSucioMensual = nou_sbm;
		this.salarioLimpioMensual = nou_sbm - (nou_sbm * REDUCCIO_IRPF_SALARI_BRUT);
		this.salarioSucioAnual = this.salarioSucioMensual * 12;
		this.salarioLimpioAnual = this.salarioLimpioMensual * 12;
	}

	public static boolean validarSalariBoss(double salario) {
		if (salario >= 8000) {
			return true;
		}
		return false;
	}

}
