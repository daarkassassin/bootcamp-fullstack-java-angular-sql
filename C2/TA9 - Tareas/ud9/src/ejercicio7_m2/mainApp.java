package ejercicio7_m2;

import java.util.ArrayList;
import ejercicio7_m2.Clases.Boss;
import ejercicio7_m2.Clases.Empleado;
import ejercicio7_m2.Clases.Junior;
import ejercicio7_m2.Clases.Manager;
import ejercicio7_m2.Clases.Mid;
import ejercicio7_m2.Clases.Senior;
import ejercicio7_m2.Clases.Volunteer;
import ejercicio7_m2.Metodos.Metodos;

public class mainApp {

	public static void main(String[] args) {

		// Lista de empleados
		ArrayList<Empleado> ListaEmpleados = new ArrayList<Empleado>();

		// Creamos un empleado Boss
		Empleado empleado1 = new Boss("Name1", "Boss", "12345678Z", Metodos.enterSalarioBoss());
		empleado1.calcSalario();
		ListaEmpleados.add(empleado1);

		// Creamos un empleado Manager
		Empleado empleado2 = new Manager("Name2", "Manager", "12345678Z", Metodos.enterSalarioManager());
		empleado2.calcSalario();
		ListaEmpleados.add(empleado2);

		// Creamos un empleado Senior
		Empleado empleado3 = new Senior("Name3", "Senior", "12345678Z", Metodos.enterSalarioSenior());
		empleado3.calcSalario();
		ListaEmpleados.add(empleado3);

		// Creamos un empleado Mid
		Empleado empleado4 = new Mid("Name4", "Mid", "12345678Z", Metodos.enterSalarioMid());
		empleado4.calcSalario();
		ListaEmpleados.add(empleado4);

		// Creamos un empleado Junior
		Empleado empleado5 = new Junior("Name5", "Junior", "12345678Z", Metodos.enterSalarioJunior());
		empleado5.calcSalario();
		ListaEmpleados.add(empleado5);

		// Creamos un empleado Volunteer
		Empleado empleado6 = new Volunteer("Name6", "Volunteer", "12345678Z", Metodos.enterSalarioVolunteer());
		empleado6.calcSalario();
		ListaEmpleados.add(empleado6);

		// Listamos los empleados y mostramos el bono.
		Metodos.ListarEmpleados(ListaEmpleados);

	}

}
