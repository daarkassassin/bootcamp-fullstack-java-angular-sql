package ejercicio7_m2.Metodos;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

import ejercicio7_m2.Clases.Boss;
import ejercicio7_m2.Clases.Empleado;
import ejercicio7_m2.Clases.Junior;
import ejercicio7_m2.Clases.Manager;
import ejercicio7_m2.Clases.Mid;
import ejercicio7_m2.Clases.Senior;
import ejercicio7_m2.Clases.Volunteer;

public class Metodos {

	// Introducir el salario del BOSS y validarlo
	public static double enterSalarioBoss() {
		double salarioBoss = 0;
		boolean validarBoss = false;
		while (!validarBoss) {
			salarioBoss = getDouble("Introduece el salario del JEFE: [mas de 8000]");
			validarBoss = Boss.validarSalariBoss(salarioBoss);
			if (!validarBoss) {
				System.out.println("Error: El salario del JEFE ha de ser mas de 8000");
			}
		}
		return salarioBoss;
	}

	// Introducir el salario del MANAGER y validarlo
	public static double enterSalarioManager() {
		double salarioManager = 0;
		boolean validarManager = false;
		System.out.println();
		while (!validarManager) {
			salarioManager = getDouble("Introduce el salario del GERENTE: [mas de 3000 y menos de 5000]");
			validarManager = Manager.validarSalariManager(salarioManager);
			if (!validarManager) {
				System.out.println("Error: El salaro del GERENTE ha de ser mas 3000 y menos de 5000");
			}
		}
		return salarioManager;
	}

	// Introducir el salario del PROGRAMADOR SENIOR y validarlo
	public static double enterSalarioSenior() {
		double salarioSenior = 0;
		boolean validarSenior = false;
		while (!validarSenior) {
			salarioSenior = getDouble("Introduce el salari del PROGRAMADOR SENIOR: [mas de 2700 y menos de 4000]");
			validarSenior = Senior.validarSalariSenior(salarioSenior);
			if (!validarSenior) {
				System.out.println("Error: El salario del PROGRAMADOR SENIOR ha de ser mas de 2700 y menos de 4000");
			}
		}
		return salarioSenior;
	}

	// Introducir el salario del PROGRAMADOR MID y validarlo
	public static double enterSalarioMid() {
		double salarioMid = 0;
		boolean validarMid = false;
		while (!validarMid) {
			salarioMid = getDouble("Introduce el salario del PROGRAMADOR MID: [mas de 1800 y menos de 2500]");
			validarMid = Mid.validarSalariMid(salarioMid);
			if (!validarMid) {
				System.out.println("Error: El salario del PROGRAMADOR MID ha de ser de mes de 1800 y menys de 2500");
			}
		}
		return salarioMid;
	}

	// Introducir el salario del PROGRAMADOR JUNIOR y validarlo
	public static double enterSalarioJunior() {
		double salarioJunior = 0;
		boolean validarJunior = false;
		while (!validarJunior) {
			salarioJunior = getDouble("Introduce el salario del PROGRAMADOR JUNIOR: [mas de 900 y menos de 1600]");
			validarJunior = Junior.validarSalariJunior(salarioJunior);
			if (!validarJunior) {
				System.out.println("Error: El salario del PROGRAMADOR JUNIOR ha de ser mas de 900 y menos de 1600");
			}
		}
		return salarioJunior;
	}

	// Introducir el salario del Volunteer y validarlo
	public static double enterSalarioVolunteer() {
		double ayudaVolunteer = 0;
		boolean validarVolunteer = false;
		while (!validarVolunteer) {
			ayudaVolunteer = getDouble("Introduce la ayuda del VOLUNTEER: [max 300]");
			validarVolunteer = Volunteer.validarAyudaVolunteer(ayudaVolunteer);
			if (!validarVolunteer) {
				System.out.println("Error: La ayuda del VOLUNTEER ha de ser de 300 com a maximo");
			}
		}
		return ayudaVolunteer;
	}

	// Listar todos los empleados
	public static void ListarEmpleados(ArrayList<Empleado> listaEmpleados) {
		Iterator<Empleado> iterator = listaEmpleados.iterator();
		Empleado item;
		System.out.println("");
		System.out.println(
				"# Lista Empleados =============================================================================================================== ");
		System.out.println("");
		while (iterator.hasNext()) {
			item = iterator.next();
			System.out.println(item.toString());

			// Le damos bono a todos los empleados menos al voluntario
			boolean bonusEmpleados = item instanceof Volunteer;
			if (!bonusEmpleados) {
				double salarioAnual = item.getSalarioLimpioAnual();
				darBono(salarioAnual);
			}

			System.out.println("");
		}

	}

	// Dar bono a los empleados
	public static double darBono(double salariAnual) {
		double bono = salariAnual * 0.10;
		System.out.println("Bono: " + bono);
		return bono;
	}

	// Entrada de datos
	public static double getDouble(String s) {
		double n = 0;
		try {
			n = Double.parseDouble(JOptionPane.showInputDialog(s));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			n = Double.NaN;
		}
		return n;
	}

}
