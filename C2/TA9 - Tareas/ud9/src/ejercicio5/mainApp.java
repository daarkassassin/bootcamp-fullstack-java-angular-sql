package ejercicio5;

import java.util.Random;

/**
 * @author David
 *
 */
public class mainApp {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random r = new Random();
		
		String[] nombresH = {"Ricardo", "Xavi", "Manuel", "Abraham", "David", "Jos�", "Ivan", "Juan", "Ignazio", "Ferran"};
		String[] nombresM = {"Anna", "Marta", "Paula", "Laura", "Lara", "Xenia", "Alba", "Canela", "Isabel", "Mar"};
		String[] asignaturas = {"matem�ticas","filosof�a","f�sica"};
		
		int tama�oAula = r.nextInt(50); //Definimos el tama�o del aula de forma random
		
		//Asignamos todos los estudiantes con sus notas
		Estudiante estudiantes[] = new Estudiante[tama�oAula];
		for(int i = 0; estudiantes.length > i; i++) {
			if(r.nextInt(10) <= 5) {
				estudiantes[i] = new Estudiante(nombresH[r.nextInt(9)],11,'H',r.nextInt(10));
			}else {
				estudiantes[i] = new Estudiante(nombresM[r.nextInt(9)],10,'M',r.nextInt(10));
			}
        }
		
		//Asignamos al profesor y su asignatura
		Profesor p1;
		if(r.nextInt(10) <= 5) {
			p1 = new Profesor(nombresH[r.nextInt(9)],35,'H',asignaturas[r.nextInt(2)]);
		}else {
			p1 = new Profesor(nombresM[r.nextInt(9)],32,'M',asignaturas[r.nextInt(2)]);
		}
		
		//Definimos el aula y de forma random que asignatura toca
		Aula aula1 = new Aula(1,tama�oAula,asignaturas[r.nextInt(2)],p1,estudiantes);
		
		
		System.out.println(aula1.toString());
		
		//Imprimimos si se podra dar clase o no. En caso afirmativo mostramos por separado, cuantos alumnos y alumnas estan aprobados.
		System.out.println("Puede dar clase? ");
		aula1.entregarNotas();
		

	}

}
