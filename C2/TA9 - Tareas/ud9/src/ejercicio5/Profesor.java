package ejercicio5;

import java.util.Random;

/**
 * @author David
 *
 */

public class Profesor extends Persona {
	
	/*
	 * CONSTANTES
	 */
	public final int INDICE_ASISTENCIA = 20;

	/*
	 * ATRIBUTOS
	 */
	private String materia;

	/*
	 * METODOS CONSTRUCTORES
	 */
	public Profesor() {
		super();
		this.materia = "";
	}
	
	public Profesor(String nombre, int edad, char sexo, String materia) {
		super(nombre, edad, sexo);
		this.materia = materia;
	}

	public Profesor(String nombre, int edad, char sexo, boolean disponible, String materia) {
		super(nombre, edad, sexo, disponible);
		this.materia = materia;
	}

	/*
	 * METODOS SET
	 */
	public void setMateria(String materia) {
		this.materia = materia;
	}

	/*
	 * METODOS GET
	 */
	public String getMateria() {
		return materia;
	}

	/*
	 * METODOS
	 */
	@Override
	public boolean assistenciaSatisfactoria() {
		Random r = new Random(); //Clase random con la que generaremos int's.
		if(r.nextInt(100) < INDICE_ASISTENCIA) {
			return false;
		}else {
			return true;
		}
		
	}

	@Override
	public String toString() {
		return "Profesor [materia=" + materia + ", " + super.toString() + "]";
	}

}
