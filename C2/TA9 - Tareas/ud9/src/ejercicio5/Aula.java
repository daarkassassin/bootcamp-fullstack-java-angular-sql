package ejercicio5;

import java.util.Arrays;

public class Aula {
	/*
	 * CONSTANTES
	 */
	
	/*
	 * ATRIBUTOS
	 */
	private int id; //Identificador del aula
	private int huecos; //Maximo de alumnos que puede tener el aula.
	private String materia; //Materia que se da en el aula
	private Profesor profesor; //(obj) profesor
	private Estudiante[] estudiantes; //(obj)[] Estudiante
	
	/*
	 * METODOS CONSTRUCTORES
	 */
	public Aula() {
		this.id = 0;
		this.huecos = 0;
		this.materia = "";
		this.profesor = new Profesor();
		this.estudiantes = new Estudiante[0];
	}
	
	public Aula(int id, int huecos, String materia, Profesor profesor, Estudiante[] estudiante) {
		this.id = id;
		this.huecos = huecos;
		this.materia = materia;
		this.profesor = profesor;
		this.estudiantes = estudiante;
	}

	
	
	/*
	 * METODOS
	 */
	public boolean puedeDarClase() {
		if(profesor.getDisponible()) {
			if(materia == profesor.getMateria()) {
				boolean alumnos50 = false;
				int huecosLlenos = 0;
				int huecos50 = (int) (huecos * 0.5);
				
				for(int i = 0; estudiantes.length > i; i++) {
					if(estudiantes[i].getDisponible()) {
						huecosLlenos++;
					}
		        }
				
				if(huecosLlenos > huecos50) {
					alumnos50 = true;
				}
				
				if(alumnos50) {
					System.out.println("Si. Buenos dias ni�os! :D");
					return true;
				}else {
					System.out.println("No. Hay menos de un 50% de los alumnos en clase.");
					return false;
				}
			}else {
				System.out.println("No. La materia del profesor no corresponde con la del aula.");
				return false;
			}
		}else {
			System.out.println("No. El profesor no est� disponible.");
			return false;
		}
	}
	
	public void entregarNotas() {
		if(puedeDarClase()) {
			int alumnos = 0;
			int alumnosAprobados = 0;
			int alumnas = 0;
			int alumnasAprobadas = 0;
			for(int i = 0; estudiantes.length > i; i++) {
				if(estudiantes[i].getSexo() == 'H') {
					if (estudiantes[i].getNota() >= 5) {
						alumnosAprobados++;
					}
					alumnos++;
				}else{
					if (estudiantes[i].getNota() >= 5) {
						alumnasAprobadas++;
					}
					alumnas++;
				}
	        }
			System.out.println("Alumnos aprobados de momento [ "+alumnosAprobados+" / "+alumnos+" ]");
			System.out.println("Alumnas aprobadas de momento [ "+alumnasAprobadas+" / "+alumnas+" ]");
		}
	}
	
	@Override
	public String toString() {
		return "Aula { id=" + id + ", huecos=" + huecos + ", materia=" + materia + ", profesor=" + profesor
				+ ", estudiantes=" + Arrays.toString(estudiantes) + " } ";
	}
	
	
	
	

	

}
