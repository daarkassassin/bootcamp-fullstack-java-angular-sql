package ejercicio5;

/**
 * @author David
 *
 */

import java.util.Random;

public class Estudiante extends Persona {
	
	/*
	 * CONSTANTES
	 */
	public final int INDICE_ASISTENCIA = 50;
	/*
	 * ATRIBUTOS
	 */
	private int nota;

	/*
	 * METODOS CONSTRUCTORES
	 */
	public Estudiante() {
		super();
		this.nota = 0;
	}
	
	public Estudiante(String nombre, int edad, char sexo, int nota) {
		super(nombre, edad, sexo);
		this.nota = nota;
	}
	
	public Estudiante(String nombre, int edad, char sexo, boolean disponible, int nota) {
		super(nombre, edad, sexo, disponible);
		this.nota = nota;
	}




	/*
	 * METODOS SET
	 */
	public void setNota(int nota) {
		this.nota = nota;
	}

	/*
	 * METODOS GET
	 */
	public int getNota() {
		return nota;
	}

	/*
	 * METODOS
	 */
	@Override
	public boolean assistenciaSatisfactoria() {
		Random r = new Random(); //Clase random con la que generaremos int's.		
		if(r.nextInt(100) < INDICE_ASISTENCIA) {
			return false;
		}else {
			return true;
		}
		
	}

	@Override
	public String toString() {
		return "Estudiante [nota=" + nota + ", " + super.toString() + "]";
	}

}
