package ejercicio5;

/**
 * @author David
 *
 */

public abstract class Persona {
	/*
	 * ATRIBUTOS
	 */
	protected String nombre;
	protected int edad;
	protected char sexo;
	private boolean disponible;

	/*
	 * METODOS CONSTRUCTORES
	 */
	public Persona() {
		this.nombre = "";
		this.edad = 0;
		this.sexo = 'H';
		this.disponible = false;
	}
	
	public Persona(String nombre, int edad, char sexo) {
		this.nombre = nombre;
		this.edad = edad;
		this.sexo = sexo;
		this.disponible = assistenciaSatisfactoria();
	}

	public Persona(String nombre, int edad, char sexo, boolean disponible) {
		this.nombre = nombre;
		this.edad = edad;
		this.sexo = sexo;
		this.disponible = disponible;
	}

	/*
	 * METODOS SET
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	/*
	 * METODOS GET
	 */
	public String getNombre() {
		return nombre;
	}

	public int getEdad() {
		return edad;
	}

	public char getSexo() {
		return sexo;
	}
	
	public boolean getDisponible() {
		return disponible;
	}
	
	/*
	 * METODOS
	 */
	public abstract boolean assistenciaSatisfactoria ();

	@Override
	public String toString() {
		return "[nombre=" + nombre + ", edad=" + edad + ", sexo=" + sexo +", disponible=" + disponible + "]";
	}
	
}
