package main;

/**
 * 2)Creaunaaplicaciónquegestioneelflujodeventasdeunacajadesupermercado.Elprogramaguardaralacantidadesdelcarritodecompradentrodeunalista.Mostraráporpantallalasiguienteinformacion:•IVAaplicado(21%o4%)•preciototalbrutoypreciomasIVA.•Numerodeartículoscomprados.•Cantidadpagada.•Cambioadevolveralcliente.
 * 
 * @author David
 */

import javax.swing.JOptionPane;
import java.util.*;

public class Ejercicio2App {
	
	public static List<String[]> carrito = new ArrayList<String[]>();
	public static double totalPrecioBruto = 0;
	public static double totalPrecioIva = 0;

	public static void main(String[] args) {
		calcularCarrito();
		imprimirTicket();
	}

	public static void calcularCarrito() {
		Boolean salir = false;
		while(!salir) {
			String anyadir = JOptionPane.showInputDialog("Añadir producto al carrito? (si o no)");
			int index = 0;
			
			switch(anyadir) {
			  case "si":
				  int articulos = Integer.parseInt(JOptionPane.showInputDialog("Total de articulos?"));
				  int precio = Integer.parseInt(JOptionPane.showInputDialog("Precio del producto?"));
				  double iva = Double.parseDouble(JOptionPane.showInputDialog("IVA a aplicar? (21 o 4)"));
				  
				  double precioBruto = articulos * precio;
				  double precioIva = precioBruto + (precioBruto * (iva/100));
				  totalPrecioBruto = totalPrecioBruto + precioBruto;
				  totalPrecioIva = totalPrecioIva + precioIva;
				  
				  carrito.add(new String[] {""+index+"> ",""+articulos,""+precioBruto,""+precioIva,""+iva+""});
				  index++;
			    break;
			  case "no":
				  salir = true;
			    break;
			  default:
				  JOptionPane.showMessageDialog(null, "Lo opción introducida no es valida!");
			}

		}
		
	}

	public static void imprimirTicket() {
		double cantidadPagada = Double.parseDouble(JOptionPane.showInputDialog("Total a pagar: "+totalPrecioIva+"€. Cuanto paga el cliente?"));
		double cantidadDevolver = cantidadPagada - totalPrecioIva;
		Iterator<String[]> it = carrito.iterator();
		String[] item;
		while(it.hasNext()) {
			item = it.next();
			System.out.println(""+item[0]+" x"+item[1]+" 'nombre del producto', Precio: "+item[2]+"€, Precio + iva: "+item[3]+"€, "+item[4]+"%Iva");
		}
		System.out.println("-----------------------------------------------");
		System.out.println("Total: "+totalPrecioBruto+"€, Total + iva: "+totalPrecioIva+"€, A devolver :"+cantidadDevolver+"€");
		
		
	}

}