package main;

/**
 * 4)Combinalosmétodosgeneradosenlasactividades2y3creandounaaplicaciónquegestioneventasycontroldestockenunamismainterfaz.Utilizaparaellolasestructurasdedatosquecreasconveniente.
 * 
 * @author David
 */

import javax.swing.JOptionPane;
import java.util.*;

public class Ejercicio4App {
	public static Hashtable<Integer, String[]> articulos = new Hashtable<Integer, String[]>();
	public static List<String[]> carrito = new ArrayList<String[]>();
	public static double totalPrecioBruto;
	public static double totalPrecioIva;
	public static double precioDelProducto;

	public static void main(String[] args) {
		llenarTienda(); // Primero llenamos la tienda para poder testear.
		menu(); // Segundo llamamos al menu para poder acceder a las funciones de la app.
	}

	/*
	 * Procedimiento que se usa en bucle como un menu hasta que decides parar el
	 * programa.
	 */
	private static void menu() {
		Boolean salir = false;
		int contador = 10;

		while (!salir) {
			String opcion = JOptionPane.showInputDialog(
					"[1] - Añadir producto, [2] - Buscar producto, [3] - Mostrar todo, [4] - Gestionar carrito, [5] - Salir");

			switch (opcion) {

			case "1": // - Añadir producto
				anyadirTienda(contador);
				break;

			case "2": // - Buscar producto
				String buscar = JOptionPane.showInputDialog("Nombre del producto a buscar?");
				buscarTienda(buscar);
				break;

			case "3": // - Mostrar todo
				mostrarTienda();
				break;

			case "5": // - Salir
				salir = true;
				break;

			case "4": // - Crear Ticket y Imprimir
				calcularCarrito();
				break;

			default:
				JOptionPane.showMessageDialog(null, "Lo opción introducida no es valida!");
			}
		}
	}

	/*
	 * Procedimiento para añadir un producto al stock de la tienda.
	 */
	public static void anyadirTienda(int contador) {
		int cantidadProducto = Integer.parseInt(JOptionPane.showInputDialog("Cantidad de productos iguales?"));
		String nombreProducto = JOptionPane.showInputDialog("Nombre del producto?");
		double precioProducto = Double.parseDouble(JOptionPane.showInputDialog("Precio del producto? (sin iva)"));
		articulos.put(contador, new String[] { "" + cantidadProducto, nombreProducto, "" + precioProducto });
		contador++;
	}

	/*
	 * Procedimiento para buscar un producto en el stock de la tienda.
	 */
	public static Boolean buscarTienda(String buscar) {
		Boolean encontrado = false;
		Enumeration<String[]> producto1 = articulos.elements();
		while (producto1.hasMoreElements()) {
			String[] item = producto1.nextElement();
			String este = item[1];
			if (este.equals(buscar)) {
				JOptionPane.showMessageDialog(null, item[1] + "  x" + item[0] + " - precio: " + item[2]);
				precioDelProducto = Double.parseDouble(item[2]);
				encontrado = true;
			}
		}
		
		if (!encontrado) {
			JOptionPane.showMessageDialog(null, "El producto no existe!");
		}

		return encontrado;
	}

	/*
	 * Procedimiento que muestra por terminal todos los productos que hay en stock.
	 */
	public static void mostrarTienda() {
		Enumeration<String[]> producto2 = articulos.elements();
		Enumeration<Integer> key2 = articulos.keys();
		System.out.println();
		System.out.println("TIENDA - STOCK-----------------------------------------------------");
		while (producto2.hasMoreElements()) {
			String[] item = producto2.nextElement();
			System.out.println(key2.nextElement() + "> " + item[1] + "  x" + item[0] + " - precio: " + item[2]);
		}
		System.out.println("###################################################################");
		System.out.println();
	}

	/*
	 * Procedimiento que llena la tienda para poder testear.
	 */
	public static void llenarTienda() {
		articulos.put(0, new String[] { "400", "cocacola", "1.20" });
		articulos.put(1, new String[] { "200", "leche", "0.85" });
		articulos.put(2, new String[] { "400", "Fanta Limon", "1.20" });
		articulos.put(3, new String[] { "400", "Fanta Naranja", "1.20" });
		articulos.put(4, new String[] { "50", "Vino de la casa", "19.5" });
		articulos.put(5, new String[] { "100", "vino del pozo", "4.5" });
		articulos.put(6, new String[] { "300", "Monster", "1.40" });
		articulos.put(7, new String[] { "300", "RedBull", "1.80" });
		articulos.put(8, new String[] { "1", "loreipsum", "200" });
		articulos.put(9, new String[] { "1", "ipsumlore", "200" });
	}

	/*
	 * Procedimiento que llena el carrito de la compra.
	 */
	public static void calcularCarrito() {
		Boolean salir = false;
		totalPrecioBruto = 0;
		totalPrecioIva = 0;
		carrito.clear();

		while (!salir) {
			precioDelProducto = 0;

			String anyadir = JOptionPane.showInputDialog("Añadir producto al carrito? (si o no)");
			int index = 0;

			switch (anyadir) {
			case "si":

				// Buscamos un producto que este en stock para poder añadirlo al carrito.
				Boolean flag = false;
				String producto = null;
				while (!flag) {
					producto = JOptionPane.showInputDialog("Nombre del producto a buscar?");
					flag = buscarTienda(producto);
				}

				// Gestionamos el carrito de la compra
				int articulos = Integer.parseInt(JOptionPane.showInputDialog("Total de articulos?"));
				if (articulos == 0) {
					articulos = 1;
				}
				double iva = Double.parseDouble(JOptionPane.showInputDialog("IVA a aplicar? (21 o 4)"));

				double precioBruto = articulos * precioDelProducto;
				double precioIva = precioBruto + (precioBruto * (iva / 100));

				totalPrecioBruto = totalPrecioBruto + precioBruto;
				totalPrecioIva = totalPrecioIva + precioIva;

				carrito.add(new String[] { "" + index + "> ", "" + articulos, "" + precioBruto, "" + precioIva,
						"" + iva, "" + producto });
				index++;

				// Llamamos al metodo que se ocupa de gestionar el stock pasandole el nombre del
				// producto a editar y la cantidad de productos retirados de la tienda.
				GestionStock(producto, articulos);

				break;
			case "no":
				salir = true;
				imprimirTicket();
				break;
			default:
				JOptionPane.showMessageDialog(null, "Lo opción introducida no es valida!");
			}

		}

	}

	/*
	 * Procedimiento que gestiona el stock de la tienda.
	 */
	public static void GestionStock(String producto, int cantFuera) {
		Enumeration<String[]> producto1 = articulos.elements();
		Enumeration<Integer> key = articulos.keys();
		while (producto1.hasMoreElements() && key.hasMoreElements()) {
			String[] item = producto1.nextElement();
			int pos = key.nextElement();
			String este = item[1];

			int cantOriginal = Integer.parseInt(item[0]);
			int nuevaCantidad = cantOriginal - cantFuera;

			if (este.equals(producto)) {
				item[0] = "" + nuevaCantidad;
				articulos.replace(pos, item);
			}

		}

	}

	/*
	 * Procedimiento que imprime el ticket.
	 */
	public static void imprimirTicket() {
		double cantidadPagada = Double.parseDouble(
				JOptionPane.showInputDialog("Total a pagar: " + totalPrecioIva + "€. Cuanto paga el cliente?"));
		double cantidadDevolver = cantidadPagada - totalPrecioIva;
		Iterator<String[]> it = carrito.iterator();
		String[] item;
		System.out.println("TICKET-------------------------------------------------------------");
		while (it.hasNext()) {
			item = it.next();
			System.out.println("" + item[0] + " x" + item[1] + " " + item[5] + ", Precio: " + item[2]
					+ "€, Precio + iva: " + item[3] + "€, " + item[4] + "%Iva");
		}
		System.out.println("-------------------------------------------------------------------");
		System.out.println();
		System.out.println("Total: " + totalPrecioBruto + "€, Total + iva: " + totalPrecioIva + "€, A devolver :"
				+ cantidadDevolver + "€");
		System.out.println("###################################################################");

	}
}