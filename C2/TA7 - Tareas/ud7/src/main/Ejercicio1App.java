package main;

/**
 * 1)Creaunaaplicaciónquecalculelanotamediadelosalumnospertenecientesalcursodeprogramación.Unavezcalculadalanotamediaseguardaraestainformaciónenundiccionariodedatosquealmacenelanotamediadecadaalumno.Todosestosdatossehandeproporcionarporpantalla.
 * 
 * @author David
 */

import javax.swing.JOptionPane;
import java.util.*;

public class Ejercicio1App {
	public static Hashtable<Integer, String> notaMedia = new Hashtable<Integer, String>();

	public static void main(String[] args) {
		int maxAlumnos = Integer.parseInt(JOptionPane.showInputDialog("Cuantos alumnos hay en el curso :"));
		int maxAsignaturas = Integer.parseInt(JOptionPane.showInputDialog("Cuantas asignaturas hay en el curso :"));

		calcularMedia(maxAlumnos, maxAsignaturas);
		imprimirNotas();
	}

	public static void calcularMedia(int al, int as) {
		for (int x = 0; x < al; x++) {
			String nombre = JOptionPane.showInputDialog("Nombre del alumno :");
			double sumaNotas = 0;

			for (int i = 0; i < as; i++) {
				sumaNotas = sumaNotas
						+ Double.parseDouble(JOptionPane.showInputDialog("Nota [ " + i  + " / " + (as - 1) + " ]"));
			}

			double nMedia = sumaNotas / as;
			
			nombre = nombre+" -> "+nMedia;
			
			notaMedia.put(x, nombre); // Guardamos el nombre del alumno y su nota media.
		}
	}

	public static void imprimirNotas() {
		Enumeration<String> notas = notaMedia.elements();
		Enumeration<Integer> key = notaMedia.keys();

		while (notas.hasMoreElements()) {
			System.out.println(key.nextElement() + " > " + notas.nextElement());

		}

	}

}