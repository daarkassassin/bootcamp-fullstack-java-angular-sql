package main;

/**
 * 3)Creaunabasededatosde10artículosparacontrolarelstockdeproductosdeunatiendapormediodeundiccionariodedatos(articulo:precio).Elusuariopodráañadir,pormediodeinterfazvisualartículosnuevosycantidadesdeestos.Elusariopodráconsultarlainformaciónalmacenadaeneldiccionarioreferenteaunarticuloconcretoeinclusolistartodalainformaciónenlaterminaldelprograma.
 * 
 * @author David
 */

import javax.swing.JOptionPane;
import java.util.*;

public class Ejercicio3App {
	public static Hashtable<Integer, String[]> articulos = new Hashtable<Integer, String[]>();

	public static void main(String[] args) {
		llenarTienda(); //Primero llenamos la tienda para poder testear.
		menu(); //Segundo llamamos al menu para poder acceder a las funciones de la app.
	}
	
	/*
	 * Procedimiento que se usa es bucle como un menu hasta que decides parar el programa.
	 */
	private static void menu() {
		Boolean salir = false;
		int contador = 10;
		
		while(!salir) {
			String opcion = JOptionPane.showInputDialog("[1] - Añadir producto, [2] - Buscar producto, [3] - Mostrar todo, [4] - Salir");
			
			switch(opcion) {
			
			  case "1": //- Añadir producto
				  anyadirTienda(contador);
				  break;
			    
			  case "2": //- Buscar producto
				  buscarTienda();
				  break;
				  
			  case "3": //- Mostrar todo
				  mostrarTienda();
				  break;
				  
			  case "4": //- Salir
				  salir = true;
				  break;
				  
			  default:
				  JOptionPane.showMessageDialog(null, "Lo opción introducida no es valida!");
			}
		}
	}
	
	/*
	 * Procedimiento para añadir un producto al stock de la tienda.
	 */
	public static void anyadirTienda(int contador) { 
		int cantidadProducto = Integer.parseInt(JOptionPane.showInputDialog("Cantidad de productos iguales?"));
		  String nombreProducto = JOptionPane.showInputDialog("Nombre del producto?");
		  int precioProducto = Integer.parseInt(JOptionPane.showInputDialog("Precio del producto? (sin iva)"));
		  articulos.put(contador, new String[] {""+cantidadProducto,nombreProducto,""+precioProducto});
		  contador++;
	}
	
	/*
	 * Procedimiento para buscar un producto en el stock de la tienda.
	 */
	public static void buscarTienda() {
		String buscar = JOptionPane.showInputDialog("Nombre del producto a buscar?");
		Boolean encontrado = false;
		Enumeration<String[]> producto1 = articulos.elements();
		while (producto1.hasMoreElements()) {
			String[] item =  producto1.nextElement();
			String este = item[1];
			if (este.equals(buscar)) {
				JOptionPane.showMessageDialog(null, item[1]+"  x"+item[0]+" - precio: "+item[2]);
				encontrado = true;
			}
		}
		if(!encontrado){
			JOptionPane.showMessageDialog(null, "El producto no existe!");
		}
	}
	
	/*
	 * Procedimiento que muestra por terminal todos los productos que hay en stock.
	 */
	public static void mostrarTienda() {
		Enumeration<String[]> producto2 = articulos.elements();
		Enumeration<Integer> key2 = articulos.keys();
		while (producto2.hasMoreElements()) {
			String[] item =  producto2.nextElement();
			System.out.println(key2.nextElement() + "> "+item[1]+"  x"+item[0]+" - precio: "+item[2]);
		}
	}
	
	/*
	 * Procedimiento que llena la tienda para poder testear.
	 */
	public static void llenarTienda() {
		articulos.put(0, new String[] {"400","cocacola","1.20"});
		articulos.put(1, new String[] {"200","leche","0.85"});
		articulos.put(2, new String[] {"400","Fanta Limon","1.20"});
		articulos.put(3, new String[] {"400","Fanta Naranja","1.20"});
		articulos.put(4, new String[] {"50","Vino de la casa","19.5"});
		articulos.put(5, new String[] {"100","vino del pozo","4.5"});
		articulos.put(6, new String[] {"300","Monster","1.40"});
		articulos.put(7, new String[] {"300","RedBull","1.80"});
		articulos.put(8, new String[] {"1","loreipsum","200"});
		articulos.put(9, new String[] {"1","ipsumlore","200"});
	}

	

}