package com.vehicles.project;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Vehicle {

	protected String plate;
	protected String brand;
	protected String color;
	protected List<Wheel> wheels = new ArrayList<Wheel>();

	public Vehicle(String plate, String brand, String color) {
		this.plate = plate;
		this.brand = brand;
		this.color = color;
	}
	
	//Metodo para comprobar el formato de la matricula.
	public static boolean testMatricula(String matricula) {
		Pattern p = Pattern.compile("^\\d{4}[A-Z]{2,3}");
		Matcher m = p.matcher(matricula);
		if (m.matches()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Vehicle [plate=" + plate + ", brand=" + brand + ", color=" + color + ", wheels=" + wheels + "]";
	}
	
}
