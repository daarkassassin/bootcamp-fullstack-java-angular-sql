package com.vehicles.project;
/**
 * @author David Bonet Daga
 *
 */

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class metodesTaller {
	// Metode menu
	public static void menu() {
		boolean salir = false;
		try {
			while (!salir) {
				String opcion = JOptionPane.showInputDialog("[1]Crear un coche, [2]Crear una moto, [0]Sortir");

				switch (opcion) {
				case "0": // - Salir
					salir = true;
					break;

				case "1": // - Crear coche
					metodesTaller.crearCoche();
					break;

				case "2": // - Crear moto
					metodesTaller.crearMoto();
					break;
				default:
					JOptionPane.showMessageDialog(null, "Lo opci�n introducida no es valida!");
				}
			}
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
	}

	// Metode per crear una moto
	public static void crearMoto() throws Exception {
		System.out.println("Creem una moto");
		// Demanem les dades del coche
		String matricula = demanarMatricula("matricula de la moto?");
		String marca = entrada("Marca de la moto?");
		String color = entrada("Color de la moto?");

		// Creem l'objecte coche
		Bike moto = new Bike(matricula, marca, color);

		// Demanem les rodes del davant i detras
		List<Wheel> wheelsmoto = new ArrayList<Wheel>();
		demanarRoda(wheelsmoto, "delantera i trasera");

		// Afegim les rodes a la moto
		moto.addTwoWheels(wheelsmoto);

		// Mostrem les dades
		System.out.println(moto.toString());
	}

	// Metode per crear un coche
	public static void crearCoche() throws Exception {
		System.out.println("Creem un coche");
		// Demanem les dades del coche
		String matricula = demanarMatricula("matricula del coche?");
		String marca = entrada("Marca del coche?");
		String color = entrada("Color del coche?");

		// Creem l'objecte coche
		Car coche = new Car(matricula, marca, color);

		// Demanem les rodes del davant
		List<Wheel> wheels1 = new ArrayList<Wheel>();
		demanarRoda(wheels1, "delantera");

		// Demanem les rodes del darrere
		List<Wheel> wheels2 = new ArrayList<Wheel>();
		demanarRoda(wheels2, "delantera");

		// Afegim les rodes al objecte
		coche.addWheels(wheels1, wheels2);

		// mostrem les dades
		System.out.println(coche.toString());
	}

	// Metodes per demanar les rodes
	public static void demanarRoda(List<Wheel> wheels, String tipo) {
		int i = 0;
		while (i < 2) {
			// Demanem les dades de les rodes
			String marcaroda = entrada("Marca de la roda " + tipo + " " + i + "?");
			double diametreroda = entradaDouble("Diametre de la roda " + tipo + " " + i + "?");

			// Comprovem el diametre de la roda
			while (!Wheel.testDiameter(diametreroda)) {
				JOptionPane.showMessageDialog(null, "El diametro ha de ser mayor que 0.4 y inferior que 4.");
				diametreroda = entradaDouble("Diametre de la roda " + tipo + " " + i + "?");
			}

			// Creem l'objecte roda i lafegim al array de rodes.
			Wheel roda = new Wheel(marcaroda, diametreroda);
			wheels.add(roda);
			i++;
		}
	}

	// Metode per demanar la matricula
	public static String demanarMatricula(String msg) {
		String matriculaUsuari;
		while (true) {
			matriculaUsuari = entrada(msg);
			if (Vehicle.testMatricula(matriculaUsuari)) {
				break;
			} else {
				JOptionPane.showMessageDialog(null,
						"La matricula del vehicle ha de tenir 4 numeros seguits de 2 o 3 lletres.");
			}
		}
		return matriculaUsuari;
	}

	// Metode per demanar dades al usuari.
	public static String entrada(String msg) {
		String entradaUsuari;
		try {
			entradaUsuari = JOptionPane.showInputDialog(msg);
		} catch (Exception e) {
			entradaUsuari = null;
		}
		return entradaUsuari;
	}

	// Metode per demanar dades al usuari.
	public static double entradaDouble(String msg) {
		double entradaUsuari;
		try {
			entradaUsuari = Double.parseDouble(JOptionPane.showInputDialog(msg));
		} catch (Exception e) {
			entradaUsuari = 1;
		}
		return entradaUsuari;
	}

}
