package com.vehicles.project;

public class Wheel {
	private String brand;
	private double diameter;

	public Wheel(String brand, double diameter) {
		this.brand = brand;
		this.diameter = diameter;
	}
	
	public static boolean testDiameter(Double diameter) {
		if(diameter >= 0.4 && diameter <= 4) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Wheel [brand=" + brand + ", diameter=" + diameter + "]";
	}
	
	
}
