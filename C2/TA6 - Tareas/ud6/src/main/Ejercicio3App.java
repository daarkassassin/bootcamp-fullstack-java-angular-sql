package main;

/**
 * 3)Crea una aplicaci�n que nos pida un n�mero por teclado y con un m�todo se lo pasamos por par�metro 
 * para que nos indique si eso un n�mero primo,debe devolver true si esprimo si no false.
 * Un n�mero primo es aquel que solo puede dividirse entre 1 y si mismo.Por ejemplo:25 no es primo,
 * ya que 25 es divisible entre 5,sin embargo,17 si es primo.
 * 
 * @author David
 */
 
import javax.swing.JOptionPane;

public class Ejercicio3App {
	
	public static void main(String[] args) {
		int opc = Integer.parseInt(JOptionPane.showInputDialog("Este numero es Primo? ")); //El usuario inserta un numero para saber si es primo o no.
		JOptionPane.showMessageDialog(null, " "+primo(opc)); //Le decimos si es primo o no con true o false
	}
	
	
	public static Boolean primo(int num) {
		boolean primo = true;
		if(num<2) {
			primo = false; //Si el numero es mas peque�o que 2, no es primo
		}else {
			for (int x=2; x*x<=num; x++) { //Mientras x * x sea mas peque�o o igual que el numero introducido recorremos el bucle.
				if (num%x==0) {  //Si la resta de la division entre el numero y x es igual a 0 no es primo.
					primo = false; 
					break;
				}
			}
		}
		//Si llegas aqui sin pasar por un false, el numero es primo.
		return primo;
	}
	
	
}
