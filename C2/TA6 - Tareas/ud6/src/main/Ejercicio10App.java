package main;

/**
 * 10)Creaunarray denúmeros deun tamañopasado por teclado,elarray contendránúmeros aleatoriosprimosentre los números deseados, porúltimonos indicarcualeselmayor detodos.Hazunmétodo paracomprobarque elnúmero aleatorioesprimo,puedeshacertodoslométodos que necesites.
 *
 * @author David
 */
 
import javax.swing.JOptionPane;
import java.util.Arrays;
import java.util.Random;

public class Ejercicio10App {
	
	public static void main(String[] args) {
		int max = Integer.parseInt(JOptionPane.showInputDialog("Indica el tamaño del array :")); 	//Definimos lo grande que va 
		int numeros[] = new int [max];																//a ser nuestro array
		numeros = llenarArray(numeros, max);  //Llamamos a la funcion que se ocupa de llenar el array de numeros primos.
		imprimirArray(numeros, max); //Llamamos al procedimiento que se ocupa de imprimir por consola.
	}
	
	/*
	 * Funcion que llena el array con numeros primos.
	 */
	public static int[] llenarArray (int x[], int max) {
		for(int i = 0; i < max; i++){
			Random r = new Random(); //Clase random con la que generaremos int's.
			boolean esPrimo = false;
			int testPrimo = 0;
			
			while (!esPrimo) {
				testPrimo = r.nextInt(1000);
				esPrimo = primo(testPrimo); //Llamamos a la funcion primo(x) para saber si el numero generado es primo o no. 
			}
			
			x [i] = testPrimo;
		}
		return x;
	}
	
	/*
	 * Procedimiento que imprime el array generado y te dice cual es el numero mas grande
	 */
	public static void imprimirArray (int x[], int max) {
		for(int i = 0; i < max; i++){
			System.out.println("pos: "+i+" > num: "+x [i]);
		}
		System.out.println("El numero primo mas grande es: "+Arrays.stream(x).max().getAsInt());
	}
	
	
	
	/*
	 * Funcion que retorna true o false segun si el numero que recibe es primo o no.
	 */
	public static Boolean primo(int num) {
		boolean primo = true;
		if(num<2) {
			primo = false; 
		}else{
			for (int x=2; x*x<=num; x++) { 
				if (num%x==0) {  
					primo = false; 
					break;
				}
			}
		}
		return primo;
	}
	
}