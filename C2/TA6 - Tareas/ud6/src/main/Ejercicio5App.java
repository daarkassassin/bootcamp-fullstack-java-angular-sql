package main;

/**
 * 5) Crea una aplicaci�n que nos convierta un n�mero en base decimal a binario.
 * Esto lo realizara un m�todo al que le pasaremos el numero como par�metro,
 * devolver� un String con el numero convertido a binario.
 * 
 * Para convertir un numero decimal a binario, debemos dividir entre 2 el numero 
 * y el resultado de esa divisi�n se divide entre 2 de nuevo hasta que no se 
 * pueda dividir mas, el resto que obtengamos de cada divisi�n formara el numero binario,
 * de abajo a arriba.
 * 
 * @author David
 */
 
import javax.swing.JOptionPane;

public class Ejercicio5App {
	
	public static void main(String[] args) {
		int opc = Integer.parseInt(JOptionPane.showInputDialog("Inserta un numero en base decimal: ")); 
		JOptionPane.showMessageDialog(null, " El numero "+opc+" en binario es: "+convertir(opc));
	}
	
	public static String convertir (int num) {
		return Integer.toBinaryString(num); //Convertimos el numero a binario y lo representamos con un string
	}
	
}
