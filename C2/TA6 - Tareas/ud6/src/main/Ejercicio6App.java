package main;

/**
 * 6)Crea una aplicaci�n que nos cuente el n�mero de cifras de un n�mero entero 
 * positivo(hay que controlarlo)pedido por teclado.
 * 
 * Crea un m�todo que realice esta acci�n, pasando el n�mero por 
 * par�metro, devolver� el n�mero de cifras.
 * 
 * @author David
 */
 
import javax.swing.JOptionPane;

public class Ejercicio6App {
	
	public static void main(String[] args) {
		int opc = Integer.parseInt(JOptionPane.showInputDialog("Introduce un numero de varias cifras :"));
		
		JOptionPane.showMessageDialog(null, " El numero "+opc+" tiene "+contador(opc)+" cifras.");
	}
	
	public static int contador (int num) {
		int cifras = 0;
		while(num>0) {
			num = num/10;
			cifras ++;
			System.out.println(num);
		}
		return cifras;
	}
	
}
