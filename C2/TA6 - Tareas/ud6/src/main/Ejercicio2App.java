package main;

/**
 * 2)Crea una aplicaci�n que nos genere una cantidad den �meros 
 * enteros aleatorios que nosotros le pasaremos por teclado.
 * Creaun m�todo donde pasamos como par�metros entre que n�meros queremos que los genere,
 * podemos pedirlas porteclado antes de generar los n�meros.
 * Este m�todo devolver� un n�mero entero aleatorio. 
 * 
 * Muestra estos n�meros por pantalla.
 * 
 * @author David
 */
 
import javax.swing.JOptionPane;
import java.util.Random;

public class Ejercicio2App {
	
	public static void main(String[] args) {
		int numMaximos = Integer.parseInt(JOptionPane.showInputDialog("Cuantos numeros quieres generar? ")); //Consultamos al usuario cuantos numeros random queremos generar.
		int numGrande = Integer.parseInt(JOptionPane.showInputDialog("Inserta el numero mas grande posible:")); //Consultamos al usuario cual sera el numero random mas grande
		JOptionPane.showMessageDialog(null, " "+generador(numMaximos, numGrande)); //Le devolvemos al usuario todos los numeros que ha pedido.
	}
	
	public static String generador(int num, int max) {
		//Guardamos todos los numeros generados en un string y al acabar devolvemos el resultado.
		String imprimir = ">";
		for(int i = 0; i < num; i++){
			if (i == 0) {
				imprimir = imprimir +" "+random(max);
			}else {
				imprimir = imprimir +" ,"+random(max);
			}
		}
	
		return imprimir;
	}
	
	public static int random(int num) {
		Random r = new Random(); //Clase random con la que generaremos int's.
		int salida = r.nextInt(num);  // Generamos un numero entre 0 y "num".
		return salida;
	}
	
}
