package main;

/**
 * 8)Crea un array de 10 posiciones de n�meros con valores pedidos por teclado.
 * Muestra por consola el indice y el valor al que corresponde.
 * Haz dos m�todos, uno para rellenar valores y otro para mostrar
 *
 * @author David
 */
 
import javax.swing.JOptionPane;

public class Ejercicio8App {
	
	public static void main(String[] args) {
		int numeros[] = new int [10];
		numeros = llenarArray(numeros);  //Llamamos al metodo que se ocupara de llenar el array
		imprimirArray(numeros); //Llamamos al metodo que se ocupara de imprimir el array.
	}
	
	
	public static int[] llenarArray (int x[]) {
		for(int i = 0; i < 10; i++){
			x [i] = Integer.parseInt(JOptionPane.showInputDialog("Introduce un numero :"));
		}
		return x;
	}
	
	public static void imprimirArray (int x[]) {
		for(int i = 0; i < 10; i++){
			System.out.println("pos: "+i+" > num: "+x [i]);
		}
	}
	
}