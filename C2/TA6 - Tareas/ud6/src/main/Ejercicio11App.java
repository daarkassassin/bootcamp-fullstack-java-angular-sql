package main;



/**
 * 11) Crea dos arrays de n�meros con la dimensi�n pasada por teclado. 
 * Uno de ellos estar� rellenado con n�meros aleatorios y el otro apuntara 
 * al array anterior, reasigna los valores del segundo array con valores aleatorios. 
 * Despu�s, crea un m�todo que tenga como par�metros, los dos arrays y devuelva 
 * uno nuevo con lamultiplicaci�n de la posici�n0 del array 1 con el del 
 * array2 y as� sucesivamente. Por �ltimo, muestra el contenido de cada array.
 * 
 * @author David
 */

import java.util.Random;
import javax.swing.JOptionPane;

public class Ejercicio11App {
	
	public static void main(String[] args) {
		int max1 = Integer.parseInt(JOptionPane.showInputDialog("Indica un numero de filas :"));
		if(max1 <= 1) {
			max1 = 2;
			System.out.println("El minimo permitido de filas es 2");
		}
		
		int max2 = Integer.parseInt(JOptionPane.showInputDialog("Indica un numero de columnas :"));
		if(max2 <= 0) {
			max2 = 1;
			System.out.println("El minimo permitido de columnas es 1");
		}
		
		int array1[] [] = new int [max1] [max2];
		LlenarArrays(array1);
		imprimirArrays(array1);
		System.out.println(" X ");System.out.println("");
		
		int array2[] [] = new int [max1] [max2];
		AsignarArrays(array2, array1);
		LlenarArrays(array2);
		imprimirArrays(array2);
		System.out.println(" = ");System.out.println("");
		
		int array3[] [] = new int [max1] [max2];
		array3 = multiplicarArrays(array1, array2);
		System.out.println("  ");
		imprimirArrays(array3);
		
	}
	
	public static void LlenarArrays (int x[][]) {
		for(int i = 0; i < x.length; i++){
			for(int j = 0; j < x[1].length; j++){
				Random r = new Random(); 
				x [i][j] = r.nextInt(9);
			}
		}
	}
	
	public static void AsignarArrays (int x[][], int p[][]) {
		for(int i = 0; i < x.length; i++){
			for(int j = 0; j < x[1].length; j++){
				x [i][j] = p [i][j];
			}
		}
	}
	
	public static void imprimirArrays (int x[][]) {
		for(int i = 0; i < x.length; i++){
			for(int j = 0; j < x[1].length; j++){
				if(j == 0) {
					System.out.print("> "+x [i][j]+", ");
				}else{
					
					if(j == x[1].length-1){
						System.out.println(""+x [i][j]+"");
					}else{
						System.out.print(""+x [i][j]+", ");
					}
				}
			}
			
		}
		System.out.println();
	}
	
	public static int[][] multiplicarArrays (int x[][], int p[][]) {
		int salida [][] = x;
		
		for(int i = 0; i < x.length; i++){
			
			for(int j = 0; j < x[1].length; j++){
				int n1 = x [i][j];
				int n2 = p [i][j];
				int resultado = (n1 * n2);
				salida[i][j] = resultado;
				
				//System.out.println("< "+n1+" x "+n2+" = "+resultado );
				
			}
		}
		
		return salida;
	}
	
}