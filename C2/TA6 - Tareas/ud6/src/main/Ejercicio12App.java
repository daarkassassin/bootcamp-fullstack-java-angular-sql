package main;

/**
 * 12)Crea un array de n�meros de un tama�o pasado por teclado,
 * el array contendr� n�meros aleatorios entre1 y 300 y mostrar� aquellos n�meros que acaben en un d�gito que nosotros le indiquemos por teclado
 * (debes controlar que se introduce un numero correcto), estos deben guardarse en un nuevo array. 
 * Porejemplo,enunarray de 10 posiciones le indicamos mostrar los n�meros acabados en 5,
 * podr�asalir155,25,etc.
 * 
 * @author David
 */
 
import javax.swing.JOptionPane;
import java.util.Random;

public class Ejercicio12App {
	
	public static void main(String[] args) {
		int max1 = Integer.parseInt(JOptionPane.showInputDialog("Indica un numero de filas :"));
		int max2 = Integer.parseInt(JOptionPane.showInputDialog("Indica un numero de columnas :"));
		int acabadosEn = Integer.parseInt(JOptionPane.showInputDialog("Mostrar numeros acabados en (0,1,2,3,4,5,6,7,8 o 9):"));
		
		int array1[] [] = new int [max1] [max2];
		LlenarArrays(array1);
		imprimirArrays(array1,acabadosEn);
		
		
	}
	
	public static void LlenarArrays (int x[][]) {
		for(int i = 0; i < x.length; i++){
			for(int j = 0; j < x[1].length; j++){
				Random r = new Random(); 
				x [i][j] = r.nextInt(300);
			}
		}
	}
	
	public static void imprimirArrays (int x[][], int f) {
		boolean flag = false;
		for(int i = 0; i < x.length; i++){
			for(int j = 0; j < x[1].length; j++){
				if(x [i][j] % 10 == f) {
					System.out.print(" "+x [i][j]+", ");
					flag = true;
				}
			}
		}
		if(!flag){
			System.out.println("No hay numeros acabados en "+f);
		}
		System.out.println();
	}
	
	
}