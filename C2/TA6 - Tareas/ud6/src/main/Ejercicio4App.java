package main;

/**
 * 4)Crea una aplicaciónquenos calculeelfactorialdeunnúmero pedidopor 
 * teclado,lorealizaramedianteun métodoal quelepasamosel númerocomoparámetro.
 * Para calcularelfactorial, semultiplicalosnúmerosanterioreshasta llegara uno.Por 
 * ejemplo,siintroducimosun5,realizara esta operación5*4*3*2*1=120.
 * 
 * @author David
 */
 
import javax.swing.JOptionPane;

public class Ejercicio4App {
	
	public static void main(String[] args) {
		int opc = Integer.parseInt(JOptionPane.showInputDialog("Inserta un numero para calcular su factorial: ")); 
		JOptionPane.showMessageDialog(null, " El factorial del num "+opc+" es: "+factorial(opc));
	}
	
	public static float factorial (float num) {
		if (num == 0) {
			return 1;
		}else{
			return num * factorial(num-1);
		}
	}
	
	
}
