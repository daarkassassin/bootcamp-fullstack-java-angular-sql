package main;

/**
 * 1)Crea una aplicaci�n que nos calcule el �rea de un circulo,cuadrado o triangulo. 
 * Pediremos que figura queremos calcular su �rea y seg�n lo introducido pedir� los valores necesarios para 
 * calcular el �rea. 
 * 
 * Crea un m�todo por cada figura para calcular cada �rea,este devolver� un n�mero real. 
 * Muestra el resultado por pantalla.
 * 
 * Aqu� te mostramos que necesita cada figura:
 * Circulo:(radio^2)*PI
 * Triangulo:(base*altura)/2
 * Cuadrado:lado * lado
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio1App {
	
	public static void main(String[] args) {
		String op = JOptionPane.showInputDialog("Que area calcularemos? [1] Circulo, [2] Triangulo, [3] Cuadrado");
		
		switch(op){
			case "1":
				//Circulo
				double radio = Double.parseDouble(JOptionPane.showInputDialog("Introduce el radio del circulo: "));
				JOptionPane.showMessageDialog(null, "El area del circulo es :"+circulo(radio));
				break;
			case "2":
				//Triangulo
				double base = Double.parseDouble(JOptionPane.showInputDialog("Introduce la base del triangulo: "));
				double altura = Double.parseDouble(JOptionPane.showInputDialog("Introduce la altura del triangulo: "));
				JOptionPane.showMessageDialog(null, "El area del triangulo es :"+triangulo(base, altura));
				break;
			case "3":
				//Cuadrado
				double lado1 = Double.parseDouble(JOptionPane.showInputDialog("Introduce el lado 1 del cuadrado: "));
				double lado2 = Double.parseDouble(JOptionPane.showInputDialog("Introduce el lado 2 del cuadrado: "));
				JOptionPane.showMessageDialog(null, "El area del quadrado es :"+cuadrado(lado1, lado2));
				break;
			default:
				JOptionPane.showMessageDialog(null, "La opci�n introducida no es valida! intentalo de nuevo mas tarde.");
		}
		
		
	}
	
	public static double circulo(double radio) {
		double salida = (Math.PI * Math.pow(radio,2));
		return salida;
	}
	
	public static double triangulo(double base, double altura) {
		double salida = (base * altura) / 2;
		return salida;
	}
	
	public static double cuadrado(double lado1, double lado2) {
		double salida = lado1 * lado2;
		
		return salida;
	}
	
}
