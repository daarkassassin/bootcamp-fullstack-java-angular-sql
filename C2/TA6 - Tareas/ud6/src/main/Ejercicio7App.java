package main;

/**
 * 7)Creaunaplicaciónquenos conviertauna cantidaddeeurosintroducidaportecladoa otramoneda,estaspuedenseradolares,yenesolibras. El métodotendrácomoparámetros, lacantidaddeeuros ylamoneda apasarquesera una cadena,este nodevolveráningúnvalor,mostrara un mensajeindicandoel cambio(void).
 * 
 * @author David
 */
 
import javax.swing.JOptionPane;

public class Ejercicio7App {
	final static double VLIBRAS = 0.86;
	final static double VDOLARES = 1.28611;
	final static double VYENES = 129.852;
	
	public static void main(String[] args) {
		int opc = Integer.parseInt(JOptionPane.showInputDialog("Convertir euros a [1]libras, [2]dolares, [3]yenes: "));
		int euros = Integer.parseInt(JOptionPane.showInputDialog("Introduce la cantidad de euros :"));
		conversorMonedas(opc, euros);
	}
	
	
	public static void conversorMonedas (int opc, int euros) {
		switch(opc){
		case 1:
			// 0.86 libras es 1€
			JOptionPane.showMessageDialog(null, " "+euros+"€ equivalen a "+(VLIBRAS*euros)+" Libras.");
			break;
		case 2:
			// 1.28611 $ es 1€
			JOptionPane.showMessageDialog(null, " "+euros+"€ equivalen a "+(VDOLARES*euros)+" Dolares.");
			break;
		case 3:
			// 129.852 yenes es 1€
			JOptionPane.showMessageDialog(null, " "+euros+"€ equivalen a "+(VYENES*euros)+" Yenes.");
			break;
		default: 
			JOptionPane.showMessageDialog(null, "La opción introducida no es valida! intentalo de nuevo mas tarde.");
		}
	}
	
}