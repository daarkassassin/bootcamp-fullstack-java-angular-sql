package main;

/**
 * 9)Creaunarray denúmeros dondeleindicamospor tecladoeltamaño delarray,rellenaremoselarrayconnúmerosaleatorios entre 0y 9. Al finalmuestra porpantallaelvalordecada posicióny lasumadetodos los valores.Tareas a realizar: Hazun métodopara rellenarelarray(quetenga comoparámetros los números entre los quetengaque generar)y otro para mostrarelcontenidoy lasumadelarray
 *
 * @author David
 */
 
import javax.swing.JOptionPane;
import java.util.Random;

public class Ejercicio9App {
	
	public static void main(String[] args) {
		int max = Integer.parseInt(JOptionPane.showInputDialog("Indica el tamaño del array :")); 	//Definimos lo grande que va 
		int numeros[] = new int [max];																//a ser nuestro array
		numeros = llenarArray(numeros, max);  //Llamamos a la funcion que se ocupa de llenar el array
		imprimirArray(numeros, max); //Llamamos al procedimiento que se ocupa de imprimir por consola.
	}
	
	/*
	 * Funcion que llena el array con numeros generados aleatoriamente entre 0 y 9.
	 */
	public static int[] llenarArray (int x[], int max) {
		for(int i = 0; i < max; i++){
			Random r = new Random(); //Clase random con la que generaremos int's.
			x [i] = r.nextInt(10);  // Generamos un numero entre 0 y "9".
		}
		return x;
	}
	
	/*
	 * Procedimiento que imprime el array generado y suma todos los numeros.
	 */
	public static void imprimirArray (int x[], int max) {
		int suma = 0;
		for(int i = 0; i < max; i++){
			System.out.println("pos: "+i+" > num: "+x [i]);
			suma = suma + x [i];
		}
		System.out.println("Total : "+suma);
	}
	
}