package main;

/**
 * 10)Realiza una aplicaci�n que nos pida un n�mero de ventas a introducir, 
 * despu�s nos pedir� tantas ventas por teclado como n�mero de ventas se hayan indicado. 
 * Al final mostrara la suma de todas las ventas. Piensa que es lo que se repite y lo que no.
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio10App {
	
	public static void main(String[] args) {
		String entrada = JOptionPane.showInputDialog("Introduce el total de ventas:");
		int totalVentas = Integer.parseInt(entrada);
		double sumaVentas = 0;
		
		for(int num = 0; num < totalVentas; num++) {
			
			double venta = Double.parseDouble(JOptionPane.showInputDialog("Introduce el total de la venta "+num));
			sumaVentas = sumaVentas + venta;
			
		}
		
		JOptionPane.showMessageDialog(null, "La suma de todas las ventas es : "+sumaVentas);
		
	}

}
