package main;

/**
 * 1) Declara 2 variables num�ricas (con el valor que desees), 
 * he indica cual es mayor de los dos. Si son iguales indicarlo tambi�n. 
 * Ves cambiando los valores para comprobar que funciona.
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio1App {
	
	public static void main(String[] args) {
		
		double N = 10.3;
		double M = 14.6;
		
		if(N<M) {
			JOptionPane.showMessageDialog(null, "El numero ("+N+") es mas peque�o que ("+M+").");
		}else if (N>M) {
			JOptionPane.showMessageDialog(null, "El numero ("+N+") es mas grande que ("+M+").");
		}else if(N==M) {
			JOptionPane.showMessageDialog(null, "El numero ("+N+") es igual que ("+M+").");
		}
		
	}
	
}
