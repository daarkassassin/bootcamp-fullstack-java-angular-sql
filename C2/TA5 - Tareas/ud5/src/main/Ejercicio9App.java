package main;

/**
 * 9)Muestra los n�meros del 1 al 100 (ambos incluidos) divisibles entre 2 y 3. 
 * Utiliza el bucle que desees.
 * 
 * @author David
 */

public class Ejercicio9App {
	
	public static void main(String[] args) {
	
		for(int num = 0; num <= 100; num++) {
			if (num%2 == 0 && num%3 == 0 ) {
				System.out.println("El numero ("+num+") es divisible entre 2 i 3.");
			} 
		}

	}

}
