package main;

/**
 * 7) Muestra los n�meros del 1 al 100 (ambos incluidos). Usa un bucle while.
 * 
 * @author David
 */

public class Ejercicio7App {
	
	public static void main(String[] args) {
		int num = 0;
		while(num <= 100){
			System.out.println(num);
			num++;
		}
		
	}

}
