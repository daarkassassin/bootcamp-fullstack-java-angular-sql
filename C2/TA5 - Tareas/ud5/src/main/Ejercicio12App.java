package main;

/**
 * 12)Escribe una aplicaci�n con un String que contenga una contrase�a cualquiera. 
 * Despu�s se te pedir� que introduzcas la contrase�a, con 3 intentos. Cuando aciertes ya 
 * no pedir� mas la contrase�a y mostrara un mensaje diciendo �Enhorabuena�. 
 * Piensa bien en la condici�n de salida 
 * (3 intentos y si acierta sale, aunque le queden intentos).
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio12App {
	
	public static void main(String[] args) {
		
		final String PASS = "HolaMundo";
		
		for(int num = 1; num <= 3; num++) {
			String entrada = JOptionPane.showInputDialog("Introduce la contrase�a: Intento ["+num+"] / 3");
			
			if(entrada.equals(PASS)){
				num = 99;
				JOptionPane.showMessageDialog(null, "Enhorabuena");
			}
		}

	}

}
