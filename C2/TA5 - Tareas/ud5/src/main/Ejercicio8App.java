package main;

/**
 * 8)Haz el mismo ejercicio anterior con un bucle for.
 * 
 * @author David
 */

public class Ejercicio8App {
	
	public static void main(String[] args) {
		for(int num = 0; num <= 100; num++) {
			System.out.println(num);
		}

	}

}
