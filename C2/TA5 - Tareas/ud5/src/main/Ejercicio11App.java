package main;

/**
 * 11)Crea una aplicaci�n que nos pida un d�a de la semana y que nos diga si es un d�a laboral o no. 
 * Usa un switch para ello.
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio11App {
	
	public static void main(String[] args) {
		
		String dia = JOptionPane.showInputDialog("Hoy es [ lunes, martes, miercoles, jueves, viernes, sabado o domingo ] :");
		
		switch(dia){
			case "lunes":
			case "martes":
			case "miercoles":
			case "jueves":
			case "viernes":
				JOptionPane.showMessageDialog(null, "Es un dia laborable");
				break;
			case "sabado":
			case "domingo":
				JOptionPane.showMessageDialog(null, "Es un dia festivo");
				break;
			default:
				JOptionPane.showMessageDialog(null, "No has introducido un dia correcto!");
		}

	}

}
