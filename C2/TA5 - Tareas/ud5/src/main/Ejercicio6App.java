package main;

/**
 * 6)Lee un n�mero por teclado que pida el precio de un producto 
 * (puede tener decimales) y calcule el precio final con IVA. 
 * El IVA serauna constante que seradel 21%
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio6App {
	
	public static void main(String[] args) {
		
		final double IVA = 0.21;
		
		String entrada = JOptionPane.showInputDialog("Introduce el precio del producto.");
		double precio = Double.parseDouble(entrada);
		
		JOptionPane.showMessageDialog(null, "El precio del producto con iva es : "+(precio+(precio*IVA)));

	}

}
