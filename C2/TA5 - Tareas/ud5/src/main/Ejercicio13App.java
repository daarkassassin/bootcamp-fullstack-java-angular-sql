package main;

/**
 * 13)Crea una aplicaci�n llamada CalculadoraInversa, 
 * nos pedir� 2 operandos(int) y un signo aritm�tico (String), 
 * seg�n este �ltimo se realizara la operaci�n correspondiente. 
 * Al final mostrara el resultado en un cuadro de dialogo. 
 * 
 * Los signos aritm�ticos disponibles son:
 * +: suma los dos operandos.
 * -: resta los operandos.
 * *: multiplica los operandos.
 * /: divide los operandos, este debe dar un resultado con decimales 
 * (double)^: 1� operando como base y 2� como exponente.
 * %: m�dulo, resto de la divisi�n entre operando1 y operando2.
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio13App {
	
	public static void main(String[] args) {
		
		int n = Integer.parseInt(JOptionPane.showInputDialog("Introduce un numero:  paso[ 1 / 3 ]"));
		int m = Integer.parseInt(JOptionPane.showInputDialog("Introduce un numero:  paso[ 2 / 3 ]"));
		String op = JOptionPane.showInputDialog("Que operaci�n necesitas? [+] suma, [-] resta, [*] multiplicaci�n, [/] division, [^]Elevar, [%]modulo: paso[ 2 / 3 ");
		
		switch(op){
			case "+":
				JOptionPane.showMessageDialog(null, n+" + "+m+" = "+(n+m));
				break;
			case "-":
				JOptionPane.showMessageDialog(null, n+" - "+m+" = "+(n-m));
				break;
			case "*":
				JOptionPane.showMessageDialog(null, n+" * "+m+" = "+(n*m));
				break;
			case "/":
				JOptionPane.showMessageDialog(null, n+" / "+m+" = "+(float)n/m);
				break;
			case "^":
				JOptionPane.showMessageDialog(null, n+" ^ "+m+" = "+Math.pow(n,m));
				break;
			case "%":
				JOptionPane.showMessageDialog(null, n+" % "+m+" = "+(n%m));
				break;
			default:
				JOptionPane.showMessageDialog(null, "El simbolo introducido no es valido, intentalo de nuevo mas tarde!");
		}

	}

}
