package main;

/**
 * 5)Lee un n�mero por teclado e indica sies divisible entre 2 
 * (resto = 0). Si no lo es, tambi�n debemos indicarlo.
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio5App {
	
	public static void main(String[] args) {
		
		String entrada = JOptionPane.showInputDialog("Introduce un numero.");
		double numero = Double.parseDouble(entrada);
		
		if (numero%2 == 0) {
			JOptionPane.showMessageDialog(null, "El numero "+entrada+" es divisible entre 2.");
		} else {
			JOptionPane.showMessageDialog(null, "El numero "+entrada+" no es divisible entre 2.");
		}

	}

}
