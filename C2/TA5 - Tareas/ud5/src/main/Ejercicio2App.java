package main;

/**
 * 2)Declara un String que contenga tu nombre, 
 * despu�s muestra un mensaje de bienvenida por consola. 
 * Por ejemplo: si introduzco �Fernando�, me aparezca �Bienvenido Fernando�.
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio2App {
	
	public static void main(String[] args) {
		
		String name = "David";
		JOptionPane.showMessageDialog(null, "Bienvenido "+name+".");

	}

}
