package main;

/**
 * 3)Modifica la aplicación anterior, para que nos pida 
 * el nombre que queremos introducir 
 * (recuerda usar JOptionPane).
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio3App {
	
	public static void main(String[] args) {
		
		String name = JOptionPane.showInputDialog("Introduce tu nombre");
		JOptionPane.showMessageDialog(null, "Bienvenido "+name+".");

	}

}
