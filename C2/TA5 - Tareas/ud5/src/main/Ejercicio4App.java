package main;

/**
 * 4)Haz una aplicaci�n que calcule el �rea de un circulo (pi*R2). 
 * El radio se pedir� por teclado (recuerda pasar de String a double con Double.parseDouble). 
 * Usa la constante PI y el m�todo pow de Math.
 * 
 * @author David
 */

import javax.swing.JOptionPane;

public class Ejercicio4App {
	
	public static void main(String[] args) {
		
		String entradaRadio = JOptionPane.showInputDialog("Introduce el radio del circulo.");
		double area = (Math.PI * Math.pow(Double.parseDouble(entradaRadio),2));
		JOptionPane.showMessageDialog(null, "El area del circulo es : "+area+".");

	}

}
