package uno;

public class unoApp {
	/* 
	 * 1- Declara dos variables numéricas (con el valor que desees), 
	 * muestra por consola la suma, resta, multiplicación, división 
	 * y módulo (resto de la división). 
	 */
	
	public static void main(String[] args) {
		int n1 = 68;
		int n2 = 1058;
		System.out.println("-> " +n1+ " + " +n2+ " = "+(n1+n2));
		System.out.println("-> " +n1+ " - " +n2+ " = "+(n1-n2));
		System.out.println("-> " +n1+ " * " +n2+ " = "+(n1*n2));
		System.out.println("-> " +n1+ " / " +n2+ " = "+(n1/n2));
		System.out.println("-> " +n1+ " % " +n2+ " = "+(n1%n2));
	}
	
}
