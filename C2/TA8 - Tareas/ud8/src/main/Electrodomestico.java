package main;

class Electrodomestico {
	/*
	 * CONSTANTES
	 */
	protected final float PRECIO_BASE = 100;
	protected final String FCOLOR = "blanco";
	protected final char CONSUMO_ENERGETICO = 'F';
	protected final float PESO = 5;

	/*
	 * ATRIBUTOS
	 */
	private float precioBase;
	private String color;
	private char consumoEnergetico;
	private float peso;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Electrodomestico() {
		setPrecioBase(PRECIO_BASE);
		setColor(FCOLOR);
		setConsumoEnergetico(CONSUMO_ENERGETICO);
		setPeso(PESO);
	}
	
	public Electrodomestico(float precioBase, float peso) {
		setPrecioBase(precioBase);
		setColor(FCOLOR);
		setConsumoEnergetico(CONSUMO_ENERGETICO);
		setPeso(peso);
	}
	
	public Electrodomestico(float precioBase, float peso, String color, char consumoEnergetico) {
		setPrecioBase(precioBase);
		setColor(color);
		setConsumoEnergetico(consumoEnergetico);
		setPeso(peso);
	}

	/*
	 * METODOS SET
	 */
	public void setPrecioBase(float precio_base) {
		this.precioBase = precio_base;
	}

	public void setColor(String entrada) {
		entrada = entrada.toLowerCase(); 
		switch(entrada) {
			case "blanco":
				this.color = entrada;
				break;
				
			case "negro":
				this.color = entrada;
				break;
				
			case "rojo":
				this.color = entrada;
				break;
				
			case "azul":
				this.color = entrada;
				break;
				
			case "gris":
				this.color = entrada;
			break;
			
			default:
				this.color = FCOLOR;
		}
	}

	public void setConsumoEnergetico(char consumo_energetico) {
		this.consumoEnergetico = consumo_energetico;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	/*
	 * METODOS GET
	 */
	public float getPrecioBase() {
		return precioBase;
	}

	public String getColor() {
		return color;
	}

	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	public float getPeso() {
		return peso;
	}

}
