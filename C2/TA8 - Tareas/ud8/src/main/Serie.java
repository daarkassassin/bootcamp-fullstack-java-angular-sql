package main;

class Serie {

	/*
	 * CONSTANTES
	 */
	private final boolean FENTREGADO = false;
	private final int FNUMERO_TEMPORADAS = 3;

	/*
	 * ATRIBUTOS
	 */
	private String titulo;
	private int numeroTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Serie() {
		setTitulo("");
		setNumeroTemporadas(FNUMERO_TEMPORADAS);
		setEntregado(FENTREGADO);
		setGenero("");
		setCreador("");
	}
	public Serie(String titulo, String creador) {
		setTitulo(titulo);
		setNumeroTemporadas(FNUMERO_TEMPORADAS);
		setEntregado(FENTREGADO);
		setGenero("");
		setCreador(creador);
	}
	public Serie(String titulo, String creador, int numeroTemporadas, String genero) {
		setTitulo(titulo);
		setNumeroTemporadas(numeroTemporadas);
		setEntregado(FENTREGADO);
		setGenero(genero);
		setCreador(creador);
	}

	/*
	 * METODOS SET
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setNumeroTemporadas(int numeroTemporadas) {
		this.numeroTemporadas = numeroTemporadas;
	}

	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	/*
	 * METODOS GET
	 */
	public String getTitulo() {
		return titulo;
	}

	public int getNumeroTemporadas() {
		return numeroTemporadas;
	}

	public boolean getEntregado() {
		return entregado;
	}

	public String getGenero() {
		return genero;
	}

	public String getCreador() {
		return creador;
	}

}
