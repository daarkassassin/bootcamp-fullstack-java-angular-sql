package main;

import java.util.Random;

class Password {
	/*
	 *  CONSTANTES
	 */
	private final int FLONGITUD = 8;

	/*
	 *  ATRIBUTOS
	 */
	private String contraseña;
	private int longitud;

	/*
	 *  METODOS CONSTRUCTOR
	 */
	public Password() {
		setLongitud(FLONGITUD);
		setContraseña(null);
	}

	public Password(int longitud) {
		setLongitud(longitud);
		setContraseña(crearContraseña(longitud));
	}

	/*
	 *  METODOS SET
	 */
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	/*
	 *  METODOS GET
	 */
	public String getContraseña() {
		return contraseña;
	}

	public int getLongitud() {
		return longitud;
	}

	/*
	 *  METODOS
	 */
	public String crearContraseña(int longitud) {
		Random r = new Random();
		String rContraseña = "";
		for (int i = 0; i < longitud; i++) {
			rContraseña = rContraseña + "" + r.nextInt(9);
		}
		return rContraseña;
	}

}
