package main;

class Persona {
	/*
	 * CONSTANTES
	 */
	private final char FSEXO = 'H';

	/*
	 * ATRIBUTOS
	 */
	private String nombre;
	private int edad;
	private String dni;
	private char sexo;
	private float peso;
	private float altura;

	/*
	 * METODOS CONSTRUCTOR
	 */
	public Persona() {
		setNombre("");
		setEdad(0);
		setDni(null);
		setSexo(FSEXO);
		setPeso(0);
		setAltura(0);
	}

	public Persona(String nombre, int edad, char sexo) {
		setNombre(nombre);
		setEdad(edad);
		setDni(null);
		setSexo(sexo);
		setPeso(0);
		setAltura(0);
	}

	public Persona(String nombre, int edad, String dni, char sexo, float peso, float altura) {
		setNombre(nombre);
		setEdad(edad);
		setDni(dni);
		setSexo(sexo);
		setPeso(peso);
		setAltura(altura);
	}

	/*
	 * METODOS SET
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	/*
	 * METODOS GET
	 */
	public String getNombre() {
		return nombre;
	}

	public int getEdad() {
		return edad;
	}

	public String getDni() {
		return dni;
	}

	public char getSexo() {
		return sexo;
	}

	public float getPeso() {
		return peso;
	}

	public float getaltura() {
		return altura;
	}

}
