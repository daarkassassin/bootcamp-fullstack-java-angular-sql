package main;

public class mainApp {
	
	public static void main(String[] args) {
		
		Persona persona1 = new Persona("Laura", 18, 'M');
		System.out.println(persona1.getNombre());
		
		Password password1 = new Password(4);
		System.out.println(password1.getContraseña());
		
		Electrodomestico electrodomestico1 = new Electrodomestico(250,90,"GrIs",'E');
		System.out.println(electrodomestico1.getColor());
		
		Serie serie1 = new Serie("Game of Thrones","George R. R. Martin");
		System.out.println(serie1.getTitulo());
		
	}

}
