package Ejercicio9;

import ConectionBD.ConnectionDB;

public class ejercicio9App {
	
	public static void main(String[] args) {
		final String DB_BAME = "Investigadores";
		
		ConnectionDB connection = new ConnectionDB();
		
		connection.createDB(DB_BAME);
		connection.executeQuery(DB_BAME, "CREATE TABLE Facultad (\r\n"
				+ "				codigo INT, \r\n"
				+ "				nombre NVARCHAR(100),\r\n"
				+ "				PRIMARY KEY (codigo)\r\n"
				+ "				);");
		connection.executeQuery(DB_BAME, "CREATE TABLE Investigadores (\r\n"
				+ "				    dni VARCHAR(8), \r\n"
				+ "				    nom_apels NVARCHAR(255), \r\n"
				+ "				    facultad INT NOT NULL,\r\n"
				+ "				    PRIMARY KEY (dni), \r\n"
				+ "				    FOREIGN KEY (facultad) REFERENCES Facultad(codigo)\r\n"
				+ "				);");
		connection.executeQuery(DB_BAME, "CREATE TABLE Equipos (\r\n"
				+ "				    num_serie CHAR(4), \r\n"
				+ "				    nombre NVARCHAR(100),\r\n"
				+ "				    facultad INT NOT NULL,\r\n"
				+ "				    PRIMARY KEY (num_serie), \r\n"
				+ "				    FOREIGN KEY (facultad) REFERENCES Facultad(codigo)\r\n"
				+ "				);");
		connection.executeQuery(DB_BAME, "CREATE TABLE Reserva (\r\n"
				+ "				    dni VARCHAR(8), \r\n"
				+ "				    num_serie CHAR(4), \r\n"
				+ "				    comienzo DATETIME,\r\n"
				+ "				    fin DATETIME,\r\n"
				+ "				    PRIMARY KEY (dni, num_serie), \r\n"
				+ "				    FOREIGN KEY (dni) REFERENCES Investigadores(dni),\r\n"
				+ "				    FOREIGN KEY (num_serie) REFERENCES Equipos(num_serie)\r\n"
				+ "				);");
		
		connection.executeQuery(DB_BAME,"insert into Facultad values \r\n"
				+ "		(10,'test'),\r\n"
				+ "		(20,'test'),\r\n"
				+ "		(30,'test'),\r\n"
				+ "		(40,'test'),\r\n"
				+ "		(50,'test'),\r\n"
				+ "		(60,'test'),\r\n"
				+ "		(70,'test'),\r\n"
				+ "		(80,'test'),\r\n"
				+ "		(90,'test'),\r\n"
				+ "		(01,'test');");
		connection.executeQuery(DB_BAME,"insert into Equipos values \r\n"
				+ "		('1111','proyecto 1',10),\r\n"
				+ "		('1112','proyecto 2',20),\r\n"
				+ "		('1113','proyecto 3',30),\r\n"
				+ "		('1114','proyecto 4',40),\r\n"
				+ "		('1115','proyecto 5',50),\r\n"
				+ "		('1116','proyecto 6',60),\r\n"
				+ "		('1117','proyecto 7',70),\r\n"
				+ "		('1118','proyecto 8',80),\r\n"
				+ "		('1119','proyecto 9',90),\r\n"
				+ "		('1121','proyecto 10',01);\r\n");
		connection.executeQuery(DB_BAME,"insert into Investigadores values\r\n"
				+ "		('1234567A','Investigadores1',10),\r\n"
				+ "		('1234567B','Investigadores2',20),\r\n"
				+ "		('1234567C','Investigadores3',30),\r\n"
				+ "		('1234567D','Investigadores4',40),\r\n"
				+ "		('1234567E','Investigadores5',50),\r\n"
				+ "		('1234567F','Investigadores6',60),\r\n"
				+ "		('1234567J','Investigadores7',70),\r\n"
				+ "		('1234567K','Investigadores8',80),\r\n"
				+ "		('1234567L','Investigadores9',90),\r\n"
				+ "		('1234567M','Investigadores0',01);");
		connection.executeQuery(DB_BAME,"insert into Reserva values\r\n"
				+ "		('1234567A','1111','20120618','20120618'),\r\n"
				+ "		('1234567B','1111','20120618','20120618'),\r\n"
				+ "		('1234567C','1111','20120618','20120618'),\r\n"
				+ "		('1234567D','1111','20120618','20120618'),\r\n"
				+ "		('1234567E','1111','20120618','20120618'),\r\n"
				+ "		('1234567F','1111','20120618','20120618'),\r\n"
				+ "		('1234567J','1111','20120618','20120618'),\r\n"
				+ "		('1234567K','1111','20120618','20120618'),\r\n"
				+ "		('1234567L','1111','20120618','20120618'),\r\n"
				+ "		('1234567M','1111','20120618','20120618');");
		
		connection.getValues(DB_BAME, "select * from Facultad");
		connection.getValues(DB_BAME, "select * from Investigadores");
		connection.getValues(DB_BAME, "select * from Reserva");
		connection.getValues(DB_BAME, "select * from Equipos");
		
		connection.closeConnection();


				

				

				
		
		
		
		

		

		
		
	}

}
