package Ejercicio1;

import ConectionBD.ConnectionDB;

public class ejercicio1App {
	
	public static void main(String[] args) {
		final String DB_BAME = "Tienda_Informatica";
		
		ConnectionDB connection = new ConnectionDB();
		
		connection.createDB(DB_BAME);
		connection.executeQuery(DB_BAME, "CREATE TABLE Fabricantes (codigo INT AUTO_INCREMENT, nombre NVARCHAR(100), PRIMARY KEY (codigo));");
		connection.executeQuery(DB_BAME, "CREATE TABLE Articulos (codigo INT AUTO_INCREMENT, nombre NVARCHAR(100), precio INT, fabricante INT NOT NULL, PRIMARY KEY (codigo), FOREIGN KEY (fabricante) REFERENCES Fabricantes(codigo));");
		connection.executeQuery(DB_BAME, "insert into Fabricantes(nombre) values ('hp'), ('amd'), ('intel'), ('corsair'), ('gigabyte');");
		connection.executeQuery(DB_BAME, "insert into Articulos(nombre,precio,fabricante) values ('raton',120,1), ('teclado',100,3), ('alfombrilla',12,2), ('pantalla',250,2), ('mesa',1000,3);");
		connection.getValues(DB_BAME, "select * from Fabricantes");
		connection.getValues(DB_BAME, "select * from Articulos");
		connection.closeConnection();
		
	}

}
