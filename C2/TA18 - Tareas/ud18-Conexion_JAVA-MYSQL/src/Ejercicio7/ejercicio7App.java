package Ejercicio7;

import ConectionBD.ConnectionDB;

public class ejercicio7App {
	
	public static void main(String[] args) {
		final String DB_BAME = "Cientificos";
		
		ConnectionDB connection = new ConnectionDB();
		
		connection.createDB(DB_BAME);
		connection.executeQuery(DB_BAME, "CREATE TABLE Cientificos (dni VARCHAR(8), nom_apels NVARCHAR(255),PRIMARY KEY (dni));");
		connection.executeQuery(DB_BAME, "CREATE TABLE Proyecto (id_proyecto CHAR(4), nombre NVARCHAR(255), horas INT, PRIMARY KEY (id_proyecto));");
		connection.executeQuery(DB_BAME, "CREATE TABLE Asignado_A (cientifico VARCHAR(8), proyecto CHAR(4), PRIMARY KEY (cientifico, proyecto), FOREIGN KEY (cientifico) REFERENCES Cientificos(dni),FOREIGN KEY (proyecto) REFERENCES Proyecto(id_proyecto));");
		
		connection.executeQuery(DB_BAME, "insert into Cientificos values\r\n"
				+ "		('1234567A','cient 1'),\r\n"
				+ "		('1234567B','cient 2'),\r\n"
				+ "		('1234567C','cient 3'),\r\n"
				+ "		('1234567D','cient 4'),\r\n"
				+ "		('1234567E','cient 5'),\r\n"
				+ "		('1234567F','cient 6'),\r\n"
				+ "		('1234567J','cient 7'),\r\n"
				+ "		('1234567K','cient 8'),\r\n"
				+ "		('1234567L','cient 9'),\r\n"
				+ "		('1234567M','cient 10'),\r\n"
				+ "		('1234567N','cient 11');");
		
		connection.executeQuery(DB_BAME, "insert into Proyecto values\r\n"
				+ "		('123','proyecto 1',100),\r\n"
				+ "		('234','proyecto 2',200),\r\n"
				+ "		('345','proyecto 3',300),\r\n"
				+ "		('456','proyecto 4',400),\r\n"
				+ "		('567','proyecto 5',500),\r\n"
				+ "		('678','proyecto 6',600),\r\n"
				+ "		('789','proyecto 7',700),\r\n"
				+ "		('890','proyecto 8',800),\r\n"
				+ "		('901','proyecto 9',900),\r\n"
				+ "		('012','proyecto 10',1000),\r\n"
				+ "		('013','proyecto 11',1100);");
		
		connection.executeQuery(DB_BAME,"insert into Asignado_A values\r\n"
				+ "		('1234567A','123'),\r\n"
				+ "		('1234567B','234'),\r\n"
				+ "		('1234567C','345'),\r\n"
				+ "		('1234567D','456'),\r\n"
				+ "		('1234567E','567'),\r\n"
				+ "		('1234567F','678'),\r\n"
				+ "		('1234567J','789'),\r\n"
				+ "		('1234567K','890'),\r\n"
				+ "		('1234567L','901'),\r\n"
				+ "		('1234567M','012'),\r\n"
				+ "		('1234567N','013');");
		
		connection.getValues(DB_BAME, "select * from Cientificos");
		connection.getValues(DB_BAME, "select * from Proyecto");
		connection.getValues(DB_BAME, "select * from Asignado_A");
		
		connection.closeConnection();
		
		
		
		

		

		
		
	}

}
