package Ejercicio6;

import ConectionBD.ConnectionDB;

public class Ejercicio6App {

	public static void main(String[] args) {

		final String DATABASE_NAME = "PiezasProveedores";
		
		// Crear objeto conexion
		ConnectionDB connection = new ConnectionDB();
		
        // Crear una base de datos
        connection.createDB(DATABASE_NAME);
        
        // Crear tablas
        connection.executeQuery(DATABASE_NAME, "CREATE TABLE Piezas ( codigo INT AUTO_INCREMENT, nombre NVARCHAR(100), PRIMARY KEY (codigo) );");
        connection.executeQuery(DATABASE_NAME, "CREATE TABLE Proveedores ( id_proveedores CHAR(4), nombre NVARCHAR(100), PRIMARY KEY (id_proveedores) );");
        connection.executeQuery(DATABASE_NAME, "CREATE TABLE Suministra ( codigo_pieza INT, id_proveedor CHAR(4), precio INT, PRIMARY KEY (codigo_pieza, id_proveedor), FOREIGN KEY (codigo_pieza) REFERENCES Piezas(codigo), FOREIGN KEY (id_proveedor) REFERENCES Proveedores(id_proveedores) );");
        
        // Insertar valores
        connection.executeQuery(DATABASE_NAME, "insert into Piezas (nombre) values ('test1')");
        connection.executeQuery(DATABASE_NAME, "insert into Piezas (nombre) values ('test2')");
        connection.executeQuery(DATABASE_NAME, "insert into Piezas (nombre) values ('test3')");
        connection.executeQuery(DATABASE_NAME, "insert into Piezas (nombre) values ('test4')");
        connection.executeQuery(DATABASE_NAME, "insert into Piezas (nombre) values ('test5')");

        connection.executeQuery(DATABASE_NAME, "insert into Proveedores values ('1234', 'nombre1')");
        connection.executeQuery(DATABASE_NAME, "insert into Proveedores values ('1235', 'nombre2')");
        connection.executeQuery(DATABASE_NAME, "insert into Proveedores values ('1236', 'nombre3')");
        connection.executeQuery(DATABASE_NAME, "insert into Proveedores values ('1237', 'nombre4')");
        connection.executeQuery(DATABASE_NAME, "insert into Proveedores values ('1238', 'nombre5')");
        
        connection.executeQuery(DATABASE_NAME, "insert into Suministra values (1, '1234', 10)");
        connection.executeQuery(DATABASE_NAME, "insert into Suministra values (2, '1235', 10)");
        connection.executeQuery(DATABASE_NAME, "insert into Suministra values (3, '1236', 10)");
        connection.executeQuery(DATABASE_NAME, "insert into Suministra values (4, '1237', 10)");
        connection.executeQuery(DATABASE_NAME, "insert into Suministra values (5, '1238', 10)");

        // Mostar valores
        connection.getValues(DATABASE_NAME, "select * from Piezas");
        connection.getValues(DATABASE_NAME, "select * from Proveedores");
        connection.getValues(DATABASE_NAME, "select * from Suministra");
        
        // Cerrar conexion
        connection.closeConnection();

	}

}
