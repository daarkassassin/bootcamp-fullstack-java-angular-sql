![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD13 - Modelo Relacional

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| David Bonet Daga | Master | Team Member |  |   |   |  |
| Xavier Bonet Daga |  | Team Member |  |   |   |  |


#### 2. Description
```
Ejercicios UD13 - Modelo Relacional
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql

```
UD13 - Modelo Relacional / https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Lucidspark Board - lucid.app/
Drive - drive.google.com/

```
