use Ej8_Investigadores

db.createCollection("Facultad")
db.createCollection("Investigadores")
db.createCollection("Equipos")
db.createCollection("Reserva")

db.Facultad.insert({"_id" : "F_1", "nombre" : "Facultad1"})
db.Facultad.insert({"_id" : "F_2", "nombre" : "Facultad2"})
db.Facultad.insert({"_id" : "F_3", "nombre" : "Facultad3"})
db.Facultad.insert({"_id" : "F_4", "nombre" : "Facultad4"})
db.Facultad.insert({"_id" : "F_5", "nombre" : "Facultad5"})

db.Investigadores.insert({"_id" : "12345678A", "nomApels" : "Investigador1", "Facultad" : "F_1"})
db.Investigadores.insert({"_id" : "12345678B", "nomApels" : "Investigador2", "Facultad" : "F_2"})
db.Investigadores.insert({"_id" : "12345678C", "nomApels" : "Investigador3", "Facultad" : "F_3"})
db.Investigadores.insert({"_id" : "12345678D", "nomApels" : "Investigador4", "Facultad" : "F_4"})
db.Investigadores.insert({"_id" : "12345678E", "nomApels" : "Investigador5", "Facultad" : "F_5"})

db.Equipos.insert({"_id" : "12345678A", "nombre" : "Equipo1", "Facultad" : "F_1"})
db.Equipos.insert({"_id" : "12345678B", "nombre" : "Equipo2", "Facultad" : "F_2"})
db.Equipos.insert({"_id" : "12345678C", "nombre" : "Equipo3", "Facultad" : "F_3"})
db.Equipos.insert({"_id" : "12345678D", "nombre" : "Equipo4", "Facultad" : "F_4"})
db.Equipos.insert({"_id" : "12345678E", "nombre" : "Equipo5", "Facultad" : "F_5"})

var date_inicio = new Date(2014, 11, 12, 14, 12);
var date_fin = new Date(2018, 11, 12, 14, 12);
db.Reserva.insert({"Investigador" : "P_1", "Equipo" : "F_1", "comienzo" : date_inicio, "fin" :date_fin})
db.Reserva.insert({"Investigador" : "P_2", "Equipo" : "F_2", "comienzo" : date_inicio, "fin" :date_fin})
db.Reserva.insert({"Investigador" : "P_3", "Equipo" : "F_3", "comienzo" : date_inicio, "fin" :date_fin})
db.Reserva.insert({"Investigador" : "P_4", "Equipo" : "F_4", "comienzo" : date_inicio, "fin" :date_fin})
db.Reserva.insert({"Investigador" : "P_5", "Equipo" : "F_5", "comienzo" : date_inicio, "fin" :date_fin})

db.getCollection("Facultad").find({})
db.getCollection("Investigadores").find({})
db.getCollection("Equipos").find({})
db.getCollection("Reserva").find({})