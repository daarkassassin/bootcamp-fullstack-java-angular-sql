use Ej1_Tienda
db.createCollection("Fabricantes")
db.createCollection("Articulos")

db.Fabricantes.insert({_id: "F_1", Nombre:"Fabricante1"})
db.Fabricantes.insert({_id: "F_2", Nombre:"Fabricante2"})
db.Fabricantes.insert({_id: "F_3", Nombre:"Fabricante3"})
db.Fabricantes.insert({_id: "F_4", Nombre:"Fabricante4"})
db.Fabricantes.insert({_id: "F_5", Nombre:"Fabricante5"})

db.Articulos.insert({_id: "A_1", Nombre:"Articulo1", Precio:"100", Fabricante: "F_1"})
db.Articulos.insert({_id: "A_2", Nombre:"Articulo2", Precio:"100", Fabricante: "F_2"})
db.Articulos.insert({_id: "A_3", Nombre:"Articulo3", Precio:"100", Fabricante: "F_3"})
db.Articulos.insert({_id: "A_4", Nombre:"Articulo4", Precio:"100", Fabricante: "F_4"})
db.Articulos.insert({_id: "A_5", Nombre:"Articulo5", Precio:"100", Fabricante: "F_5"})

db.getCollection("Fabricantes").find({})
db.getCollection("Articulos").find({})


