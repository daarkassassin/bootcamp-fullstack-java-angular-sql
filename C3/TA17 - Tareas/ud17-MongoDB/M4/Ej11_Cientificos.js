use Ej11_Cientificos

db.createCollection("Cientificos")
db.createCollection("Proyectos")
db.createCollection("Asignado_A")

db.Cientificos.insert({"_id" : "12345678A", "nomApels" : "Investigador1"})
db.Cientificos.insert({"_id" : "12345678B", "nomApels" : "Investigador2"})
db.Cientificos.insert({"_id" : "12345678C", "nomApels" : "Investigador3"})
db.Cientificos.insert({"_id" : "12345678D", "nomApels" : "Investigador4"})
db.Cientificos.insert({"_id" : "12345678E", "nomApels" : "Investigador5"})

db.Proyectos.insert({"_id" : "P_1", "nombre" : "Proyecto1", "horas" : 300})
db.Proyectos.insert({"_id" : "P_2", "nombre" : "Proyecto2", "horas" : 50})
db.Proyectos.insert({"_id" : "P_3", "nombre" : "Proyecto3", "horas" : 200})
db.Proyectos.insert({"_id" : "P_4", "nombre" : "Proyecto4", "horas" : 100})
db.Proyectos.insert({"_id" : "P_5", "nombre" : "Proyecto5", "horas" : 500})

db.Asignado_A.insert({"Cientifico" : "12345678A", "Proyecto" : "P_1"})
db.Asignado_A.insert({"Cientifico" : "12345678B", "Proyecto" : "P_2"})
db.Asignado_A.insert({"Cientifico" : "12345678C", "Proyecto" : "P_3"})
db.Asignado_A.insert({"Cientifico" : "12345678D", "Proyecto" : "P_4"})
db.Asignado_A.insert({"Cientifico" : "12345678E", "Proyecto" : "P_5"})

db.getCollection("Cientificos").find({})
db.getCollection("Proyectos").find({})
db.getCollection("Asignado_A").find({})