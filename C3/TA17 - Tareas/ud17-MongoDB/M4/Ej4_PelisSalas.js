use Ej4_PelisSalas
db.createCollection("Salas")
db.createCollection("Peliculas")

db.Peliculas.insert({"_id" : "P_1", "nombre" : "El string perdido", "calificacionEdad" : 18})
db.Peliculas.insert({"_id" : "P_2", "nombre" : "L'array multidimensional", "calificacionEdad" : 16})
db.Peliculas.insert({"_id" : "P_3", "nombre" : "El bucle", "calificacionEdad" : 8})
db.Peliculas.insert({"_id" : "P_4", "nombre" : "Perdidos online", "calificacionEdad" : 18})
db.Peliculas.insert({"_id" : "P_5", "nombre" : "etc", "calificacionEdad" : 3})

db.Salas.insert({ "_id" : "S_1", "nombre" : "sala 1", "_pelicula" : "P_1" })
db.Salas.insert({ "_id" : "S_2", "nombre" : "sala 2", "_pelicula" : "P_2" })
db.Salas.insert({ "_id" : "S_3", "nombre" : "sala 3", "_pelicula" : "P_3" })
db.Salas.insert({ "_id" : "S_4", "nombre" : "sala 4", "_pelicula" : "P_4" })
db.Salas.insert({ "_id" : "S_5", "nombre" : "sala 3D", "_pelicula" : "P_5" })



db.getCollection("Peliculas").find({})
db.getCollection("Salas").find({})