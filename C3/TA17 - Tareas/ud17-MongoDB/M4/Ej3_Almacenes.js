use Ej3_Almacenes
db.createCollection("Almacenes")
db.createCollection("Cajas")

db.Almacenes.insert({_id: "A_1",lugar:"Lugar1",capacidad:1})
db.Almacenes.insert({_id: "A_2",lugar:"Lugar2",capacidad:2})
db.Almacenes.insert({_id: "A_3",lugar:"Lugar3",capacidad:3})
db.Almacenes.insert({_id: "A_4",lugar:"Lugar4",capacidad:4})
db.Almacenes.insert({_id: "A_5",lugar:"Lugar5",capacidad:5})

db.Cajas.insert({_id: "C_1",contenido:"Contenido1",valor:100,almacen: "A_1"})
db.Cajas.insert({_id: "C_2",contenido:"Contenido2",valor:100,almacen: "A_2"})  
db.Cajas.insert({_id: "C_3",contenido:"Contenido3",valor:100,almacen: "A_3"}) 
db.Cajas.insert({_id: "C_4",contenido:"Contenido4",valor:100,almacen: "A_4"}) 
db.Cajas.insert({_id: "C_5",contenido:"Contenido5",valor:100,almacen: "A_5"})

db.getCollection("Almacenes").find({})
db.getCollection("Cajas").find({})