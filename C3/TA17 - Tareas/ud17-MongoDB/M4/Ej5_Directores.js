use Ej5_Directores
db.createCollection("Directores")
db.Directores.insert(
    {
        _id: "1234567A",
        nom_apels:"nom1",
        dni_jefe:"1234567A",
        despacho:"D_1"
    }
)
db.Directores.insert(
    {
        _id: "1234567B",
        nom_apels:"nom2",
        dni_jefe:"1234567C",
        despacho:"D_2"
    }
)
db.Directores.insert(
    {
        _id: "1234567C",
        nom_apels:"nom3",
        dni_jefe:"1234567C",
        despacho:"D_2"
    }
)
db.Directores.insert(
    {
        _id: "1234567Q",
        nom_apels:"nom4",
        dni_jefe:"1234567A",
        despacho:"D_1"
    }
)
db.Directores.insert(
    {
        _id: "1234567W",
        nom_apels:"nom5",
        dni_jefe:"1234567A",
        despacho:"D_1"
    }
)

db.createCollection("Despachos")
db.Despachos.insert(
    {
        _id: "D_1",
        capacidad: 10
    }
)
db.Despachos.insert(
    {
        _id: "D_2",
        capacidad: 10
    }
)
db.Despachos.insert(
    {
        _id: "D_3",
        capacidad: 10
    }
)
db.Despachos.insert(
    {
        _id: "D_4",
        capacidad: 10
    }
)
db.Despachos.insert(
    {
        _id: "D_5",
        capacidad: 10
    }
)

db.getCollection("Directores").find({})
db.getCollection("Despachos").find({})
