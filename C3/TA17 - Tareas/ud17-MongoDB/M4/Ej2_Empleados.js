use Ej2_Empleados
db.createCollection("Empleados")
db.createCollection("Departamentos")

db.Departamentos.insert({ "_id" : "D01", "nombre" : "cientificos", "presupuesto" : 20000 })
db.Departamentos.insert({ "_id" : "D02", "nombre" : "IT", "presupuesto" : 40000 })
db.Departamentos.insert({ "_id" : "D03", "nombre" : "test", "presupuesto" : 1000 })
db.Departamentos.insert({ "_id" : "D04", "nombre" : "lorem", "presupuesto" : 60000 })
db.Departamentos.insert({ "_id" : "D05", "nombre" : "ipsum", "presupuesto" : 80000 })

db.Empleados.insert({"_id" : "47938828C", "nombre" : "David", "apellidos" : "Bonet Daga", "departamento" : "D01"})
db.Empleados.insert({"_id" : "12345678X", "nombre" : "Marcos", "apellidos" : "lorem ipsum", "departamento" : "D02"})
db.Empleados.insert({"_id" : "12345678Z", "nombre" : "Anna", "apellidos" : "ipsum lorem", "departamento" : "D03"})
db.Empleados.insert({"_id" : "12345678Y", "nombre" : "Lara", "apellidos" : "lorimu ipsum", "departamento" : "D04"})
db.Empleados.insert({"_id" : "12345678U", "nombre" : "Xavi", "apellidos" : "psumlor emsum", "departamento" : "D05"})

db.getCollection("Departamentos").find({})
db.getCollection("Empleados").find({})



