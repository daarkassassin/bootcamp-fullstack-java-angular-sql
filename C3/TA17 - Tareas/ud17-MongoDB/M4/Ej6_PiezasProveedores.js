use Ej6_PiezasProveedores
db.createCollection("Piezas")
db.createCollection("Proveedores")
db.createCollection("Suministra")

db.Piezas.insert({"_id" : "P_1", "nombre" : "Tornillo"})
db.Piezas.insert({"_id" : "P_2", "nombre" : "Tuerca"})
db.Piezas.insert({"_id" : "P_3", "nombre" : "Arandela"})
db.Piezas.insert({"_id" : "P_4", "nombre" : "Varilla"})
db.Piezas.insert({"_id" : "P_5", "nombre" : "LaminaHierro"})

db.Proveedores.insert({"_id" : "F_1", "nombre" : "Proveedor1"})
db.Proveedores.insert({"_id" : "F_2", "nombre" : "Proveedor2"})
db.Proveedores.insert({"_id" : "F_3", "nombre" : "Proveedor3"})
db.Proveedores.insert({"_id" : "F_4", "nombre" : "Proveedor4"})
db.Proveedores.insert({"_id" : "F_5", "nombre" : "Proveedor5"})

db.Suministra.insert({"Pieza" : "P_1", "Proveedor" : "F_1", precio:20})
db.Suministra.insert({"Pieza" : "P_2", "Proveedor" : "F_2", precio:30})
db.Suministra.insert({"Pieza" : "P_3", "Proveedor" : "F_3", precio:40})
db.Suministra.insert({"Pieza" : "P_4", "Proveedor" : "F_4", precio:50})
db.Suministra.insert({"Pieza" : "P_5", "Proveedor" : "F_5", precio:60})


db.getCollection("Piezas").find({})
db.getCollection("Proveedores").find({})
db.getCollection("Suministra").find({})