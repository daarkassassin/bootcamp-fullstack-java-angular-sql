![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD15 - DML1

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| David Bonet Daga | Master | Team Member |  |   |   |  |
| Xavier Bonet Daga |  | Team Member |  |   |   |  |


#### 2. Description
```
Ejercicios UD15 - DML1
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql

```
UD15 - DML1 / https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
MySQL Workbench - https://www.mysql.com/products/workbench/
Sql Server [Fedora]

```
