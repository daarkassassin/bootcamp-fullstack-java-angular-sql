USE UD14_Ejercicio7;
ALTER TABLE Directores CHANGE COLUMN dni_jefe dni_jefe varchar(8) NULL;

insert into Despachos values
(0,10),
(1,11),
(2,20),
(3,21),
(4,30),
(5,31),
(6,40),
(7,41),
(8,50),
(9,51),
(10,60);

insert into Directores values
('02345678','Fulanito',null,0),
('12345678','Fulanito','02345678',0),
('22345678','Fulanito','02345678',1),
('32345678','Fulanito','02345678',1),
('42345678','Fulanito','02345678',1),
('52345678','Fulanito','02345678',2),
('62345678','Fulanito','02345678',2),
('72345678','Fulanito','02345678',2),
('82345678','Fulanito','02345678',3),
('92345678','Fulanito','02345678',3),
('01345678','Fulanito','02345678',3);

select * from Directores where dni_jefe = '02345678' and despacho = 3;


