USE UD14_Ejercicio10;

insert into Productos (nombre, precio) values 
('lorem1',100), 
('lorem2',200), 
('lorem3',300), 
('lorem4',400), 
('lorem5',500),
('lorem6',600), 
('lorem7',700), 
('lorem8',800), 
('lorem9',900), 
('lorem10',1000), 
('lorem11',1100);
select * from Productos;

insert into Cajeros(nomApels) values 
('test'),
('test'),
('test'),
('test'),
('test'),
('test'),
('test'),
('test'),
('test'),
('test');
select * from Cajeros;

insert into Maquinas_registradoras(piso) values 
(10),
(20),
(30),
(40),
(50),
(60),
(70),
(80),
(90),
(100);
select * from Maquinas_registradoras;

insert into Venta values 
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10);
select * from Venta;

