-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: 192.168.1.200    Database: UD14_Ejercicio7
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `UD14_Ejercicio7`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `UD14_Ejercicio7` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `UD14_Ejercicio7`;

--
-- Table structure for table `Despachos`
--

DROP TABLE IF EXISTS `Despachos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Despachos` (
  `numero` int NOT NULL,
  `capacidad` int DEFAULT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Despachos`
--

LOCK TABLES `Despachos` WRITE;
/*!40000 ALTER TABLE `Despachos` DISABLE KEYS */;
INSERT INTO `Despachos` VALUES (0,10),(1,11),(2,20),(3,21),(4,30),(5,31),(6,40),(7,41),(8,50),(9,51),(10,60);
/*!40000 ALTER TABLE `Despachos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Directores`
--

DROP TABLE IF EXISTS `Directores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Directores` (
  `dni` varchar(8) NOT NULL,
  `nom_apels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dni_jefe` varchar(8) DEFAULT NULL,
  `despacho` int NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `dni_jefe` (`dni_jefe`),
  KEY `despacho` (`despacho`),
  CONSTRAINT `Directores_ibfk_1` FOREIGN KEY (`dni_jefe`) REFERENCES `Directores` (`dni`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Directores_ibfk_2` FOREIGN KEY (`despacho`) REFERENCES `Despachos` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Directores`
--

LOCK TABLES `Directores` WRITE;
/*!40000 ALTER TABLE `Directores` DISABLE KEYS */;
INSERT INTO `Directores` VALUES ('01345678','Fulanito','02345678',3),('02345678','Fulanito',NULL,0),('12345678','Fulanito','02345678',0),('22345678','Fulanito','02345678',1),('32345678','Fulanito','02345678',1),('42345678','Fulanito','02345678',1),('52345678','Fulanito','02345678',2),('62345678','Fulanito','02345678',2),('72345678','Fulanito','02345678',2),('82345678','Fulanito','02345678',3),('92345678','Fulanito','02345678',3);
/*!40000 ALTER TABLE `Directores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `UD14_Ejercicio11`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `UD14_Ejercicio11` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `UD14_Ejercicio11`;

--
-- Table structure for table `Equipos`
--

DROP TABLE IF EXISTS `Equipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Equipos` (
  `num_serie` char(4) NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `facultad` int NOT NULL,
  PRIMARY KEY (`num_serie`),
  KEY `facultad` (`facultad`),
  CONSTRAINT `Equipos_ibfk_1` FOREIGN KEY (`facultad`) REFERENCES `Facultad` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Equipos`
--

LOCK TABLES `Equipos` WRITE;
/*!40000 ALTER TABLE `Equipos` DISABLE KEYS */;
INSERT INTO `Equipos` VALUES ('1111','proyecto 1',10),('1112','proyecto 2',20),('1113','proyecto 3',30),('1114','proyecto 4',40),('1115','proyecto 5',50),('1116','proyecto 6',60),('1117','proyecto 7',70),('1118','proyecto 8',80),('1119','proyecto 9',90),('1121','proyecto 10',1);
/*!40000 ALTER TABLE `Equipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Facultad`
--

DROP TABLE IF EXISTS `Facultad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Facultad` (
  `codigo` int NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Facultad`
--

LOCK TABLES `Facultad` WRITE;
/*!40000 ALTER TABLE `Facultad` DISABLE KEYS */;
INSERT INTO `Facultad` VALUES (1,'test'),(10,'test'),(20,'test'),(30,'test'),(40,'test'),(50,'test'),(60,'test'),(70,'test'),(80,'test'),(90,'test');
/*!40000 ALTER TABLE `Facultad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Investigadores`
--

DROP TABLE IF EXISTS `Investigadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Investigadores` (
  `dni` varchar(8) NOT NULL,
  `nom_apels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `facultad` int NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `facultad` (`facultad`),
  CONSTRAINT `Investigadores_ibfk_1` FOREIGN KEY (`facultad`) REFERENCES `Facultad` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Investigadores`
--

LOCK TABLES `Investigadores` WRITE;
/*!40000 ALTER TABLE `Investigadores` DISABLE KEYS */;
INSERT INTO `Investigadores` VALUES ('1234567A','Investigadores 1',10),('1234567B','Investigadores 2',20),('1234567C','Investigadores 3',30),('1234567D','Investigadores 4',40),('1234567E','Investigadores 5',50),('1234567F','Investigadores 6',60),('1234567J','Investigadores 7',70),('1234567K','Investigadores 8',80),('1234567L','Investigadores 9',90),('1234567M','Investigadores 10',1);
/*!40000 ALTER TABLE `Investigadores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reserva`
--

DROP TABLE IF EXISTS `Reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Reserva` (
  `dni` varchar(8) NOT NULL,
  `num_serie` char(4) NOT NULL,
  `comienzo` datetime DEFAULT NULL,
  `fin` datetime DEFAULT NULL,
  PRIMARY KEY (`dni`,`num_serie`),
  KEY `num_serie` (`num_serie`),
  CONSTRAINT `Reserva_ibfk_1` FOREIGN KEY (`dni`) REFERENCES `Investigadores` (`dni`),
  CONSTRAINT `Reserva_ibfk_2` FOREIGN KEY (`num_serie`) REFERENCES `Equipos` (`num_serie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reserva`
--

LOCK TABLES `Reserva` WRITE;
/*!40000 ALTER TABLE `Reserva` DISABLE KEYS */;
INSERT INTO `Reserva` VALUES ('1234567A','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567B','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567C','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567D','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567E','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567F','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567J','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567K','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567L','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567M','1111','2012-06-18 00:00:00','2012-06-18 00:00:00');
/*!40000 ALTER TABLE `Reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `UD14_Ejercicio10`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `UD14_Ejercicio10` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `UD14_Ejercicio10`;

--
-- Table structure for table `Cajeros`
--

DROP TABLE IF EXISTS `Cajeros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Cajeros` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nomApels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cajeros`
--

LOCK TABLES `Cajeros` WRITE;
/*!40000 ALTER TABLE `Cajeros` DISABLE KEYS */;
INSERT INTO `Cajeros` VALUES (1,'test'),(2,'test'),(3,'test'),(4,'test'),(5,'test'),(6,'test'),(7,'test'),(8,'test'),(9,'test'),(10,'test');
/*!40000 ALTER TABLE `Cajeros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Maquinas_registradoras`
--

DROP TABLE IF EXISTS `Maquinas_registradoras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Maquinas_registradoras` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `piso` int DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Maquinas_registradoras`
--

LOCK TABLES `Maquinas_registradoras` WRITE;
/*!40000 ALTER TABLE `Maquinas_registradoras` DISABLE KEYS */;
INSERT INTO `Maquinas_registradoras` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,10),(12,20),(13,30),(14,40),(15,50),(16,60),(17,70),(18,80),(19,90),(20,100);
/*!40000 ALTER TABLE `Maquinas_registradoras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Productos`
--

DROP TABLE IF EXISTS `Productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Productos` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `precio` int DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Productos`
--

LOCK TABLES `Productos` WRITE;
/*!40000 ALTER TABLE `Productos` DISABLE KEYS */;
INSERT INTO `Productos` VALUES (1,'lorem1',100),(2,'lorem2',200),(3,'lorem3',300),(4,'lorem4',400),(5,'lorem5',500),(6,'lorem6',600),(7,'lorem7',700),(8,'lorem8',800),(9,'lorem9',900),(10,'lorem10',1000),(11,'lorem11',1100);
/*!40000 ALTER TABLE `Productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Venta`
--

DROP TABLE IF EXISTS `Venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Venta` (
  `cajero` int NOT NULL,
  `maquina` int NOT NULL,
  `producto` int NOT NULL,
  KEY `cajero` (`cajero`),
  KEY `maquina` (`maquina`),
  KEY `producto` (`producto`),
  CONSTRAINT `Venta_ibfk_1` FOREIGN KEY (`cajero`) REFERENCES `Cajeros` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Venta_ibfk_2` FOREIGN KEY (`maquina`) REFERENCES `Maquinas_registradoras` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Venta_ibfk_3` FOREIGN KEY (`producto`) REFERENCES `Productos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Venta`
--

LOCK TABLES `Venta` WRITE;
/*!40000 ALTER TABLE `Venta` DISABLE KEYS */;
INSERT INTO `Venta` VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,5,5),(6,6,6),(7,7,7),(8,8,8),(9,9,9),(10,10,10);
/*!40000 ALTER TABLE `Venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `UD14_Ejercicio9`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `UD14_Ejercicio9` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `UD14_Ejercicio9`;

--
-- Table structure for table `Asignado_A`
--

DROP TABLE IF EXISTS `Asignado_A`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Asignado_A` (
  `cientifico` varchar(8) NOT NULL,
  `proyecto` char(4) NOT NULL,
  PRIMARY KEY (`cientifico`,`proyecto`),
  KEY `proyecto` (`proyecto`),
  CONSTRAINT `Asignado_A_ibfk_1` FOREIGN KEY (`cientifico`) REFERENCES `Cientificos` (`dni`),
  CONSTRAINT `Asignado_A_ibfk_2` FOREIGN KEY (`proyecto`) REFERENCES `Proyecto` (`id_proyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Asignado_A`
--

LOCK TABLES `Asignado_A` WRITE;
/*!40000 ALTER TABLE `Asignado_A` DISABLE KEYS */;
INSERT INTO `Asignado_A` VALUES ('1234567M','012'),('1234567N','013'),('1234567A','123'),('1234567B','234'),('1234567C','345'),('1234567D','456'),('1234567E','567'),('1234567F','678'),('1234567J','789'),('1234567K','890'),('1234567L','901');
/*!40000 ALTER TABLE `Asignado_A` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cientificos`
--

DROP TABLE IF EXISTS `Cientificos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Cientificos` (
  `dni` varchar(8) NOT NULL,
  `nom_apels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cientificos`
--

LOCK TABLES `Cientificos` WRITE;
/*!40000 ALTER TABLE `Cientificos` DISABLE KEYS */;
INSERT INTO `Cientificos` VALUES ('1234567A','cient 1'),('1234567B','cient 2'),('1234567C','cient 3'),('1234567D','cient 4'),('1234567E','cient 5'),('1234567F','cient 6'),('1234567J','cient 7'),('1234567K','cient 8'),('1234567L','cient 9'),('1234567M','cient 10'),('1234567N','cient 11');
/*!40000 ALTER TABLE `Cientificos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proyecto`
--

DROP TABLE IF EXISTS `Proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Proyecto` (
  `id_proyecto` char(4) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `horas` int DEFAULT NULL,
  PRIMARY KEY (`id_proyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proyecto`
--

LOCK TABLES `Proyecto` WRITE;
/*!40000 ALTER TABLE `Proyecto` DISABLE KEYS */;
INSERT INTO `Proyecto` VALUES ('012','proyecto 10',1000),('013','proyecto 11',1100),('123','proyecto 1',100),('234','proyecto 2',200),('345','proyecto 3',300),('456','proyecto 4',400),('567','proyecto 5',500),('678','proyecto 6',600),('789','proyecto 7',700),('890','proyecto 8',800),('901','proyecto 9',900);
/*!40000 ALTER TABLE `Proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `UD14_Ejercicio8`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `UD14_Ejercicio8` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `UD14_Ejercicio8`;

--
-- Table structure for table `Piezas`
--

DROP TABLE IF EXISTS `Piezas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Piezas` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Piezas`
--

LOCK TABLES `Piezas` WRITE;
/*!40000 ALTER TABLE `Piezas` DISABLE KEYS */;
INSERT INTO `Piezas` VALUES (1,'tornillo'),(2,'ruesca'),(3,'arandela'),(4,'varilla'),(5,'aaaaa'),(6,'bbbbb'),(7,'ccccc'),(8,'ddddd'),(9,'eeeee'),(10,'fffff'),(11,'jjjjjj');
/*!40000 ALTER TABLE `Piezas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proveedores`
--

DROP TABLE IF EXISTS `Proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Proveedores` (
  `id_proveedores` char(4) NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_proveedores`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proveedores`
--

LOCK TABLES `Proveedores` WRITE;
/*!40000 ALTER TABLE `Proveedores` DISABLE KEYS */;
INSERT INTO `Proveedores` VALUES ('aaab','prov1'),('aaba','prov2'),('abaa','prov3'),('babb','prov4'),('bbab','prov5'),('bbca','prov8'),('bbcb','prov7'),('bcbb','prov6'),('cacc','prov10'),('ccac','prov9'),('ccca','prov11');
/*!40000 ALTER TABLE `Proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Suministra`
--

DROP TABLE IF EXISTS `Suministra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Suministra` (
  `codigo_pieza` int NOT NULL,
  `id_proveedor` char(4) NOT NULL,
  `precio` int DEFAULT NULL,
  PRIMARY KEY (`codigo_pieza`,`id_proveedor`),
  KEY `id_proveedor` (`id_proveedor`),
  CONSTRAINT `Suministra_ibfk_1` FOREIGN KEY (`codigo_pieza`) REFERENCES `Piezas` (`codigo`),
  CONSTRAINT `Suministra_ibfk_2` FOREIGN KEY (`id_proveedor`) REFERENCES `Proveedores` (`id_proveedores`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Suministra`
--

LOCK TABLES `Suministra` WRITE;
/*!40000 ALTER TABLE `Suministra` DISABLE KEYS */;
INSERT INTO `Suministra` VALUES (1,'aaab',100),(2,'aaab',120),(3,'aaab',90),(4,'aaba',60),(5,'aaba',50),(6,'aaba',22),(7,'abaa',21),(8,'abaa',23),(9,'abaa',36),(10,'babb',39),(11,'babb',3789);
/*!40000 ALTER TABLE `Suministra` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-18 20:15:28
