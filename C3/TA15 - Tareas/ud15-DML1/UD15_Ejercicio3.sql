USE UD14_Ejercicio9;

insert into Cientificos values
('1234567A','cient 1'),
('1234567B','cient 2'),
('1234567C','cient 3'),
('1234567D','cient 4'),
('1234567E','cient 5'),
('1234567F','cient 6'),
('1234567J','cient 7'),
('1234567K','cient 8'),
('1234567L','cient 9'),
('1234567M','cient 10'),
('1234567N','cient 11');
select * from Cientificos;

insert into Proyecto values
('123','proyecto 1',100),
('234','proyecto 2',200),
('345','proyecto 3',300),
('456','proyecto 4',400),
('567','proyecto 5',500),
('678','proyecto 6',600),
('789','proyecto 7',700),
('890','proyecto 8',800),
('901','proyecto 9',900),
('012','proyecto 10',1000),
('013','proyecto 11',1100);
select * from Proyecto;

insert into Asignado_A values
('1234567A','123'),
('1234567B','234'),
('1234567C','345'),
('1234567D','456'),
('1234567E','567'),
('1234567F','678'),
('1234567J','789'),
('1234567K','890'),
('1234567L','901'),
('1234567M','012'),
('1234567N','013');
select * from Asignado_A;
