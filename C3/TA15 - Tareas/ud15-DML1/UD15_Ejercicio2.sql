USE UD14_Ejercicio8;
drop table Suministra;
drop table Piezas;

CREATE TABLE Piezas (
    codigo INT AUTO_INCREMENT, 
    nombre NVARCHAR(100),
       PRIMARY KEY (codigo)
);

CREATE TABLE Suministra (
    codigo_pieza INT NOT NULL, 
    id_proveedor CHAR(4), 
    precio INT, 
    PRIMARY KEY (codigo_pieza, id_proveedor), 
    FOREIGN KEY (codigo_pieza) REFERENCES Piezas(codigo),
    FOREIGN KEY (id_proveedor) REFERENCES Proveedores(id_proveedores)
);

insert into Piezas (nombre) values 
('tornillo'), ('ruesca'), ('arandela'), ('varilla'), ('aaaaa'),
('bbbbb'), ('ccccc'), ('ddddd'), ('eeeee'), ('fffff'), ('jjjjjj');

select * from Piezas;

insert into Proveedores values
('aaab','prov1'),
('aaba','prov2'),
('abaa','prov3'),
('babb','prov4'),
('bbab','prov5'),
('bcbb','prov6'),
('bbcb','prov7'),
('bbca','prov8'),
('ccac','prov9'),
('cacc','prov10'),
('ccca','prov11');
select * from Proveedores;

insert into Suministra values
(1,'aaab',100),
(2,'aaab',120),
(3,'aaab',90),
(4,'aaba',60),
(5,'aaba',50),
(6,'aaba',22),
(7,'abaa',21),
(8,'abaa',23),
(9,'abaa',36),
(10,'babb',39),
(11,'babb',3789);

select * from Suministra;

