CREATE DATABASE UD14_Ejercicio8;
USE UD14_Ejercicio8;

CREATE TABLE Piezas (
codigo INT, 
nombre NVARCHAR(100),
PRIMARY KEY (codigo)
);

CREATE TABLE Proveedores (
id_proveedores CHAR(4), 
nombre NVARCHAR(100),
PRIMARY KEY (id_proveedores)
);

CREATE TABLE Suministra (
codigo_pieza INT, 
id_proveedor CHAR(4), 
precio INT, 
PRIMARY KEY (codigo_pieza, id_proveedor), 
FOREIGN KEY (codigo_pieza) 
REFERENCES Piezas(codigo)
ON DELETE cascade
ON UPDATE CASCADE,
FOREIGN KEY (id_proveedor) 
REFERENCES Proveedores(id_proveedores) 
ON DELETE cascade
ON UPDATE CASCADE
);
