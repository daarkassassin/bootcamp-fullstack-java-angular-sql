CREATE DATABASE UD14_Ejercicio6;
USE UD14_Ejercicio6;

CREATE TABLE Peliculas (
codigo INT AUTO_INCREMENT, 
nombre NVARCHAR(100), 
calificacion_edad INT,
PRIMARY KEY (codigo)
);

CREATE TABLE Salas (
codigo INT AUTO_INCREMENT, 
nombre NVARCHAR(100), 
pelicula INT NOT NULL, 
PRIMARY KEY (codigo), 
FOREIGN KEY (pelicula) 
REFERENCES Peliculas(codigo)
ON DELETE cascade
ON UPDATE CASCADE
);
