CREATE DATABASE UD14_Ejercicio10;
USE UD14_Ejercicio10;

CREATE TABLE Productos (
codigo INT auto_increment, 
nombre NVARCHAR(100),
precio int,
PRIMARY KEY (codigo)
);

CREATE TABLE Cajeros (
codigo INT auto_increment, 
nomApels NVARCHAR(255),
PRIMARY KEY (codigo)
);

CREATE TABLE Maquinas_registradoras (
codigo INT auto_increment, 
piso int,
PRIMARY KEY (codigo)
);

CREATE TABLE Venta (
cajero INT NOT NULL,
maquina INT NOT NULL,
producto INT NOT NULL,
foreign key(cajero)
references Cajeros(codigo)
ON delete cascade
on update cascade,
foreign key(maquina)
references Maquinas_registradoras(codigo)
ON delete cascade
on update cascade,
foreign key(producto)
references Productos(codigo)
ON delete cascade
on update cascade
);