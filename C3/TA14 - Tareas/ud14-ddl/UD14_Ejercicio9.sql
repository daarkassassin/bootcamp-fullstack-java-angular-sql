CREATE DATABASE UD14_Ejercicio9;
USE UD14_Ejercicio9;

CREATE TABLE Cientificos (
    dni VARCHAR(8), 
    nom_apels NVARCHAR(255),
       PRIMARY KEY (dni)
);

CREATE TABLE Proyecto (
    id_proyecto CHAR(4), 
    nombre NVARCHAR(255),
    horas INT,
    PRIMARY KEY (id_proyecto)
);

CREATE TABLE Asignado_A (
    cientifico VARCHAR(8), 
    proyecto CHAR(4), 
    PRIMARY KEY (cientifico, proyecto), 
    FOREIGN KEY (cientifico) REFERENCES Cientificos(dni),
    FOREIGN KEY (proyecto) REFERENCES Proyecto(id_proyecto)
);
