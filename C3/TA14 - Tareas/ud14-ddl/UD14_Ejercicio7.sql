CREATE DATABASE UD14_Ejercicio7;

USE UD14_Ejercicio7;

CREATE TABLE Despachos (
numero INT, 
capacidad INT,
PRIMARY KEY (numero)
);

CREATE TABLE Directores (
dni VARCHAR(8), 
nom_apels NVARCHAR(255), 
dni_jefe VARCHAR(8) NOT NULL, 
despacho INT NOT NULL, 
PRIMARY KEY (dni), 
FOREIGN KEY (dni_jefe) 
REFERENCES Directores(dni)
ON DELETE cascade
ON UPDATE CASCADE,
FOREIGN KEY (despacho) 
REFERENCES Despachos(numero)
ON DELETE cascade
ON UPDATE CASCADE
);
