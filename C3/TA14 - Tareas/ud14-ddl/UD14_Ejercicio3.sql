CREATE DATABASE UD14_Ejercicio3;
USE UD14_Ejercicio3;

CREATE TABLE Fabricantes (
codigo INT AUTO_INCREMENT, 
nombre NVARCHAR(100), 
PRIMARY KEY (codigo)
);

CREATE TABLE Articulos (
codigo INT AUTO_INCREMENT, 
nombre NVARCHAR(100), 
precio INT, 
fabricante INT NOT NULL, 
PRIMARY KEY (codigo), 
FOREIGN KEY (fabricante) 
REFERENCES Fabricantes(codigo)
);