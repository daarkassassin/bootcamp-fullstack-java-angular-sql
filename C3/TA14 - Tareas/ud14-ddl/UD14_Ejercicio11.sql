CREATE DATABASE UD14_Ejercicio11;

USE UD14_Ejercicio11;

CREATE TABLE Facultad (
codigo INT, 
nombre NVARCHAR(100),
PRIMARY KEY (codigo)
);

CREATE TABLE Investigadores (
    dni VARCHAR(8), 
    nom_apels NVARCHAR(255), 
    facultad INT NOT NULL,
    PRIMARY KEY (dni), 
    FOREIGN KEY (facultad) REFERENCES Facultad(codigo)
);

CREATE TABLE Equipos (
    num_serie CHAR(4), 
    nombre NVARCHAR(100),
    facultad INT NOT NULL,
    PRIMARY KEY (num_serie), 
    FOREIGN KEY (facultad) REFERENCES Facultad(codigo)
);

CREATE TABLE Reserva (
    dni VARCHAR(8), 
    num_serie CHAR(4), 
    comienzo DATETIME,
    fin DATETIME,
    PRIMARY KEY (dni, num_serie), 
    FOREIGN KEY (dni) REFERENCES Investigadores(dni),
    FOREIGN KEY (num_serie) REFERENCES Equipos(num_serie)
);
