CREATE DATABASE UD14_Ejercicio5;
USE UD14_Ejercicio5;

CREATE TABLE Almacenes (
codigo INT AUTO_INCREMENT, 
lugar NVARCHAR(100), 
capacidad INT,
PRIMARY KEY (codigo)
);

CREATE TABLE Cajas (
numReferencia CHAR(5), 
contenido NVARCHAR(100), 
valor INT, 
almacen INT NOT NULL, 
PRIMARY KEY (numReferencia), 
FOREIGN KEY (almacen) 
REFERENCES Almacenes(codigo)
ON DELETE cascade
ON UPDATE CASCADE
);
