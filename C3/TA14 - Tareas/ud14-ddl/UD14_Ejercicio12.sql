CREATE DATABASE UD14_Ejercicio12;
USE UD14_Ejercicio12;

CREATE TABLE Profesores (
dni VARCHAR(8),
nombre VARCHAR(100),
apellido1 VARCHAR(100),
apellido2 VARCHAR(100),
direccion VARCHAR(255),
titulo VARCHAR(255),
gana Int,
PRIMARY KEY (dni)
);

CREATE TABLE Cursos (
cod_curso INT auto_increment, 
nombre_curso VARCHAR(100),
dni_profesor VARCHAR(8) Not NULL,
max_alumnos INT,
fecha_inicio VARCHAR(16),
fecha_fin VARCHAR(16),
num_horas Int,
PRIMARY KEY (cod_curso),
foreign key(dni_profesor)
references Profesores(dni)
ON delete cascade
on update cascade
);

CREATE TABLE Alumnos (
id_alumno INT auto_increment, 
nombre VARCHAR(100),
apellido1 VARCHAR(100),
apellido2 VARCHAR(100),
dni VARCHAR(8),
direccion VARCHAR(255),
sexo char,
fecha_nacimiento VARCHAR(16),
cod_curso Int NOT NULL,
PRIMARY KEY (id_alumno),
foreign key(cod_curso)
references Cursos(cod_curso)
ON delete cascade
on update cascade
);