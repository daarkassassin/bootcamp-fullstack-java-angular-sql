CREATE DATABASE UD14_Ejercicio4;
USE UD14_Ejercicio4;

CREATE TABLE Departamentos (
codigo INT NOT NULL,
nombre nVARCHAR(60),
presupuesto INT,
PRIMARY KEY (codigo)
);

CREATE TABLE Empleados (
dni varchar(8) NOT NULL,
departamento INT NOT NULL,
nombre nVARCHAR(100),
apellidos nVARCHAR(255),
primary key (dni),
foreign key(departamento)
REFERENCES Departamentos(codigo)
ON DELETE cascade
ON UPDATE CASCADE
);