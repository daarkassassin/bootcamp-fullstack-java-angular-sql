CREATE DATABASE UD14_Ejercicio2;
USE UD14_Ejercicio2;

CREATE TABLE Editorial (
claveeditorial SMALLINT NOT NULL,
nombre VARCHAR(60),
direccion VARCHAR(60),
telefono VARCHAR(15),
PRIMARY KEY (claveeditorial)
);

CREATE TABLE Libro (
clavelibro INT NOT NULL,
titulo VARCHAR(60),
idioma VARCHAR(15),
formato VARCHAR(15),
claveeditorial SMALLINT,
primary key (clavelibro),
key(claveeditorial),
foreign key(claveeditorial)
REFERENCES Editorial(claveeditorial)
ON DELETE SET NULL
ON UPDATE CASCADE
);

CREATE table Tema(
clavetema SMALLINT NOT NULL,
nombre VARCHAR(40),
primary key (clavetema)
);

CREATE table Autor(
claveautor INT NOT NULL,
nombre varchar(60),
primary key(claveautor)
);

CREATE TABLE Ejemplar (
claveejemplar INT NOT NULL,
clavelibro INT NOT NULL,
numeroorden smallint NOT NULL,
edicion SMALLINT,
ubicacion varchar(15),
categoria CHAR,
primary key (claveejemplar),
foreign key (clavelibro)
references Libro(clavelibro)
ON delete cascade
on update cascade
);

CREATE TABLE Socio (
clavesocio INT NOT null,
nombre VARCHAR(60),
direccion VARCHAR(60),
telefono VARCHAR(15),
categoria CHAR,
PRIMARY KEY (clavesocio)
);

CREATE TABLE Prestamo (
clavesocio INT NOT NULL,
claveejemplar INT NOT NULL,
numeroorden SMALLINT,
fecha_prestamo DATE NOT NULL,
fecha_devolucion DATE DEFAULT NULL,
notas BLOB,
foreign key(clavesocio)
references Socio(clavesocio)
ON delete cascade
on update cascade,
foreign key(claveejemplar)
references Ejemplar(claveejemplar)
ON delete cascade
on update cascade
);

create table Trata_Sobre (
clavelibro INT NOT NULL,
clavetema smallint NOT NULL,
foreign key(clavelibro)
references Libro(clavelibro)
ON delete cascade
on update cascade,
foreign key(clavetema)
references Tema(clavetema)
ON delete cascade
on update cascade
);

create table Escrito_Por (
clavelibro INT NOT NULL,
claveautor INT NOT NULL,
foreign key(clavelibro)
references Libro(clavelibro)
ON delete cascade
on update cascade,
foreign key(claveautor)
references Autor(claveautor)
ON delete cascade
on update cascade
);