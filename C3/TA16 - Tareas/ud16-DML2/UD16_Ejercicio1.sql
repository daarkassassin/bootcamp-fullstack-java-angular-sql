USE actividades;
insert into fabricantes (NOMBRE) values ('fabricante 1'),('fabricante 2'),('fabricante 3'),('fabricante 4'),('fabricante 5'),('fabricante 6'),('fabricante 7'),('fabricante 8'),('fabricante 9'),('fabricante 10');
insert into articulos (NOMBRE, PRECIO, FABRICANTE) values ('Articulo 1', 70, 7),('Articulo 2', 50, 7),('Articulo 3', 50, 7),('Articulo 4', 50, 7),('Articulo 5', 50, 7),('Articulo 6', 50, 7),('Articulo 7', 50, 7),('Articulo 8', 50, 7),('Articulo 9', 50, 7),('Articulo 10', 50, 7);
INSERT INTO `fabricantes`(NOMBRE) VALUES ('test'),('test'),('test'),('test'),('test'),('test');
INSERT INTO `articulos`(NOMBRE, PRECIO, FABRICANTE) VALUES ('test',100,5),('test',100,6),('test',100,4),('test',100,6),('test',100,1),('test',100,2),('test',100,2),('test',100,3),('test',100,3),('test',100,2);
--


-- 1.1
select NOMBRE from articulos;

-- 1.2
select NOMBRE, PRECIO from articulos;

-- 1.3
select NOMBRE from articulos where PRECIO <= 200;

-- 1.4
select * from articulos where PRECIO >= 60 and PRECIO <= 120;

-- 1.5
select NOMBRE, (PRECIO * 166.386) as PRECIO_PESETAS from articulos;

-- 1.6
select avg(PRECIO)  from articulos;

-- 1.7
select avg(PRECIO)  from articulos where CODIGO = 2;

-- 1.8
select count(*) from articulos where PRECIO >=180;

-- 1.9
select NOMBRE, PRECIO from articulos where PRECIO >= 180 order by PRECIO desc, Nombre asc;

-- 1.10
select  *  from articulos A, fabricantes F where A.FABRICANTE = F.CODIGO;

-- 1.11
select  A.NOMBRE, A.PRECIO, F.NOMBRE as FABRICANTE  from articulos A, fabricantes F where A.FABRICANTE = F.CODIGO;

-- 1.12
select  avg(PRECIO) as PRECIO_MEDIO, FABRICANTE from articulos group by FABRICANTE;

-- 1.13
select  avg(PRECIO) as PRECIO_MEDIO , F.NOMBRE from articulos A, fabricantes F where A.FABRICANTE = F.CODIGO group by F.NOMBRE;

-- 1.14
select  avg(PRECIO) as PRECIO_MEDIO , F.NOMBRE from articulos A, fabricantes F where A.FABRICANTE = F.CODIGO group by F.NOMBRE 
having avg(PRECIO) >= 120;

-- 1.15
select NOMBRE, PRECIO from articulos order by PRECIO asc limit 1;
select NOMBRE, PRECIO from articulos where PRECIO = (select min(PRECIO) from articulos);

-- 1.16
select A.NOMBRE, A.PRECIO, F.NOMBRE from articulos A, fabricantes F where A.FABRICANTE = F.CODIGO 
and A.PRECIO = ( select MAX(A2.PRECIO)  from articulos A2  where A2.FABRICANTE = F.CODIGO );

-- 1.17
insert into articulos (NOMBRE, PRECIO ,FABRICANTE) values ('altavoces', 70, 2);

-- 1.18
update articulos set NOMBRE = 'impresora laser' where CODIGO = 8;

-- 1.19
select NOMBRE, (PRECIO - 10) as 'CON_10%_DE_DESCUENTO' from articulos;

-- 1.20
select NOMBRE, (PRECIO - 10) as 'CON_10€_DE_DESCUENTO' from articulos where PRECIO >= 120;
