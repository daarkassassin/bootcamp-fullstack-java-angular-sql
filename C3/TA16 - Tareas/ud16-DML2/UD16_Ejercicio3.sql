USE actividades;
INSERT INTO almacenes(LUGAR, CAPACIDAD) VALUES ('Madrid',1),('NewYork',6),('Andorra',2),('Paris',5),('Berlin',3),('te-st',1),('test-test-test',6),('test-st',2),('test-test',5),('test',3);
INSERT INTO cajas VALUES ('0MN7ab','test',185,6),('4H8Pab','test',255,6),('4RT3ab','test-test',195,7),('7G3Hab','test-test',205,7),('8JN6ab','test-test-test-test',71,8),('8Y6Uab','test-test-test-test',55,8),('9J6Fab','te st',100,9),('LL08ab','te st',145,9),('P0H6ab','te st - te st',115,10),('P2T6ab','te st - te st',125,10);
INSERT INTO almacenes (LUGAR, CAPACIDAD) VALUES ('Madrid',1),('NewYork',6),('Andorra',2),('Paris',5),('Berlin',3);
INSERT INTO cajas VALUES ('0MN7ab','Apple',180,6),('4H8Pab','Apple',250,6),('4RT3ab','Pin',190,7),('7G3Hab','Pin',200,7),('8JN6ab','PinApple',75,8),('8Y6Uab','PinApple',50,8),('9J6Fab','ApplePinApple',175,9),('LL08ab','ApplePinApple',140,9),('P0H6ab','ApplePinApplePin',125,10),('P2T6ab','ApplePinApplePin',150,10),('TU55ab','PinPinApple',90,5);
--


-- 3.1
select * from almacenes;

-- 3.2
select * from cajas where VALOR > 150;

-- 3.3
select CONTENIDO from cajas group by CONTENIDO;

-- 3.4
select avg(VALOR) from cajas;

-- 3.5
select  avg(VALOR) as Valor_Medio, ALMACEN from cajas group by ALMACEN;

-- 3.6
select CODIGO from almacenes where CODIGO IN 
(select ALMACEN  from cajas group by ALMACEN having avg(VALOR) > 150 );

-- 3.7
select C.NUMREFERENCIA, A.LUGAR from cajas C, almacenes A where C.ALMACEN = A.CODIGO;

-- 3.8
select ALMACEN, count(*) from cajas group by ALMACEN;

-- 3.9
select CODIGO from almacenes where CAPACIDAD < (select count(*) from cajas where ALMACEN = CODIGO);

-- 3.10
select C.NUMREFERENCIA from cajas C, almacenes A where A.LUGAR = 'Bilbao';

-- 3.11
insert into almacenes(LUGAR, CAPACIDAD) values ('Barcelona',3);

-- 3.12
insert into cajas values ('H5RT','Papel',200,2);

-- 3.13
update cajas set VALOR = VALOR * 0.85;

-- 3.14
update cajas SET VALOR = VALOR * 0.80 where VALOR > (select avg(VALOR) from cajas);
-- no funciona por que un mecanismo de la bd lo impide.
-- UPDATE cajas SET VALOR * 0.80 WHERE VALOR IN (SELECT VALOR FROM (SELECT avg(VALOR) FROM cajas) AS alias_tabla);


-- 3.15
delete from cajas where VALOR < 100;

-- 3.16
delete from cajas where ALMACEN in ( select CODIGO from almacenes where CAPACIDAD < ( select COUNT(*) from cajas where ALMACEN = CODIGO ) );
-- no funciona por que un mecanismo de la bd lo impide.


