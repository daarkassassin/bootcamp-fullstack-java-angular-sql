USE actividades;

insert into peliculas (NOMBRE, CALIFICACIONEDAD) values
('test1', 1),
('test10', 1),
('test9', 1),
('test8', 1),
('test7', 1),
('test6', 1),
('test5', 1),
('test4', 1),
('test3', 1),
('test2', 1);

insert into salas (NOMBRE, PELICULA) values 
('test1', 1),
('test10', 1),
('test9', 1),
('test8', 1),
('test7', 1),
('test6', 1),
('test5', 1),
('test4', 1),
('test3', 1),
('test2', 2);

-- 4.1
select NOMBRE from peliculas;

-- 4.2
select distinct CALIFICACIONEDAD from peliculas where  CALIFICACIONEDAD is not null;

-- 4.3
select NOMBRE, CALIFICACIONEDAD from peliculas where CALIFICACIONEDAD is null;

-- 4.4
select NOMBRE from salas where PELICULA is null;

-- 4.5
select * from salas left join peliculas on salas.PELICULA = peliculas.CODIGO;

-- 4.6
select * from peliculas left join salas on salas.PELICULA = peliculas.CODIGO;

-- 4.7
select peliculas.NOMBRE from salas right join peliculas on salas.PELICULA = peliculas.CODIGO where salas.PELICULA is null;

-- 4.8
insert into peliculas (NOMBRE, CALIFICACIONEDAD) values ('Uno, Dos, Tres', 7);
select * from peliculas;

-- 4.9
update peliculas set CALIFICACIONEDAD = 13 where CALIFICACIONEDAD is null;

-- 4.10
delete from salas where PELICULA in (select CODIGO from peliculas where CALIFICACIONEDAD like "G" and CALIFICACIONEDAD like "1");
select * from peliculas;
select * from salas;