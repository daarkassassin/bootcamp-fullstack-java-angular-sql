USE actividades;
insert into empleados (DNI, NOMBRE, APELLIDOS, DEPARTAMENTO) values(12346, 'test', 'test', 14),(21345, 'test', 'test', 14),(32145, 'test', 'test', 14),(43256, 'test', 'test', 14),(54316, 'test', 'test', 14),(65321, 'test', 'test', 14),(76432, 'test', 'test', 14),(11111, 'test', 'test', 14),(22222, 'test', 'test', 14),(33333, 'test', 'test', 14);
INSERT INTO departamentos VALUES (10,'IT - test',65000),(20,'Accounting - test',15000),(30,'Human Resources - test',240000),(40,'Research - test',55000),(51,'test - test',55000),(61,'test - test',55000),(71,'test - test',55000),(81,'test - test',55000),(91,'test - test',55000),(101,'test - test',55000);
INSERT INTO departamentos VALUES (1,'IT - test',65000),(2,'Accounting - test',15000),(3,'Human Resources - test',240000),(4,'Research - test',55000);
INSERT INTO empleados VALUES (21232877,'test','test',1),(21529485,'test','test',1),(22223683,'test','test',2),(23265417,'test','test',2),(23321419,'test','test',2),(23356943,'test','test',2),(25452478,'test','test',3),(26312482,'test','test',4),(26583219,'test','test',3),(27468214,'test','test',3),(28465733,'test','test',2),(28456245,'test','test',2),(28456746,'test','test',2),(28455266,'test','test',4);
--


-- 2.1
select APELLIDOS from empleados;

-- 2.2
select distinct APELLIDOS from empleados;

-- 2.3 
select * from empleados where APELLIDOS = 'López';

-- 2.4
select * from empleados where APELLIDOS = 'López' OR APELLIDOS = 'Pérez';

-- 2.5
select * from empleados where DEPARTAMENTO = 14;

-- 2.6
select * from empleados where DEPARTAMENTO = 37 OR DEPARTAMENTO = 77;

-- 2.7
select * from empleados where APELLIDOS like 'P%';

-- 2.8
select sum(PRESUPUESTO) from departamentos;

-- 2.9
select DEPARTAMENTO, count(*) as 'Total_Empleados' from empleados group by DEPARTAMENTO;

-- 2.10
select * from empleados E, departamentos D where E.DEPARTAMENTO = D.CODIGO;

-- 2.11
select E.NOMBRE, E.APELLIDOS, D.NOMBRE as 'Nombre_Departamento', D.PRESUPUESTO from empleados E, departamentos D where E.DEPARTAMENTO = D.CODIGO;

-- 2.12
select E.NOMBRE, E.APELLIDOS, D.NOMBRE as 'Nombre_Departamento', D.PRESUPUESTO from empleados E, departamentos D where E.DEPARTAMENTO = D.CODIGO and D.PRESUPUESTO > 60000;

-- 2.13
select * from departamentos where PRESUPUESTO > (select avg(PRESUPUESTO) from departamentos);

-- 2.14
select NOMBRE from departamentos where CODIGO in 
(select DEPARTAMENTO  from empleados group by DEPARTAMENTO having count(DEPARTAMENTO) > 2 );

-- 2.15
insert into departamentos values (666,'Calidad',40000);

-- 2.16
update departamentos set PRESUPUESTO = PRESUPUESTO *0.9;

-- 2.17
update empleados set DEPARTAMENTO = 14 where DEPARTAMENTO = 77;

-- 2.18
delete from empleados where DEPARTAMENTO = 14;
select * from empleados;

-- 2.19
delete from empleados where DEPARTAMENTO in
(select CODIGO  from departamentos where PRESUPUESTO >= 60000);
select * from empleados;

-- 2.20
delete from empleados;
select * from empleados;




