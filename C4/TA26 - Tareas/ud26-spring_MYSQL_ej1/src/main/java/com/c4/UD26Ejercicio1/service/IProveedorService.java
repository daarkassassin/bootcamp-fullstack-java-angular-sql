package com.c4.UD26Ejercicio1.service;

import java.util.List;

import com.c4.UD26Ejercicio1.dto.Proveedor;

public interface IProveedorService {

	public List<Proveedor> listarProveedor();
	
	public Proveedor guardarProveedor(Proveedor proveedor);
	
	public Proveedor proveedorXID(String id);
	
	public Proveedor actualizarProveedor(Proveedor proveedor);
	
	public void eliminarProveedor(String id);
	
}
