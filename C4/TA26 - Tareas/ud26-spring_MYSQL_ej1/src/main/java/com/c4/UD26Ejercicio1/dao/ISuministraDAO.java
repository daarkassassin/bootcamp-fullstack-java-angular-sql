package com.c4.UD26Ejercicio1.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD26Ejercicio1.dto.Suministra;

public interface ISuministraDAO extends JpaRepository<Suministra, Integer>{

}
