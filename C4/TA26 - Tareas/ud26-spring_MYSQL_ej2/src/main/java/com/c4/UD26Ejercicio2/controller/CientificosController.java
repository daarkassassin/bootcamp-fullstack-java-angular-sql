package com.c4.UD26Ejercicio2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD26Ejercicio2.dto.Cientificos;
import com.c4.UD26Ejercicio2.service.CientificosServiceImpl;

@RestController
@RequestMapping("/api")
public class CientificosController {
	
	@Autowired
	CientificosServiceImpl cientificosServiceImpl;
	
	@GetMapping("/cientificos")
	public List<Cientificos> listarCientificos(){
		return cientificosServiceImpl.listarCientificos();
	}
	
	@PostMapping("/cientificos")
	public Cientificos salvarCientificos(@RequestBody Cientificos cientifico) {
		return cientificosServiceImpl.guardarCientifico(cientifico);
	}
		
	@GetMapping("/cientificos/{id}")
	public Cientificos cientificosXID(@PathVariable(name="id") String id) {
		
		Cientificos cientificos_xid= new Cientificos();
		
		cientificos_xid = cientificosServiceImpl.cientificoXID(id);
		
		System.out.println("Cientificos XID: "+cientificos_xid);
		
		return cientificos_xid;
	}
	
	@PutMapping("/cientificos/{id}")
	public Cientificos actualizarCientificos(@PathVariable(name="id")String id,@RequestBody Cientificos cientifico) {
		
		Cientificos cientificos_seleccionado= new Cientificos();
		Cientificos cientificos_actualizado= new Cientificos();
		
		cientificos_seleccionado= cientificosServiceImpl.cientificoXID(id);
		
		cientificos_seleccionado.setNombre(cientifico.getNombre());
		cientificos_seleccionado.setDni(cientifico.getDni());
		
		cientificos_actualizado = cientificosServiceImpl.actualizarCientifico(cientificos_seleccionado);
		
		System.out.println("El Cientifico actualizado es: "+ cientificos_actualizado);
		
		return cientificos_actualizado;
	}
	
	@DeleteMapping("/cientificos/{id}")
	public void eleiminarCientificos(@PathVariable(name="id")String id) {
		cientificosServiceImpl.eliminarCientifico(id);
	}
	
}
