package com.c4.UD26Ejercicio2.service;

import java.util.List;

import com.c4.UD26Ejercicio2.dto.Asignado_A;


public interface IAsignado_AService {
	
	public List<Asignado_A> listarAsignado_A();
	
	public Asignado_A guardarAsignado_A(Asignado_A asignado_A);
	
	public Asignado_A asignado_AXID(Long id);
	
	public Asignado_A actualizarAsignado_A(Asignado_A asignado_A);
	
	public void eliminarAsignado_A(Long id);

}
