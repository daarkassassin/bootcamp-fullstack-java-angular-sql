package com.c4.UD26Ejercicio2.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="cientificos")
public class Cientificos {
	
	@Id
	@Column(name = "dni")
	private String dni;
	@Column(name = "nom_apels")
	private String nombre;
	
    @OneToMany
    @JoinColumn(name="dni")
    private List<Asignado_A> Asignado_A;
	
    //Constructores
	public Cientificos() {
		
	}

	public Cientificos(String dni, String nombre) {
		this.dni = dni;
		this.nombre = nombre;
	}
	
	//Metodos SET y GET
	
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @param asignado_A the asignado_A to set
	 */
	public void setAsignado_A(List<Asignado_A> asignado_A) {
		Asignado_A = asignado_A;
	}
	
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "asignado")
	public List<Asignado_A> getAsignado_A() {
		return Asignado_A;
	}
	
}
