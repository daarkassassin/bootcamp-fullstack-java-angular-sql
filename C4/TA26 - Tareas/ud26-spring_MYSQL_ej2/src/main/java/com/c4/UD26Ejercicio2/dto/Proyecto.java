package com.c4.UD26Ejercicio2.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="proyecto")
public class Proyecto {
	
	@Id
	@Column(name = "id_proyecto")
	private String id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "horas")
	private int horas;
	
    @OneToMany
    @JoinColumn(name="id_proyecto")
    private List<Asignado_A> Asignado_A;
	
    //Constructores
	public Proyecto() {
		
	}

	public Proyecto(String id, String nombre, int horas) {
		this.id = id;
		this.nombre = nombre;
		this.horas = horas;
	}
	
	//Metodos SET y GET

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the horas
	 */
	public int getHoras() {
		return horas;
	}

	/**
	 * @param horas the horas to set
	 */
	public void setHoras(int horas) {
		this.horas = horas;
	}

	/**
	 * @param asignado_A the asignado_A to set
	 */
	public void setAsignado_A(List<Asignado_A> asignado_A) {
		Asignado_A = asignado_A;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "asignado")
	public List<Asignado_A> getAsignado_A() {
		return Asignado_A;
	}
	
	
}
