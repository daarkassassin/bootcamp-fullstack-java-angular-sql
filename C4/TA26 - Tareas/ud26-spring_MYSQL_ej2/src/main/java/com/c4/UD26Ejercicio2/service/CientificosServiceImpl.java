package com.c4.UD26Ejercicio2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD26Ejercicio2.dao.ICientificosDAO;
import com.c4.UD26Ejercicio2.dto.Cientificos;

@Service
public class CientificosServiceImpl implements ICientificosService{
	
	@Autowired
	ICientificosDAO iCientificoDAO;

	@Override
	public List<Cientificos> listarCientificos() {
		return iCientificoDAO.findAll();
	}

	@Override
	public Cientificos guardarCientifico(Cientificos cientifico) {
		return iCientificoDAO.save(cientifico);
	}

	@Override
	public Cientificos cientificoXID(String id) {
		return iCientificoDAO.findById(id).get();
	}

	@Override
	public Cientificos actualizarCientifico(Cientificos cientifico) {
		return iCientificoDAO.save(cientifico);
	}

	@Override
	public void eliminarCientifico(String id) {
		iCientificoDAO.deleteById(id);
	}

}
