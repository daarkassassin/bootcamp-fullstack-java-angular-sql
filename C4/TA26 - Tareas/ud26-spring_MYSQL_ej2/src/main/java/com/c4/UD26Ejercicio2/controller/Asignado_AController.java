package com.c4.UD26Ejercicio2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD26Ejercicio2.dto.Asignado_A;
import com.c4.UD26Ejercicio2.service.Asignado_AServiceImpl;

@RestController
@RequestMapping("/api")
public class Asignado_AController {
	
	@Autowired
	Asignado_AServiceImpl asignado_AServiceImpl;
	
	@GetMapping("/asignado_A")
	public List<Asignado_A> listarAsignado_A(){
		return asignado_AServiceImpl.listarAsignado_A();
	}
	
	@PostMapping("/asignado_A")
	public Asignado_A salvarAsignado_A(@RequestBody Asignado_A asignado_A) {
		return asignado_AServiceImpl.guardarAsignado_A(asignado_A);
	}
		
	@GetMapping("/asignado_A/{id}")
	public Asignado_A asignado_AXID(@PathVariable(name="id") Long id) {
		
		Asignado_A asignado_A_xid= new Asignado_A();
		
		asignado_A_xid = asignado_AServiceImpl.asignado_AXID(id);
		
		System.out.println("Asignado_A XID: "+asignado_A_xid);
		
		return asignado_A_xid;
	}
	
	@DeleteMapping("/asignado_A/{id}")
	public void eleiminarAsignado_A(@PathVariable(name="id")Long id) {
		asignado_AServiceImpl.eliminarAsignado_A(id);
	}
	
}
