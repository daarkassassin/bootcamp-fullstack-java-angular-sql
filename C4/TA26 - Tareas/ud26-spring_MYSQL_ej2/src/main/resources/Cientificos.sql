USE Cientificos;
DROP TABLE IF EXISTS `asignado`;
DROP TABLE IF EXISTS `cientificos`;
DROP TABLE IF EXISTS `proyecto`;

--
-- Table structure for table `proyecto`
--
CREATE TABLE `proyecto` (
  `id_proyecto` char(4) NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `horas` int DEFAULT NULL,
  PRIMARY KEY (`id_proyecto`)
);

--
-- Dumping data for table `Proyecto`
--
INSERT INTO `proyecto` VALUES ('012','proyecto 10',1000),('013','proyecto 11',1100),('123','proyecto 1',100),('234','proyecto 2',200),('345','proyecto 3',300),('456','proyecto 4',400),('567','proyecto 5',500),('678','proyecto 6',600),('789','proyecto 7',700),('890','proyecto 8',800),('901','proyecto 9',900);


--
-- Table structure for table `cientificos`
--
CREATE TABLE `cientificos` (
  `dni` varchar(8) NOT NULL,
  `nom_apels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Cientificos`
--
INSERT INTO `cientificos` VALUES ('1234567A','cient 1'),('1234567B','cient 2'),('1234567C','cient 3'),('1234567D','cient 4'),('1234567E','cient 5'),('1234567F','cient 6'),('1234567J','cient 7'),('1234567K','cient 8'),('1234567L','cient 9'),('1234567M','cient 10'),('1234567N','cient 11');



--
-- Table structure for table `Asignado_A`
--
CREATE TABLE `asignado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(8) NOT NULL,
  `id_proyecto` char(4) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `asignado_ibfk_1` FOREIGN KEY (`dni`) REFERENCES `cientificos` (`dni`),
  CONSTRAINT `asignado_ibfk_2` FOREIGN KEY (`id_proyecto`) REFERENCES `proyecto` (`id_proyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `asignado`
--
INSERT INTO `asignado`(dni, id_proyecto) VALUES ('1234567M','012'),('1234567N','013'),('1234567A','123'),('1234567B','234'),('1234567C','345'),('1234567D','456'),('1234567E','567'),('1234567F','678'),('1234567J','789'),('1234567K','890'),('1234567L','901');
