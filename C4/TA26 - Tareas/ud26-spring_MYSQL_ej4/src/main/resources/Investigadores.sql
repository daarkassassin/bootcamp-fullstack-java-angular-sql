Use Investigadores;

DROP TABLE IF EXISTS `reserva`;
DROP TABLE IF EXISTS `equipos`;
DROP TABLE IF EXISTS `investigadores`;
DROP TABLE IF EXISTS `facultad`;

--
-- Table structure for table `Facultad`
--
CREATE TABLE `facultad` (
  `id_facultad` int NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_facultad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Facultad`
--
INSERT INTO `facultad` VALUES (1,'test'),(10,'test'),(20,'test'),(30,'test'),(40,'test'),(50,'test'),(60,'test'),(70,'test'),(80,'test'),(90,'test');

--
-- Table structure for table `Investigadores`
--
CREATE TABLE `investigadores` (
  `dni` varchar(8) NOT NULL,
  `nom_apels` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_facultad` int NOT NULL,
  PRIMARY KEY (`dni`),
  CONSTRAINT `Investigadores_ibfk_1` FOREIGN KEY (`id_facultad`) REFERENCES `facultad` (`id_facultad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Investigadores`
--
INSERT INTO `investigadores` VALUES ('1234567A','Investigadores1',10),('1234567B','Investigadores2',20),('1234567C','Investigadores3',30),('1234567D','Investigadores4',40),('1234567E','Investigadores5',50),('1234567F','Investigadores6',60),('1234567J','Investigadores7',70),('1234567K','Investigadores8',80),('1234567L','Investigadores9',90),('1234567M','Investigadores0',1);

--
-- Table structure for table `Equipos`
--
CREATE TABLE `equipos` (
  `num_equipo` char(4) NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_facultad` int NOT NULL,
  PRIMARY KEY (`num_equipo`),
  CONSTRAINT `Equipos_ibfk_1` FOREIGN KEY (`id_facultad`) REFERENCES `facultad` (`id_facultad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Equipos`
--
INSERT INTO `equipos` VALUES ('1111','proyecto 1',10),('1112','proyecto 2',20),('1113','proyecto 3',30),('1114','proyecto 4',40),('1115','proyecto 5',50),('1116','proyecto 6',60),('1117','proyecto 7',70),('1118','proyecto 8',80),('1119','proyecto 9',90),('1121','proyecto 10',1);

--
-- Table structure for table `Reserva`
--
CREATE TABLE `reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(8) NOT NULL,
  `num_equipo` char(4) NOT NULL,
  `comienzo` datetime DEFAULT NULL,
  `fin` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `Reserva_ibfk_1` FOREIGN KEY (`dni`) REFERENCES `investigadores` (`dni`),
  CONSTRAINT `Reserva_ibfk_2` FOREIGN KEY (`num_equipo`) REFERENCES `equipos` (`num_equipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Reserva`
--
INSERT INTO `reserva`(dni, num_equipo, comienzo, fin) VALUES ('1234567A','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567B','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567C','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567D','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567E','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567F','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567J','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567K','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567L','1111','2012-06-18 00:00:00','2012-06-18 00:00:00'),('1234567M','1111','2012-06-18 00:00:00','2012-06-18 00:00:00');


