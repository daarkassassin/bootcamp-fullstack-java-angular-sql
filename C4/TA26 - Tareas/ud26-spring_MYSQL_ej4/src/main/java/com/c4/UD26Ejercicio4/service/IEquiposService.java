package com.c4.UD26Ejercicio4.service;

import java.util.List;

import com.c4.UD26Ejercicio4.dto.Equipos;


public interface IEquiposService {
	
	public List<Equipos> listarEquipos();
	
	public Equipos guardarEquipos(Equipos equipos);
	
	public Equipos equiposXID(String id);
	
	public Equipos actualizarEquipos(Equipos equipos);
	
	public void eliminarEquipos(String id);

}
