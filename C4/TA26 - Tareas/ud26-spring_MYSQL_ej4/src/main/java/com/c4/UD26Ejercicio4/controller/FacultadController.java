package com.c4.UD26Ejercicio4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD26Ejercicio4.dto.Facultad;
import com.c4.UD26Ejercicio4.service.FacultadServiceImpl;

@RestController
@RequestMapping("/api")
public class FacultadController {
	
	@Autowired
	FacultadServiceImpl facultadServiceImpl;
	
	@GetMapping("/facultad")
	public List<Facultad> listarfacultad(){
		return facultadServiceImpl.listarFacultad();
	}
	
	@PostMapping("/facultad")
	public Facultad salvarfacultad(@RequestBody Facultad facultad) {
		return facultadServiceImpl.guardarFacultad(facultad);
	}
		
	@GetMapping("/facultad/{id}")
	public Facultad facultadXID(@PathVariable(name="id") Long id) {
		
		Facultad facultad_xid= new Facultad();
		
		facultad_xid = facultadServiceImpl.facultadXID(id);
		
		System.out.println("Facultad XID: "+facultad_xid);
		
		return facultad_xid;
	}
	
	@PutMapping("/facultad/{id}")
	public Facultad actualizarfacultad(@PathVariable(name="id")Long id,@RequestBody Facultad facultad) {
		
		Facultad facultad_seleccionado= new Facultad();
		Facultad facultad_actualizado= new Facultad();
		
		facultad_seleccionado= facultadServiceImpl.facultadXID(id);
		
		facultad_seleccionado.setId(facultad.getId());
		facultad_seleccionado.setNombre(facultad.getNombre());
		
		facultad_actualizado = facultadServiceImpl.actualizarFacultad(facultad_seleccionado);
		
		System.out.println("La Facultad actualizada es: "+ facultad_actualizado);
		
		return facultad_actualizado;
	}
	
	@DeleteMapping("/facultad/{id}")
	public void eleiminarfacultad(@PathVariable(name="id")Long id) {
		facultadServiceImpl.eliminarFacultad(id);
	}
	
}
