package com.c4.UD26Ejercicio4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud26SpringMysqlEj4Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud26SpringMysqlEj4Application.class, args);
	}

}
