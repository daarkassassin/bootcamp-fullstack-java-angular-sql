package com.c4.UD26Ejercicio4.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="investigadores")
public class Investigadores {
	
	@Id
	@Column(name = "dni")
	private String dni;
	@Column(name = "nom_apels")
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name = "id_facultad")
	Facultad facultad;
	
    @OneToMany
    @JoinColumn(name="dni")
    private List<Reserva> Reserva;
	
    //Constructores
	public Investigadores() {
		
	}

	public Investigadores(String dni, String nombre, Facultad facultad) {
		this.dni = dni;
		this.nombre = nombre;
		this.facultad = facultad;
	}
	
	//Metodos SET y GET
	
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @param reserva the asignado_A to set
	 */
	public void setReserva(List<Reserva> reserva) {
		Reserva = reserva;
	}
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "asignado")
	public List<Reserva> getReserva() {
		return Reserva;
	}

	/**
	 * @return the facultad
	 */
	public Facultad getFacultad() {
		return facultad;
	}

	/**
	 * @param facultad the facultad to set
	 */
	public void setFacultad(Facultad facultad) {
		this.facultad = facultad;
	}
	
	
	
	
}
