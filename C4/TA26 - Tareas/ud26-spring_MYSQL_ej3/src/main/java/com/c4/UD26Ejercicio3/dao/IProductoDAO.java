package com.c4.UD26Ejercicio3.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD26Ejercicio3.dto.Producto;

public interface IProductoDAO extends JpaRepository<Producto,Integer>{

}
