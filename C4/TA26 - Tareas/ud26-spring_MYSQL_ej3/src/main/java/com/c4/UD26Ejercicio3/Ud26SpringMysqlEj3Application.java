package com.c4.UD26Ejercicio3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud26SpringMysqlEj3Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud26SpringMysqlEj3Application.class, args);
	}

}
