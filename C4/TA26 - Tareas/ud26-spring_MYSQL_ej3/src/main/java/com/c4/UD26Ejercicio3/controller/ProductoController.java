package com.c4.UD26Ejercicio3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD26Ejercicio3.dto.Producto;
import com.c4.UD26Ejercicio3.service.ProductoServiceImpl;

@RestController
@RequestMapping("/api")
public class ProductoController {

	@Autowired
	ProductoServiceImpl productoServiceImpl;
	
	@GetMapping("/productos")
	public List<Producto> listarProducto(){
		return productoServiceImpl.listarProducto();
	}
	
	@PostMapping("/producto")
	public Producto salvarProducto(@RequestBody Producto producto) {
		return productoServiceImpl.guardarProducto(producto);
	}
	
	@GetMapping("/producto/{id}")
	public Producto productoXID(@PathVariable(name="id") int id) {
		
		Producto producto_xid= new Producto();
		
		producto_xid = productoServiceImpl.productoXID(id);
		
		System.out.println("Producto XID: "+producto_xid);
		
		return producto_xid;
	}
	
	@PutMapping("/producto/{id}")
	public Producto actualizarProducto(@PathVariable(name="id")int id,@RequestBody Producto producto) {
		
		Producto producto_seleccionado= new Producto();
		Producto producto_actualizado= new Producto();
		
		producto_seleccionado= productoServiceImpl.productoXID(id);
		
		producto_seleccionado.setNombre(producto.getNombre());
		producto_seleccionado.setPrecio(producto.getPrecio());
		
		producto_actualizado = productoServiceImpl.actualizarProducto(producto_seleccionado);
		
		System.out.println("La Producto actualizada es: "+ producto_actualizado);
		
		return producto_actualizado;
	}
	
	@DeleteMapping("/producto/{id}")
	public void eleiminarProducto(@PathVariable(name="id")int id) {
		productoServiceImpl.eliminarProducto(id);
	}
	
}
