package com.c4.UD26Ejercicio3.service;

import java.util.List;

import com.c4.UD26Ejercicio3.dto.Maquina;

public interface IMaquinaService {

	public List<Maquina> listarMaquina();
	
	public Maquina guardarMaquina(Maquina maquina);
	
	public Maquina maquinaXID(int id);
	
	public Maquina actualizarMaquina(Maquina maquina);
	
	public void eliminarMaquina(int id);
	
}
