package JUnit.Junit09_Geometria.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GeometriaTest {
	
	Geometria gm;
	private static final double DELTA = 1e-15;
	
	@BeforeEach
	public void before() {
		System.out.println("before");
		gm = new Geometria(9);
	}
	
	@AfterEach
	public void after() {
		System.out.println("after");
		gm = null;
	}

	@Test
	void testAreaCuadrado() {
		int resultado = gm.areacuadrado(2);
		int esperado = 4;
		System.out.println("Area Cuadrado: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaCirculo() {
		double resultado = gm.areaCirculo(2);
		double esperado = 12.5664;
		System.out.println("Area Circulo: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado, DELTA);
	}
	
	@Test
	void testAreaTriangulo() {
		int resultado = gm.areatriangulo(20, 2);
		int esperado = 20;
		System.out.println("Area Triangulo: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaRectangulo() {
		int resultado = gm.arearectangulo(20, 2);
		int esperado = 40;
		System.out.println("Area Rectangulo: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaPentagono() {
		int resultado = gm.areapentagono(20, 2);
		int esperado = 20;
		System.out.println("Area Pentagono: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaRombo() {
		int resultado = gm.arearombo(20, 2);
		int esperado = 20;
		System.out.println("Area Rombo: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaRomboide() {
		int resultado = gm.arearomboide(20, 2);
		int esperado = 40;
		System.out.println("Area Rombo: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaTrapecio() {
		int resultado = gm.areatrapecio(20, 2, 4);
		int esperado = 44;
		System.out.println("Area Rombo: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testGeometria() {
		Geometria gmtest = new Geometria(5);
		String resultado = gmtest.figura(gmtest.getId());
		String esperado = "Pentagono";
		System.out.println("Geometria: Esperado["+esperado+"], Resultado["+resultado+"]");
		assertEquals(esperado, resultado);
	}
	
	
	
	
}
