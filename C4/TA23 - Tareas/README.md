![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD23/M6 – SPRING / POSTMAN

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| David Bonet Daga | Master | Team Member | 08/03/2021 |   |   |  |
| Xavier Bonet Daga |  | Team Member | 08/03/2021 |   |   |  |


#### 2. Description
```
Ejercicios UD23/M6 – SPRING / POSTMAN
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql

```
UD23/M6 – SPRING / POSTMAN  / https://gitlab.com/daarkassassin/bootcamp-fullstack-java-angular-sql
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8 - https://www.oracle.com/es/java/technologies/javase/javase-jdk8-downloads.html
Apache Maven - http://maven.apache.org/download.cgi
IDE - Spring Tool Suite 4 - https://spring.io/tools
Postman - https://www.postman.com/downloads/
```
