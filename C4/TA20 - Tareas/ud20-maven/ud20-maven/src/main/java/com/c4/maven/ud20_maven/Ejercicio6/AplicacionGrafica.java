package com.c4.maven.ud20_maven.Ejercicio6;


import java.awt.event.*;
import javax.swing.*;

public class AplicacionGrafica extends JFrame{

	// Panel de la aplicacion
	private JPanel contentPane;
	
	public AplicacionGrafica() {
		
		// CONTENEDOR
		
		// Titulo de la ventana
		setTitle("Ejercicio 6");
		// Tama�o ventana (x, y, longitud, altura)
		setBounds(600, 300, 425, 150);
		// Cerrar la app al cerrar la ventana
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		

		// PANEL
		
		// Crear el panel
		contentPane = new JPanel();
		// Indicar el dise�o del panel
		contentPane.setLayout(null);
		// Asignar el panel a la ventana
		setContentPane(contentPane);
		
		
		// COMPONENTES
		
		// Crear componente JLabel
		JLabel jL1 = new JLabel("Altura (metros)");
		// Colocar el componente (x, y, longitud, altura)
		jL1.setBounds(25,25,150,20);
		// A�adir el componente al panel
		contentPane.add(jL1);
		
		// Crear componente JTextField
		JTextField jTF1 = new JTextField("");
		// Colocar el componente (x, y, longitud, altura)
		jTF1.setBounds(125,25,75,20);
		// A�adir el componente al panel
		contentPane.add(jTF1);
		
		
		// Crear componente JLabel
		JLabel jL2 = new JLabel("Peso (kg)");
		// Colocar el componente (x, y, longitud, altura)
		jL2.setBounds(225,25,150,20);
		// A�adir el componente al panel
		contentPane.add(jL2);
		
		// Crear componente JTextField
		JTextField jTF2 = new JTextField("");
		// Colocar el componente (x, y, longitud, altura)
		jTF2.setBounds(300,25,75,20);
		// A�adir el componente al panel
		contentPane.add(jTF2);
		
		
		// Crear componente JButton
		JButton btn1 = new JButton("Calcular IMC");
		// Colocar el componente (x, y, longitud, altura)
		btn1.setBounds(25,50,125,20);
		// A�adir el componente al panel
		contentPane.add(btn1);
		
		// Crear componente JLabel
		JLabel jL3 = new JLabel("IMC");
		// Colocar el componente (x, y, longitud, altura)
		jL3.setBounds(175,50,200,20);
		// A�adir el componente al panel
		contentPane.add(jL3);
		
		// Crear componente JTextField
		JTextField jTF3 = new JTextField("");
		// Colocar el componente (x, y, longitud, altura)
		jTF3.setBounds(225,50,150,20);
		// A�adir el componente al panel
		contentPane.add(jTF3);
		
		
		// EVENTOS
		
		// Evento al pulsar el boton 1
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {

				double peso = Double.parseDouble(jTF2.getText());
				double altura = Double.parseDouble(jTF1.getText());
				double imc = peso / Math.pow(altura, 2);

				jTF3.setText(Double.toString(imc));
				
			}
		});

		
		// Hacer la ventana visible
		setVisible(true);
		
		
		
	}
	
}
