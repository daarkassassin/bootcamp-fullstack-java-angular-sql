package com.c4.UD25Ejercicio3.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Cajas") // en caso que la tabala sea diferente
public class Cajas {

	// Atributos de entidad Fabricante
	@Id
	@Column(name = "numReferencia")
	private String numReferencia;
	@Column(name = "contenido") 
	private String contenido;
	@Column(name = "valor") 
	private int valor;
	
	@ManyToOne
	@JoinColumn(name="idAlmacen")
	private Almacenes Almacenes;

	// Constructores
	public Cajas() {

	}

	/**
	 * @param numReferencia
	 * @param contenido
	 * @param valor
	 * @param Almacenes
	 */
	public Cajas(String numReferencia, String contenido, int valor, Almacenes Almacenes) {
		// super();
		this.numReferencia = numReferencia;
		this.contenido = contenido;
		this.valor = valor;
		this.Almacenes = Almacenes;
	}
	
	//Seters & Geters

	/**
	 * @return the numReferencia
	 */
	public String getNumReferencia() {
		return numReferencia;
	}

	/**
	 * @param numReferencia the numReferencia to set
	 */
	public void setNumReferencia(String numReferencia) {
		this.numReferencia = numReferencia;
	}

	/**
	 * @return the contenido
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * @param contenido the contenido to set
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	/**
	 * @return the valor
	 */
	public int getValor() {
		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(int valor) {
		this.valor = valor;
	}

	/**
	 * @return the almacenes
	 */
	public Almacenes getAlmacenes() {
		return Almacenes;
	}

	/**
	 * @param almacenes the almacenes to set
	 */
	public void setAlmacenes(Almacenes almacenes) {
		Almacenes = almacenes;
	}

	//ToString
	@Override
	public String toString() {
		return "Cajas [numReferencia=" + numReferencia + ", contenido=" + contenido + ", valor=" + valor
				+ ", Almacenes=" + Almacenes + "]";
	}

}
