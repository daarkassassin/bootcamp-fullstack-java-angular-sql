package com.c4.UD25Ejercicio3.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.c4.UD25Ejercicio3.dto.Almacenes;

public interface IAlmacenesDAO extends JpaRepository<Almacenes, Long> {

}
