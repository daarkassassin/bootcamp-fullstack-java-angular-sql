package com.c4.UD25Ejercicio3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD25Ejercicio3.dao.ICajasDAO;
import com.c4.UD25Ejercicio3.dto.Cajas;



@Service
public class CajasServiceImpl implements ICajasService{
	
	@Autowired
	ICajasDAO iCajasDAO;

	@Override
	public List<Cajas> listarCajas() {
		// TODO Auto-generated method stub
		return iCajasDAO.findAll();
	}

	@Override
	public Cajas guardarCaja(Cajas Caja) {
		return iCajasDAO.save(Caja);
	}

	@Override
	public Cajas cajaXRef(String numReferencia) {
		// TODO Auto-generated method stub
		return iCajasDAO.findById(numReferencia).get();
	}

	@Override
	public Cajas actualizarCaja(Cajas Caja) {
		return iCajasDAO.save(Caja);
	}

	@Override
	public void eliminarCaja(String numReferencia) {
		iCajasDAO.deleteById(numReferencia);
		
	}
	
	

}


