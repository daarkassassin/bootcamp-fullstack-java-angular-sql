package com.c4.UD25Ejercicio3.service;

import java.util.List;
import com.c4.UD25Ejercicio3.dto.Cajas;



public interface ICajasService {

	//Metodos del CRUD
	public List<Cajas> listarCajas(); //Listar All 
	
	public Cajas guardarCaja(Cajas Caja);	//Guarda una Caja CREATE
	
	public Cajas cajaXRef(String numReferencia); //Leer datos de una Caja READ
	
	public Cajas actualizarCaja(Cajas Caja); //Actualiza datos de la Caja UPDATE
	
	public void eliminarCaja(String numReferencia);// Elimina la Caja DELETE
	
	
}
