package com.c4.UD25Ejercicio4.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.c4.UD25Ejercicio4.dto.Sala;

public interface ISalaDAO extends JpaRepository<Sala, Integer>{

}
