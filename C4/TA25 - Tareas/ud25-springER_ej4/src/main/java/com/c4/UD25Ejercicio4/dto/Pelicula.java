package com.c4.UD25Ejercicio4.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Peliculas")
public class Pelicula {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo_Pelicula")
	private int id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "calificacion_edad")
	private String calificacionEdad;
	
    @OneToMany
    @JoinColumn(name="codigo_sala")
    private List<Sala> sala;
	
	public Pelicula() {
		
	}

	public Pelicula(int id, String nombre, String calificacionEdad) {
		this.id = id;
		this.nombre = nombre;
		this.calificacionEdad = calificacionEdad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCalificacionEdad() {
		return calificacionEdad;
	}

	public void setCalificacionEdad(String calificacionEdad) {
		this.calificacionEdad = calificacionEdad;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Salas")
	public List<Sala> getSala() {
		return sala;
	}

	public void setSala(List<Sala> sala) {
		this.sala = sala;
	}
	
	
}
