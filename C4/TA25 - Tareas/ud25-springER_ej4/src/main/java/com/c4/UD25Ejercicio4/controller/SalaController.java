package com.c4.UD25Ejercicio4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD25Ejercicio4.dto.Sala;
import com.c4.UD25Ejercicio4.service.SalaServiceImpl;

@RestController
@RequestMapping("/api")
public class SalaController {

	@Autowired
	SalaServiceImpl salaServiceImpl;
	
	@GetMapping("/salas")
	public List<Sala> listarSala(){
		return salaServiceImpl.listarSala();
	}
	
	@PostMapping("/sala")
	public Sala salvarSala(@RequestBody Sala sala) {
		return salaServiceImpl.guardarSala(sala);
	}
		
	@GetMapping("/sala/{id}")
	public Sala salaXID(@PathVariable(name="id") int id) {
		
		Sala sala_xid= new Sala();
		
		sala_xid = salaServiceImpl.salaXID(id);
		
		System.out.println("Sala XID: "+sala_xid);
		
		return sala_xid;
	}
	
	@PutMapping("/sala/{id}")
	public Sala actualizarSala(@PathVariable(name="id")int id,@RequestBody Sala sala) {
		
		Sala sala_seleccionado= new Sala();
		Sala sala_actualizado= new Sala();
		
		sala_seleccionado= salaServiceImpl.salaXID(id);
		
		sala_seleccionado.setNombre(sala.getNombre());
		sala_seleccionado.setPelicula(sala.getPelicula());
		
		sala_actualizado = salaServiceImpl.actualizarSala(sala_seleccionado);
		
		System.out.println("La sala actualizada es: "+ sala_actualizado);
		
		return sala_actualizado;
	}
	
	@DeleteMapping("/sala/{id}")
	public void eleiminarCliente(@PathVariable(name="id")int id) {
		salaServiceImpl.eliminarSala(id);
	}
	
	
	
}
