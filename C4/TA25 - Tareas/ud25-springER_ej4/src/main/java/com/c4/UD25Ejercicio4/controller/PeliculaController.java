package com.c4.UD25Ejercicio4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD25Ejercicio4.dto.Pelicula;
import com.c4.UD25Ejercicio4.service.PeliculaServiceImpl;

@RestController
@RequestMapping("/api")
public class PeliculaController {
	
	@Autowired
	PeliculaServiceImpl peliculaServiceImpl;
	
	@GetMapping("/peliculas")
	public List<Pelicula> listarPelicula(){
		return peliculaServiceImpl.listarPelicula();
	}
	
	@PostMapping("/pelicula")
	public Pelicula salvarPelicula(@RequestBody Pelicula pelicula) {
		return peliculaServiceImpl.guardarPelicula(pelicula);
	}
		
	@GetMapping("/pelicula/{id}")
	public Pelicula peliculaXID(@PathVariable(name="id") int id) {
		
		Pelicula pelicula_xid= new Pelicula();
		
		pelicula_xid = peliculaServiceImpl.peliculaXID(id);
		
		System.out.println("Pelicula XID: "+pelicula_xid);
		
		return pelicula_xid;
	}
	
	@PutMapping("/pelicula/{id}")
	public Pelicula actualizarPelicula(@PathVariable(name="id")int id,@RequestBody Pelicula pelicula) {
		
		Pelicula pelicula_seleccionado= new Pelicula();
		Pelicula pelicula_actualizado= new Pelicula();
		
		pelicula_seleccionado= peliculaServiceImpl.peliculaXID(id);
		
		pelicula_seleccionado.setNombre(pelicula.getNombre());
		pelicula_seleccionado.setCalificacionEdad(pelicula.getCalificacionEdad());
		
		pelicula_actualizado = peliculaServiceImpl.actualizarPelicula(pelicula_seleccionado);
		
		System.out.println("La Pelicula actualizada es: "+ pelicula_actualizado);
		
		return pelicula_actualizado;
	}
	
	@DeleteMapping("/pelicula/{id}")
	public void eleiminarPelicula(@PathVariable(name="id")int id) {
		peliculaServiceImpl.eliminarPelicula(id);
	}
	
}
