package com.c4.UD25Ejercicio1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.c4.UD25Ejercicio1.dto.Articulos;


public interface IArticulosDAO extends JpaRepository<Articulos, Long> {
	
	// Listar Empleados por campo trabajo
	//public List<Articulos> findByFabricante(int idFabricante);

}
