package com.c4.UD25Ejercicio1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD25Ejercicio1.dto.Articulos;
import com.c4.UD25Ejercicio1.service.ArticulosServiceImpl;


@RestController
@RequestMapping("/api")
public class ArticulosController {

	@Autowired
	ArticulosServiceImpl articulosServideImpl;

	@GetMapping("/articulos")
	public List<Articulos> listarArticulos() {
		return articulosServideImpl.listarArticulos();
	}

	@PostMapping("/articulos")
	public Articulos salvarArticulo(@RequestBody Articulos articulo) {

		return articulosServideImpl.guardarArticulo(articulo);
	}

	@GetMapping("/articulos/{id}")
	public Articulos articuloXID(@PathVariable(name = "id") Long id) {

		Articulos articulo_xid = new Articulos();

		articulo_xid = articulosServideImpl.articuloXID(id);

		System.out.println("articulo XID: " + articulo_xid);

		return articulo_xid;
	}

	@PutMapping("/articulos/{id}")
	public Articulos actualizarArticulo(@PathVariable(name = "id") Long id, @RequestBody Articulos articulo) {

		Articulos articulo_seleccionado = new Articulos();
		Articulos articulo_actualizado = new Articulos();

		articulo_seleccionado = articulosServideImpl.articuloXID(id);

		articulo_seleccionado.setNombre(articulo.getNombre());
		articulo_seleccionado.setPrecio(articulo.getPrecio());
		articulo_seleccionado.setFabricantes(articulo.getFabricantes());

		articulo_actualizado = articulosServideImpl.actualizarArticulo(articulo_seleccionado);

		System.out.println("El articulo actualizado es: " + articulo_actualizado);

		return articulo_actualizado;
	}

	@DeleteMapping("/articulos/{id}")
	public void eliminarArticulo(@PathVariable(name = "id") Long id) {
		articulosServideImpl.eliminarArticulo(id);;
	}

}
