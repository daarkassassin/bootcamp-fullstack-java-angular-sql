package com.c4.UD25Ejercicio1.service;

import java.util.List;
import com.c4.UD25Ejercicio1.dto.Articulos;



public interface IArticulosService {

	//Metodos del CRUD
	public List<Articulos> listarArticulos(); //Listar All 
	
	public Articulos guardarArticulo(Articulos articulos);	//Guarda un Articulo CREATE
	
	public Articulos articuloXID(Long id); //Leer datos de un Articulo READ
	
	public Articulos actualizarArticulo(Articulos articulos); //Actualiza datos del Articulo UPDATE
	
	public void eliminarArticulo(Long id);// Elimina el Articulo DELETE
	
	
}
