package com.c4.UD25Ejercicio1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.c4.UD25Ejercicio1.dto.Fabricantes;

public interface IFabricantesDAO extends JpaRepository<Fabricantes, Long> {

}
