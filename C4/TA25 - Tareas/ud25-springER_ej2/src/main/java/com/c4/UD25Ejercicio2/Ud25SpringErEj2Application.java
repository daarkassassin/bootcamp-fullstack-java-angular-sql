package com.c4.UD25Ejercicio2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud25SpringErEj2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud25SpringErEj2Application.class, args);
	}

}
