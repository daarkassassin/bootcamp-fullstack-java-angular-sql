package com.c4.UD25Ejercicio2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD25Ejercicio2.dto.Departamento;
import com.c4.UD25Ejercicio2.service.DepartamentoServiceImpl;

@RestController
@RequestMapping("/api")
public class DepartamentoController {

	@Autowired
	DepartamentoServiceImpl departamentoServiceImpl;
	
	@GetMapping("/departamentos")
	public List<Departamento> listarDepartamento(){
		return departamentoServiceImpl.listarDepartamento();
	}
	
	@PostMapping("/departamento")
	public Departamento salvarCliente(@RequestBody Departamento departamento) {
		
		return departamentoServiceImpl.guardarDepartamento(departamento);
	}
	
	@GetMapping("/departamento/{codigo}")
	public Departamento departamentoXID(@PathVariable(name="codigo") int codigo) {
		
		Departamento departamento_xid= new Departamento();
		
		departamento_xid=departamentoServiceImpl.departamentoXID(codigo);
		
		System.out.println("Cliente XID: "+departamento_xid);
		
		return departamento_xid;
	}
	
	@PutMapping("/departamento/{codigo}")
	public Departamento actualizarDepartamento(@PathVariable(name="codigo")int codigo,@RequestBody Departamento departamento) {
		
		Departamento departamento_seleccionado= new Departamento();
		Departamento departamento_actualizado= new Departamento();
		
		departamento_seleccionado= departamentoServiceImpl.departamentoXID(codigo);
		
		departamento_seleccionado.setCodigo(departamento.getCodigo());
		departamento_seleccionado.setNombre(departamento.getNombre());
		departamento_seleccionado.setPresupuesto(departamento.getPresupuesto());
		
		departamento_actualizado = departamentoServiceImpl.actualizarDepartamento(departamento_seleccionado);
		
		System.out.println("El departamento actualizado es: "+ departamento_actualizado);
		
		return departamento_actualizado;
	}
	
	@DeleteMapping("/departamento/{codigo}")
	public void eleiminarDepartamento(@PathVariable(name="codigo")int codigo) {
		departamentoServiceImpl.eliminarDepartamento(codigo);
	}
	
	@GetMapping("/departamento/nombre/{nombre}")
	public List<Departamento> listarDepartamentoNombre(@PathVariable(name="nombre") String nombre) {
	    return departamentoServiceImpl.listarDepartamentoNomnbre(nombre);
	}
}
