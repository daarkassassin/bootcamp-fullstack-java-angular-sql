DROP table IF EXISTS Empleado;
DROP table IF EXISTS Trabajo;

create table Trabajo(
	id int auto_increment,
	nombre varchar(250),
	salario int,
	PRIMARY KEY (id)
);

create table Empleado(
	id int auto_increment,
	nombre varchar(250),
	trabajo int DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (trabajo) REFERENCES Trabajo(id)
);

insert into Trabajo (nombre, salario)values('Jefe',2200);
insert into Trabajo (nombre, salario)values('Encargado',1900);
insert into Trabajo (nombre, salario)values('Programador',1100);
insert into Trabajo (nombre, salario)values('Administrador',1200);
insert into Trabajo (nombre, salario)values('Voluntario',300);

insert into Empleado (nombre, trabajo)values('David',1);
insert into Empleado (nombre, trabajo)values('Xavi',2);
insert into Empleado (nombre, trabajo)values('Abraham',3);
insert into Empleado (nombre, trabajo)values('Anna',4);
insert into Empleado (nombre, trabajo)values('Jose',5);
