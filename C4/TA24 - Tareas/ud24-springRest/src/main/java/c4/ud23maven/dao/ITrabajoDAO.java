package c4.ud23maven.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import c4.ud23maven.dto.Trabajo;


public interface ITrabajoDAO extends JpaRepository<Trabajo, Long>{
	
	//Listar Empleados or campo nombre
		public List<Trabajo> findByNombre(String nombre);
}
