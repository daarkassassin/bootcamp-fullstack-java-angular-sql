package c4.ud23maven.service;

import java.util.List;

import c4.ud23maven.dto.Empleado;



public interface IEmpleadoService {

	//Metodos del CRUD
	public List<Empleado> listarEmpleados(); //Listar All 
	
	public Empleado guardarEmpleado(Empleado empleado);	//Guarda un Empleado CREATE
	
	public Empleado empleadoXID(Long id); //Leer datos de un Empleado READ
	
	public List<Empleado> listarEmpleadosNombre(String nombre);//Listar Empleados por campo nombre
	
	public List<Empleado> listarEmpleadosTrabajo(int trabajo);//Listar Empleados por campo nombre
	
	public Empleado actualizarEmpleado(Empleado empleado); //Actualiza datos del Empleado UPDATE
	
	public void eliminarEmpleado(Long id);// Elimina el Empleado DELETE
	
	
}
