package c4.ud23maven.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import c4.ud23maven.dto.Empleado;

public interface IEmpleadoDAO extends JpaRepository<Empleado, Long> {

	// Listar Empleados por campo nombre
	public List<Empleado> findByNombre(String nombre);

	// Listar Empleados por campo trabajo
	public List<Empleado> findByTrabajo(int trabajo);

}
