package c4.ud23maven.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import c4.ud23maven.dao.ITrabajoDAO;
import c4.ud23maven.dto.Trabajo;



@Service
public class TrabajoServiceImpl implements ITrabajoService{
	
	//Utilizamos los metodos de la interface IClienteDAO, es como si instaciaramos.
	@Autowired
	ITrabajoDAO iTrabajoDao;

	@Override
	public List<Trabajo> listarTrabajos() {
		// TODO Auto-generated method stub
		return iTrabajoDao.findAll();
	}

	@Override
	public Trabajo guardarTrabajo(Trabajo trabajo) {
		// TODO Auto-generated method stub
		return iTrabajoDao.save(trabajo);
	}

	@Override
	public Trabajo trabajoXID(Long id) {
		// TODO Auto-generated method stub
		return iTrabajoDao.findById(id).get();
	}

	@Override
	public Trabajo actualizarTrabajo(Trabajo trabajo) {
		return  iTrabajoDao.save(trabajo);
	}

	@Override
	public void eliminarTrabajo(Long id) {
		iTrabajoDao.deleteById(id);
		
	}

	@Override
	public List<Trabajo> listarTrabajosNombre(String nombre) {
		return iTrabajoDao.findByNombre(nombre);
	}
	
	

}
