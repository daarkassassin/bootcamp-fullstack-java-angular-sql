package c4.ud23maven.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import c4.ud23maven.dto.Trabajo;
import c4.ud23maven.service.TrabajoServiceImpl;

@RestController
@RequestMapping("/api")
public class TrabajoController {
	
	@Autowired
	TrabajoServiceImpl trabajoServiceImpl;
	
	@GetMapping("/trabajos")
	public List<Trabajo> listarTrabajos(){
		return trabajoServiceImpl.listarTrabajos();
	}	
	
	@PostMapping("/trabajos")
	public Trabajo salvarTrabajos(@RequestBody Trabajo trabajo) {
		
		return trabajoServiceImpl.guardarTrabajo(trabajo);
	}
	
	
	@GetMapping("/trabajos/{id}")
	public Trabajo trabajoXID(@PathVariable(name="id") Long id) {
		
		Trabajo trabajo_xid= new Trabajo();
		
		trabajo_xid=trabajoServiceImpl.trabajoXID(id);
		
		System.out.println("Trabajo XID: "+trabajo_xid);
		
		return trabajo_xid;
	}
	
	@PutMapping("/trabajos/{id}")
	public Trabajo actualizarTrabajo(@PathVariable(name="id")Long id,@RequestBody Trabajo trabajo) {
		
		Trabajo trabajo_seleccionado= new Trabajo();
		Trabajo trabajo_actualizado= new Trabajo();
		
		trabajo_seleccionado= trabajoServiceImpl.trabajoXID(id);
		
		trabajo_seleccionado.setTrabajo(trabajo.getTrabajo());
		trabajo_seleccionado.setSalario(trabajo.getSalario());
		
		trabajo_actualizado = trabajoServiceImpl.actualizarTrabajo(trabajo_seleccionado);
		
		System.out.println("El trabajo actualizado es: "+ trabajo_actualizado);
		
		return trabajo_actualizado;
	}
	
	@DeleteMapping("/trabajos/{id}")
	public void eliminarTrabajo(@PathVariable(name="id")Long id) {
		trabajoServiceImpl.eliminarTrabajo(id);
	}
	
	
	
	
}
