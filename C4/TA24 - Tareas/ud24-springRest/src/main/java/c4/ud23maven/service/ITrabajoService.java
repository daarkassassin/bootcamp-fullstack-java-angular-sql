package c4.ud23maven.service;

import java.util.List;

import c4.ud23maven.dto.Trabajo;



public interface ITrabajoService {

	//Metodos del CRUD
	public List<Trabajo> listarTrabajos(); //Listar All 
	
	public Trabajo guardarTrabajo(Trabajo trabajo);	//Guarda un Trabajo CREATE
	
	public Trabajo trabajoXID(Long id); //Leer datos de un Trabajo READ
	
	public List<Trabajo> listarTrabajosNombre(String nombre);//Listar Trabajos por campo nombre
	
	public Trabajo actualizarTrabajo(Trabajo trabajo); //Actualiza datos del Trabajo UPDATE
	
	public void eliminarTrabajo(Long id);// Elimina el Trabajo DELETE
	
	
}
