package c4.ud23maven.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Trabajo") // en caso que la tabala sea diferente
public class Trabajo {

	// Atributos de entidad cliente
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // busca ultimo valor e incrementa desde id final de db
	private Long id;
	@Column(name = "nombre") // no hace falta si se llama igual
	private String nombre;
	@Column(name = "salario") // no hace falta si se llama igual
	private Long salario;

	// Constructores
	public Trabajo() {

	}

	/**
	 * @param id
	 * @param trabajo
	 * @param salario
	 */
	public Trabajo(Long id, String nombre, Long salario) {
		// super();
		this.id = id;
		this.nombre = nombre;
		this.salario = salario;
	}
	
	// Getters y Setters
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the trabajo
	 */
	public String getTrabajo() {
		return nombre;
	}

	/**
	 * @param trabajo the trabajo to set
	 */
	public void setTrabajo(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the salario
	 */
	public Long getSalario() {
		return salario;
	}

	/**
	 * @param salario the salario to set
	 */
	public void setSalario(Long salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "Trabajo [id=" + id + ", nombre=" + nombre + ", salario=" + salario + "]";
	}
	
	
	
	
	

}
