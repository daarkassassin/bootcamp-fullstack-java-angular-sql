package c4.ud22_patronMVC.view.asignado_a;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.controller.Asignado_AController;
import c4.ud22_patronMVC.controller.CientificoController;
import c4.ud22_patronMVC.controller.ProyectoController;
import c4.ud22_patronMVC.model.dto.Asignado_A;
import c4.ud22_patronMVC.model.dto.Cientifico;
import c4.ud22_patronMVC.model.dto.Proyecto;

public class VentanaRegistroAsignado_A extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private Asignado_AController asignado_AController;
	private CientificoController cientificoController;
	private ProyectoController proyectoController;
	
	private JTextField textProyecto, textDni;
	private JLabel proyecto, dni;
	private JButton botonGuardar, botonCancelar;
	
	public VentanaRegistroAsignado_A() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(10, 95, 120, 25);
		botonGuardar.setText("Registrar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(140, 95, 120, 25);
		botonCancelar.setText("Cancelar");

		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		getContentPane().add(botonCancelar);
		getContentPane().add(botonGuardar);

		proyecto = new JLabel();
		proyecto.setText("id_proyecto");
		proyecto.setBounds(10, 23, 80, 25);
		getContentPane().add(proyecto);

		dni = new JLabel();
		dni.setText("Dni");
		dni.setBounds(10, 59, 80, 25);
		getContentPane().add(dni);

		textProyecto = new JTextField();
		textProyecto.setText("");
		textProyecto.setBounds(70, 23, 190, 25);
		getContentPane().add(textProyecto);

		textDni = new JTextField();
		textDni.setText("");
		textDni.setBounds(70, 59, 190, 25);
		getContentPane().add(textDni);
		
		limpiar();
		setSize(277, 181);
		setTitle("Registro de Cientificos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

	}
	private void limpiar() {
		
	}

	public void setCoordinador(Asignado_AController asignado_AController, CientificoController cientificoController, ProyectoController proyectoController) {
		this.asignado_AController = asignado_AController;
		this.cientificoController = cientificoController;
		this.proyectoController = proyectoController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Cientifico miCientifico = cientificoController.buscarCientifico(textDni.getText());
				if (miCientifico == null) {
					throw new Exception("El cientifico ingresado no existe");
				}
				
				Proyecto miProyecto = proyectoController.buscarProyecto(textProyecto.getText());
				if (miProyecto == null) {
					throw new Exception("El proyecto ingresado no existe");
				}
				
				
				asignado_AController.registrarAsignado_A(miCientifico, miProyecto);
				
			} catch (Exception ex) {
				
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos"+ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}
	}

}
