package c4.ud22_patronMVC.model.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import c4.ud22_patronMVC.model.conexion.ConnectionDB;
import c4.ud22_patronMVC.model.dto.Cientifico;

public class CientificoDao {

	public void registrarCientifico(Cientifico cientifico) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			// Ejecutar Query que insrta el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "insert into Cientificos(dni, nom_apels) values ('" + cientifico.getDni() + "', '" + cientifico.getNomApels() + "');");

			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
				
			// Cerrar conexion
			conex.closeConnection();
			
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente", "Información",JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se ha registrado", "Información", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public ArrayList<Cientifico> listarCientificos() {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			ArrayList<Cientifico> listaCientificos = new ArrayList<Cientifico>();
			

			// Ejecutar Query que obtiene el cientifico, guardar los datos en el resultSet i
			// guardarlos despues en el objeto cientifico
			ResultSet res = conex.getValues("UD22-E3", "SELECT * FROM Cientificos;");
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				Cientifico cientifico = new Cientifico();
				cientifico.setDni(res.getString("dni"));
				cientifico.setNomApels(res.getString("nom_apels"));
				listaCientificos.add(cientifico);
			}
			// Cerrar conexion
			conex.closeConnection();
			
			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return listaCientificos;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public Cientifico buscarCientifico(String dni) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			Cientifico cientifico = new Cientifico();

			// Ejecutar Query que obtiene el cientifico, guardar los datos en el resultSet i
			// guardarlos despues en el objeto cientifico
			ResultSet res = conex.getValues("UD22-E3", "SELECT * FROM Cientificos where dni = '" + dni + "';");

			// Si el cliente no existe, lanzar un error
			if (res == null)
				throw new Exception();
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				cientifico.setDni(res.getString("dni"));
				cientifico.setNomApels(res.getString("nom_apels"));
			}
			// Cerrar conexion
			conex.closeConnection();

			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return cientifico;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}

	public void modificarCientifico(Cientifico cientifico) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			// Ejecutar Query que modifica el cientifico en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "update Cientificos set nom_apels = '" + cientifico.getNomApels() + "' where dni = '" + cientifico.getDni() + "';");

			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
				
			// Cerrar conexion
			conex.closeConnection();
			
			JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ", "Confirmación",JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Error al Modificar", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void eliminarCientifico(String dni) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			// Ejecutar Query que modifica el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "DELETE FROM Cientificos WHERE dni='" + dni + "'");
			
			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
			
			// Cerrar conexion
			conex.closeConnection();
			
            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
            
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}

}
