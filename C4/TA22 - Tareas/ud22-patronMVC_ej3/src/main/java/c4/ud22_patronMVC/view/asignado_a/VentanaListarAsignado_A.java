package c4.ud22_patronMVC.view.asignado_a;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import c4.ud22_patronMVC.controller.Asignado_AController;
import c4.ud22_patronMVC.model.dto.Asignado_A;
import c4.ud22_patronMVC.model.service.Asignado_AServ;

public class VentanaListarAsignado_A extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private Asignado_AController asignado_AController; 
	
	private JButton botonVolver, botonListarAsignado_A;
	
	private int respuesta;
	
	private JTable lista;
	JScrollPane panelLista;
	
	public VentanaListarAsignado_A() {

		botonVolver = new JButton();
		botonVolver.setBounds(352, 422, 120, 25);
		botonVolver.setText("Cancelar");
		
		botonListarAsignado_A = new JButton();
		botonListarAsignado_A.setBounds(10, 422, 120, 25);
		botonListarAsignado_A.setText("Listar Asignado_A");
		
		lista = new JTable();
		lista.setBounds(10, 11, 462, 400);
	
		panelLista = new JScrollPane(lista);
		panelLista.setBounds(10, 11, 462, 400);
		
		botonVolver.addActionListener(this);
		botonListarAsignado_A.addActionListener(this);
		
		getContentPane().add(botonVolver);
		getContentPane().add(botonListarAsignado_A);
		getContentPane().add(panelLista);
	
		setSize(500, 500);
		setTitle("Listar Asignado_A");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);	
		
	}
	
	public void setCoordinador(Asignado_AController asignado_AController) {
		this.asignado_AController=asignado_AController;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonVolver) {
			this.dispose();		
		}
		if (e.getSource() == botonListarAsignado_A) {
			
			ArrayList<Asignado_A> listaAsignado_A = asignado_AController.listarAsignado_A();
			
			if (listaAsignado_A != null) {
				muestraAsignado_A(listaAsignado_A);
			} else if (Asignado_AServ.consultaAsignado_A == true) {
				JOptionPane.showMessageDialog(null, "No hay Proyectos Asignados a Cientificos", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	private void muestraAsignado_A(ArrayList<Asignado_A> misAsignado_A) {

		// Crear la tabla con los datos del arraylist de clientes
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("ID Cientifico");
	    model.addColumn("ID Proyecto");
	    model.addColumn("Eliminar");
		
		Iterator <Asignado_A> iterator = misAsignado_A.iterator();
		Asignado_A item;
		int contador = 0;
		while (iterator.hasNext()) {
			item = iterator.next();
			
			//Añadimos 'delete' al final de cada fila.
	    	model.insertRow(contador, new Object[] { item.getCientifico(), item.getProyecto(), "delete"});
	        contador++;
	    }
		
	    lista.setModel(model);
	    
	    //Añadimos el boton eliminar a la tabla 'lista' con la accion 'delete' en la columna '2';
	    ButtonColumn buttonColumn = new ButtonColumn(lista, delete, 2);
	}
	private void eliminarQuestion() {
		respuesta = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar?", "Confirmación", JOptionPane.YES_NO_OPTION);
	}
	
	Action delete = new AbstractAction(){
		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent e){
			eliminarQuestion();
			if (respuesta == JOptionPane.YES_NO_OPTION) {
				JTable table = (JTable)e.getSource();
		        int modelRow = Integer.valueOf( e.getActionCommand() );
		        String data = ((DefaultTableModel)table.getModel()).getDataVector().elementAt(modelRow).toString();
		        
		        //System.out.println(data);
		        
		        String[] parts = data.split(",");
		        String dni = parts[0].substring(1);
		        String id_proyecto = parts[1].substring(1);
		        
		        //System.out.println(dni);
		        //System.out.println(id_proyecto);
		        
		        asignado_AController.eliminarAsignado_A(dni, id_proyecto);
		        ((DefaultTableModel)table.getModel()).removeRow(modelRow);
			}
			
	        
	    }
	};
	 
	
	
	
}

