package c4.ud22_patronMVC.view.proyecto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.controller.ProyectoController;
import c4.ud22_patronMVC.model.dto.Proyecto;

public class VentanaRegistroProyecto extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private ProyectoController proyectoController; 
	
	private JButton botonGuardar, botonCancelar;
	private JLabel id, nombre, horas;
	private JTextField textId, textNombre, textHoras;
	
	public VentanaRegistroProyecto() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(10, 116, 120, 25);
		botonGuardar.setText("Registrar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(141, 116, 120, 25);
		botonCancelar.setText("Cancelar");
		
		id = new JLabel();
		id.setText("ID");
		id.setBounds(10, 11, 80, 25);

		nombre = new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(10, 47, 80, 25);
		
		horas = new JLabel();
		horas.setText("Horas");
		horas.setBounds(10, 80, 80, 25);
		
		
		textId = new JTextField();
		textId.setText("");
		textId.setBounds(71, 11, 190, 25);

		textNombre = new JTextField();
		textNombre.setText("");
		textNombre.setBounds(71, 47, 190, 25);
	
		textHoras = new JTextField();
		textHoras.setText("");
		textHoras.setBounds(70, 80, 190, 25);
		
		
		
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		
		getContentPane().add(botonCancelar);
		getContentPane().add(botonGuardar);
		getContentPane().add(textHoras);
		getContentPane().add(textNombre);
		getContentPane().add(textId);
		getContentPane().add(horas);
		getContentPane().add(nombre);
		getContentPane().add(id);
		
		
		limpiar();
		
		setSize(300, 190);
		setTitle("Registro de Proyectos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
	}
	
	private void limpiar() {
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Proyecto miProyecto = new Proyecto();
				
				miProyecto.setId(textId.getText());
				miProyecto.setNombre(textNombre.getText());
				miProyecto.setHoras(Integer.parseInt(textHoras.getText()));
				
				proyectoController.registrarProyecto(miProyecto);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}
	}
	

	public void setCoordinador(ProyectoController proyectoController) {
		this.proyectoController = proyectoController;
	}

}
