package c4.ud22_patronMVC.view.cientifico;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.controller.CientificoController;
import c4.ud22_patronMVC.model.dto.Cientifico;
import c4.ud22_patronMVC.model.service.CientificoServ;

public class VentanaBuscarCientifico extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private CientificoController cientificoController; // objeto clienteController que permite la relacion entre esta
														// clase y
														// la clase clienteController
	private JTextField textNombre, textDni;
	private JLabel nombre, dni;
	private JButton botonGuardar, botonCancelar, botonBuscar, botonModificar, botonEliminar;

	public VentanaBuscarCientifico() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(11, 83, 120, 25);
		botonGuardar.setText("Guardar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(151, 119, 120, 25);
		botonCancelar.setText("Cancelar");

		botonBuscar = new JButton();
		botonBuscar.setBounds(221, 11, 50, 25);
		botonBuscar.setText("Ok");

		botonEliminar = new JButton();
		botonEliminar.setBounds(11, 119, 120, 25);
		botonEliminar.setText("Eliminar");

		botonModificar = new JButton();
		botonModificar.setBounds(151, 83, 120, 25);
		botonModificar.setText("Modificar");


		nombre = new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(20, 47, 80, 25);
		getContentPane().add(nombre);


		dni = new JLabel();
		dni.setText("Dni");
		dni.setBounds(20, 11, 80, 25);
		getContentPane().add(dni);
		

		textNombre = new JTextField();
		textNombre.setBounds(80, 47, 138, 25);
		getContentPane().add(textNombre);

		textDni = new JTextField();
		textDni.setBounds(80, 11, 138, 25);
		getContentPane().add(textDni);
	

		botonModificar.addActionListener(this);
		botonEliminar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);

		getContentPane().add(botonCancelar);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonModificar);
		getContentPane().add(botonEliminar);
		getContentPane().add(botonGuardar);
		
		limpiar();

		setSize(304, 190);
		setTitle("Administrar Cientificos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

		

	}

	public void setCoordinador(CientificoController cientificoController) {
		this.cientificoController = cientificoController;

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Cientifico miCientifico = new Cientifico();
				miCientifico.setNomApels(textNombre.getText());
				miCientifico.setDni(textDni.getText());

				cientificoController.modificarCientifico(miCientifico);

				if (CientificoServ.modificaCientifico == true) {
					limpiar();
				}
			} catch (Exception e2) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
			}

		}

		if (e.getSource() == botonBuscar) {
			Cientifico miCientifico = cientificoController.buscarCientifico(textDni.getText());
			if (miCientifico != null) {
				muestraCientifico(miCientifico);
			} else if (CientificoServ.consultaCientifico == true) {
				JOptionPane.showMessageDialog(null, "El cliente no Existe", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}

		if (e.getSource() == botonModificar) {
			habilita(false, true, false, true, false, false);

		}

		if (e.getSource() == botonEliminar) {
			if (!textDni.getText().equals("")) {
				int respuesta = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el cientifico?",
						"Confirmación", JOptionPane.YES_NO_OPTION);
				if (respuesta == JOptionPane.YES_NO_OPTION) {
					cientificoController.eliminarCientifico(textDni.getText());
					limpiar();
				}
			} else {
				JOptionPane.showMessageDialog(null, "Ingrese un numero de Documento", "Información",
						JOptionPane.WARNING_MESSAGE);
			}

		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}

	}

	/**
	 * permite cargar los datos de la persona consultada
	 * 
	 * @param miPersona
	 */
	private void muestraCientifico(Cientifico miCientifico) {
		textNombre.setText(miCientifico.getNomApels());
		textDni.setText(miCientifico.getDni());
		habilita(true, false, true, false, true, true);
	}

	/**
	 * Permite limpiar los componentes
	 */
	public void limpiar() {
		textNombre.setText("");
		textDni.setText("");
		habilita(true, false, true, false, true, true);
	}

	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * 
	 * @param nombre
	 * @param dni
	 * @param bBuscar
	 * @param bGuardar
	 * @param bModificar
	 * @param bEliminar
	 */
	public void habilita(boolean dni, boolean nombre, boolean bBuscar, boolean bGuardar, boolean bModificar, boolean bEliminar) {
		textDni.setEditable(dni);
		textNombre.setEditable(nombre);
		botonBuscar.setEnabled(bBuscar);
		botonGuardar.setEnabled(bGuardar);
		botonModificar.setEnabled(bModificar);
		botonEliminar.setEnabled(bEliminar);
	}

}
