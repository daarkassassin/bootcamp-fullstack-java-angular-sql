package c4.ud22_patronMVC.model.service;

import javax.swing.JOptionPane;

import c4.ud22_patronMVC.controller.Asignado_AController;
import c4.ud22_patronMVC.model.dao.Asignado_ADao;
import c4.ud22_patronMVC.model.dao.CientificoDao;
import c4.ud22_patronMVC.model.dto.Asignado_A;
import c4.ud22_patronMVC.model.dto.Cientifico;
import c4.ud22_patronMVC.model.dto.Proyecto;

public class Asignado_AServ {

	private Asignado_AController asignado_AController; 
	public static boolean consultaAsignado_A=false;
	public static boolean modificaAsignado_A=false;

	//Metodo de vinculación con el controller principal
	public void setasignado_AController(Asignado_AController asignado_AController) {
		this.setController(asignado_AController);		
	}

	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Cientifico cientifico, Proyecto proyecto) {
		Asignado_ADao miAsignado_ADao;
		miAsignado_ADao = new Asignado_ADao();
		miAsignado_ADao.registrarAsignado_A(cientifico, proyecto);	

	}

	//Metodo que valida los datos de consulta antes de pasar estos al DAO
	public Asignado_A validarConsulta(String cientifico, String proyecto) {
		Asignado_ADao miAsignado_ADao;
		boolean consultaAsignado_A;
		try {
			miAsignado_ADao = new Asignado_ADao();
			consultaAsignado_A = true;
			return miAsignado_ADao.buscarAsignado_A(cientifico, proyecto);
		} catch (Exception e) {
			consultaAsignado_A = false;
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
		}		
		return null;
	}
	
	//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
	public void validarEliminacion(String cientifico, String proyecto) {
		Asignado_ADao miAsignado_ADao=new Asignado_ADao();
		miAsignado_ADao.eliminarAsignado_A(cientifico, proyecto);
	}
	
	public Asignado_AController getAsignado_AController() {
		return asignado_AController;
	}

	public void setController(Asignado_AController Asignado_AController) {
		this.asignado_AController = Asignado_AController;
	}



}
