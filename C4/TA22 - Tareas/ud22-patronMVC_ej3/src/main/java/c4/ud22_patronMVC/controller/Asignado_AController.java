
/*
 * Esta parte del patrón es la que define la lógica de administración del sistema, 
 * establece la conexión entre la vista y el modelo.
 */

package c4.ud22_patronMVC.controller;

import java.util.ArrayList;

import c4.ud22_patronMVC.model.dao.Asignado_ADao;
import c4.ud22_patronMVC.model.dto.Asignado_A;
import c4.ud22_patronMVC.model.dto.Cientifico;
import c4.ud22_patronMVC.model.dto.Proyecto;
import c4.ud22_patronMVC.model.service.Asignado_AServ;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.asignado_a.VentanaListarAsignado_A;
import c4.ud22_patronMVC.view.asignado_a.VentanaRegistroAsignado_A;

public class Asignado_AController {
	
	private Asignado_AServ asignado_AServ;
	private VentanaPrincipal miVentanaPrincipal;
	private VentanaRegistroAsignado_A miVentanaRegistroAsignado_A;
	private VentanaListarAsignado_A miVentanaListarAsignado_A;
	
	//Metodos getter Setters de vistas
	public Asignado_AServ getAsignado_AServ() {
		return asignado_AServ;
	}
	public void setAsignado_AServ(Asignado_AServ asignado_AServ) {
		this.asignado_AServ = asignado_AServ;
	}
	public VentanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	public VentanaRegistroAsignado_A getMiVentanaRegistroAsignado_A() {
		return miVentanaRegistroAsignado_A;
	}
	public void setMiVentanaRegistroAsignado_A(VentanaRegistroAsignado_A miVentanaRegistroAsignado_A) {
		this.miVentanaRegistroAsignado_A = miVentanaRegistroAsignado_A;
	}
	public VentanaListarAsignado_A getMiVentanaListarAsignado_A() {
		return miVentanaListarAsignado_A;
	}
	public void setMiVentanaListarAsignado_A(VentanaListarAsignado_A miVentanaListarAsignado_A) {
		this.miVentanaListarAsignado_A = miVentanaListarAsignado_A;
	}
	
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarVentanaRegistroAsignado_A() {
		miVentanaRegistroAsignado_A.setVisible(true);
	}
	public void mostrarVentanaListarAsignado_A() {
		miVentanaListarAsignado_A.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarAsignado_A(Cientifico miCientifico, Proyecto miProyecto) {
		asignado_AServ.validarRegistro(miCientifico, miProyecto);
	}
	
	public Asignado_A buscarAsignado_A(String codigoCientifico, String codigoProyecto) {
		return asignado_AServ.validarConsulta(codigoCientifico, codigoProyecto);
	}
	
	public void eliminarAsignado_A(String codigoCientifico, String codigoProyecto) {
		asignado_AServ.validarEliminacion(codigoCientifico, codigoProyecto);
	}
	
	public ArrayList<Asignado_A> listarAsignado_A() {
		Asignado_ADao miAsignado_ADao = new Asignado_ADao();
		return miAsignado_ADao.listarAsignado_A();
	}


}
