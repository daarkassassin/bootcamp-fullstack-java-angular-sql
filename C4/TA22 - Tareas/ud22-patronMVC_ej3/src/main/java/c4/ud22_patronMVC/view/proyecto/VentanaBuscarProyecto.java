package c4.ud22_patronMVC.view.proyecto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.controller.ProyectoController;
import c4.ud22_patronMVC.model.dto.Proyecto;
import c4.ud22_patronMVC.model.service.ProyectoServ;

public class VentanaBuscarProyecto extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;
	private ProyectoController proyectoController; 
	
	private JLabel id, nombre, horas;
	private JTextField textId, textNombre, textHoras;
	private JButton botonGuardar, botonCancelar, botonBuscar, botonModificar, botonEliminar;
	
	public VentanaBuscarProyecto() {
	
		botonGuardar = new JButton();
		botonGuardar.setBounds(11, 116, 120, 25);
		botonGuardar.setText("Guardar");
	
		botonCancelar = new JButton();
		botonCancelar.setBounds(151, 152, 120, 25);
		botonCancelar.setText("Cancelar");
	
		botonBuscar = new JButton();
		botonBuscar.setBounds(221, 11, 50, 25);
		botonBuscar.setText("Ok");
	
		botonEliminar = new JButton();
		botonEliminar.setBounds(11, 152, 120, 25);
		botonEliminar.setText("Eliminar");
	
		botonModificar = new JButton();
		botonModificar.setBounds(151, 116, 120, 25);
		botonModificar.setText("Modificar");
	
		
		id = new JLabel();
		id.setText("ID");
		id.setBounds(10, 11, 80, 25);

		nombre = new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(10, 47, 80, 25);
		
		horas = new JLabel();
		horas.setText("Horas");
		horas.setBounds(10, 80, 80, 25);
		
		
		textId = new JTextField();
		textId.setText("");
		textId.setBounds(71, 11, 140, 25);

		textNombre = new JTextField();
		textNombre.setText("");
		textNombre.setBounds(71, 47, 200, 25);
	
		textHoras = new JTextField();
		textHoras.setText("");
		textHoras.setBounds(70, 80, 201, 25);
		
		
		
		
		
		
		botonModificar.addActionListener(this);
		botonEliminar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);

		getContentPane().add(botonCancelar);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonModificar);
		getContentPane().add(botonEliminar);
		getContentPane().add(botonGuardar);
		getContentPane().add(textHoras);
		getContentPane().add(textNombre);
		getContentPane().add(textId);
		getContentPane().add(horas);
		getContentPane().add(nombre);
		getContentPane().add(id);
		
		
		
		limpiar();

		setSize(304, 225);
		setTitle("Administrar Proyectos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Proyecto miProyecto = new Proyecto();
				
				miProyecto.setId(textId.getText());
				miProyecto.setNombre(textNombre.getText());
				miProyecto.setHoras(Integer.parseInt(textHoras.getText()));

				proyectoController.modificarProyecto(miProyecto);

				if (ProyectoServ.modificaProyecto == true) {
					limpiar();
				}
			} catch (Exception e2) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
			}

		}

		if (e.getSource() == botonBuscar) {
			Proyecto miProyecto = proyectoController.buscarProyecto(textId.getText());
			if (miProyecto != null) {
				muestraProyecto(miProyecto);
			} else if (ProyectoServ.consultaProyecto == true) {
				JOptionPane.showMessageDialog(null, "El cliente no Existe", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}

		if (e.getSource() == botonModificar) {
			habilita(false, true, true, false, true, false, false);

		}

		if (e.getSource() == botonEliminar) {
			if (!textId.getText().equals("")) {
				int respuesta = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el proyecto?", "Confirmación", JOptionPane.YES_NO_OPTION);
				if (respuesta == JOptionPane.YES_NO_OPTION) {
					proyectoController.eliminarProyecto(textId.getText());
					limpiar();
				}
			} else {
				JOptionPane.showMessageDialog(null, "Ingrese un Id de Proyecto", "Información", JOptionPane.WARNING_MESSAGE);
			}

		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}

	}
	
	private void muestraProyecto(Proyecto miProyecto) {
		textId.setText(miProyecto.getId());
		textNombre.setText(miProyecto.getNombre());
		textHoras.setText(Integer.toString(miProyecto.getHoras()));

		habilita(true, false, false, true, false, true, true);
	}
	
	public void limpiar() {
		textId.setText("");
		textNombre.setText("");
		textHoras.setText("");
		
		habilita(true, false, false, true, false, true, true);
	}
	
	public void habilita(boolean id, boolean nombre, boolean horas, boolean bBuscar, boolean bGuardar, boolean bModificar, boolean bEliminar) {
		textId.setEditable(id);
		textNombre.setEditable(nombre);
		textHoras.setEditable(horas);
		botonBuscar.setEnabled(bBuscar);
		botonGuardar.setEnabled(bGuardar);
		botonModificar.setEnabled(bModificar);
		botonEliminar.setEnabled(bEliminar);
	}
	
	public void setCoordinador(ProyectoController proyectoController) {
		this.proyectoController = proyectoController;
	}

}
