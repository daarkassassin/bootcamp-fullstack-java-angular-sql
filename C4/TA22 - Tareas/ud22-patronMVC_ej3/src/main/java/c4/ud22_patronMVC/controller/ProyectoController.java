package c4.ud22_patronMVC.controller;

import java.util.ArrayList;

import c4.ud22_patronMVC.model.dao.CientificoDao;
import c4.ud22_patronMVC.model.dao.ProyectoDao;
import c4.ud22_patronMVC.model.dto.Proyecto;
import c4.ud22_patronMVC.model.service.ProyectoServ;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.proyecto.VentanaBuscarProyecto;
import c4.ud22_patronMVC.view.proyecto.VentanaListarProyecto;
import c4.ud22_patronMVC.view.proyecto.VentanaRegistroProyecto;

public class ProyectoController {

	private ProyectoServ proyectoServ;
	private ProyectoDao proyectoDao;
	private VentanaPrincipal miVentanaPrincipal;
	private VentanaRegistroProyecto miVentanaRegistroProyecto;
	private VentanaBuscarProyecto miVentanaBuscarProyecto;
	private VentanaListarProyecto miVentanaListarProyecto;
	
	//Metodos getter Setters de vistas
	public ProyectoServ getProyectoServ() {
		return proyectoServ;
	}
	public void setProyectoServ(ProyectoServ proyectoServ) {
		this.proyectoServ = proyectoServ;
	}
	public ProyectoDao getProyectoDao() {
		return proyectoDao;
	}
	public void setProyectoDao(ProyectoDao proyectoDao) {
		this.proyectoDao = proyectoDao;
	}
	public VentanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	public VentanaRegistroProyecto getMiVentanaRegistroProyecto() {
		return miVentanaRegistroProyecto;
	}
	public void setMiVentanaRegistroProyecto(VentanaRegistroProyecto miVentanaRegistroProyecto) {
		this.miVentanaRegistroProyecto = miVentanaRegistroProyecto;
	}
	public VentanaBuscarProyecto getMiVentanaBuscarProyecto() {
		return miVentanaBuscarProyecto;
	}
	public void setMiVentanaBuscarProyecto(VentanaBuscarProyecto miVentanaBuscarProyecto) {
		this.miVentanaBuscarProyecto = miVentanaBuscarProyecto;
	}
	public VentanaListarProyecto getMiVentanaListarProyecto() {
		return miVentanaListarProyecto;
	}
	public void setMiVentanaListarProyecto(VentanaListarProyecto miVentanaListarProyecto) {
		this.miVentanaListarProyecto = miVentanaListarProyecto;
	}
	
	//Hace visible las vistas de Registro, Consulta y Listar
	public void mostrarVentanaRegistroProyecto() {
		miVentanaRegistroProyecto.setVisible(true);
	}
	public void mostrarVentanaConsultaProyecto() {
		miVentanaBuscarProyecto.setVisible(true);
	}
	public void mostrarVentanaListarProyecto() {
		miVentanaListarProyecto.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarProyecto(Proyecto miProyecto) {
		proyectoServ.validarRegistro(miProyecto);
	}
	
	public Proyecto buscarProyecto(String codigoProyecto) {
		return proyectoServ.validarConsulta(codigoProyecto);
	}
	
	public void modificarProyecto(Proyecto miProyecto) {
		proyectoServ.validarModificacion(miProyecto);
	}
	
	public void eliminarProyecto(String codigo) {
		proyectoServ.validarEliminacion(codigo);
	}
	
	public ArrayList<Proyecto> listarProyecto() {
		ProyectoDao proyectoDao = new ProyectoDao();
		return proyectoDao.listarProyecto();
	}	
}
