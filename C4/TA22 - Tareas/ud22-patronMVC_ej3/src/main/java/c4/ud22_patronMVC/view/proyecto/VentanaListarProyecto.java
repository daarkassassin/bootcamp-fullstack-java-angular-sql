package c4.ud22_patronMVC.view.proyecto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import c4.ud22_patronMVC.controller.ProyectoController;
import c4.ud22_patronMVC.model.dto.Proyecto;
import c4.ud22_patronMVC.model.service.ProyectoServ;

public class VentanaListarProyecto extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private ProyectoController proyectoController; 
	
	private JButton botonVolver, botonListarProyecto;
	
	private JTable lista;
	JScrollPane panelLista;
	
	public VentanaListarProyecto() {

		botonVolver = new JButton();
		botonVolver.setBounds(352, 422, 120, 25);
		botonVolver.setText("Cancelar");
		
		botonListarProyecto = new JButton();
		botonListarProyecto.setBounds(10, 422, 120, 25);
		botonListarProyecto.setText("Listar Proyecto");
		
		lista = new JTable();
		lista.setBounds(10, 11, 462, 400);
	
		panelLista = new JScrollPane(lista);
		panelLista.setBounds(10, 11, 462, 400);
		
		botonVolver.addActionListener(this);
		botonListarProyecto.addActionListener(this);
		
		getContentPane().add(botonVolver);
		getContentPane().add(botonListarProyecto);
		getContentPane().add(panelLista);
	
		setSize(500, 500);
		setTitle("Listar Proyecto");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);	
	}
	
	public void setCoordinador(ProyectoController proyectoController) {
		this.proyectoController=proyectoController;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonVolver) {
			this.dispose();		
		}
		if (e.getSource() == botonListarProyecto) {
			
			ArrayList<Proyecto> listaProyecto = proyectoController.listarProyecto();
			if (listaProyecto != null) {
				muestraProyecto(listaProyecto);
			} else if (ProyectoServ.consultaProyecto == true) {
				JOptionPane.showMessageDialog(null, "No hay Proyectos", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	private void muestraProyecto(ArrayList<Proyecto> misProyectos) {

		// Crear la tabla con los datos del arraylist de clientes
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("ID");
		model.addColumn("Nombre");
	    model.addColumn("Horas");
	    
		Iterator <Proyecto> iterator = misProyectos.iterator();
		Proyecto item;
		int contador = 0;
		while (iterator.hasNext()) {
			item = iterator.next();
	    	model.insertRow(contador, new Object[] { item.getId(), item.getNombre(), item.getHoras() });
	        contador++;
	    }
		
	    lista.setModel(model);
	}
}
