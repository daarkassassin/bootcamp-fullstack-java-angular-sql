
/*
 * Esta parte del patrón es la que define la lógica de administración del sistema, 
 * establece la conexión entre la vista y el modelo.
 */

package c4.ud22_patronMVC.controller;

import java.util.ArrayList;

import c4.ud22_patronMVC.model.dao.CientificoDao;
import c4.ud22_patronMVC.model.dto.Cientifico;
import c4.ud22_patronMVC.model.service.CientificoServ;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.cientifico.VentanaBuscarCientifico;
import c4.ud22_patronMVC.view.cientifico.VentanaListarCientifico;
import c4.ud22_patronMVC.view.cientifico.VentanaRegistroCientifico;




public class CientificoController {
	
	private CientificoServ cientificoServ;
	private VentanaPrincipal miVentanaPrincipal;
	private VentanaRegistroCientifico miVentanaRegistroCientifico;
	private VentanaBuscarCientifico miVentanaBuscarCientifico;
	private VentanaListarCientifico miVentanaListarCientifico;
	
	//Metodos getter Setters de vistas
	public CientificoServ getCientificoServ() {
		return cientificoServ;
	}
	public void setCientificoServ(CientificoServ cientificoServ) {
		this.cientificoServ = cientificoServ;
	}
	public VentanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	public VentanaRegistroCientifico getMiVentanaRegistroCientifico() {
		return miVentanaRegistroCientifico;
	}
	public void setMiVentanaRegistroCientifico(VentanaRegistroCientifico miVentanaRegistroCientifico) {
		this.miVentanaRegistroCientifico = miVentanaRegistroCientifico;
	}
	public VentanaBuscarCientifico getMiVentanaBuscarCientifico() {
		return miVentanaBuscarCientifico;
	}
	public void setMiVentanaBuscarCientifico(VentanaBuscarCientifico miVentanaBuscarCientifico) {
		this.miVentanaBuscarCientifico = miVentanaBuscarCientifico;
	}
	public VentanaListarCientifico getMiVentanaListarCientifico() {
		return miVentanaListarCientifico;
	}
	public void setMiVentanaListarCientifico(VentanaListarCientifico miVentanaListarCientifico) {
		this.miVentanaListarCientifico = miVentanaListarCientifico;
	}
	
	
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarVentanaRegistroCientifico() {
		miVentanaRegistroCientifico.setVisible(true);
	}
	public void mostrarVentanaConsultaCientifico() {
		miVentanaBuscarCientifico.setVisible(true);
	}
	public void mostrarVentanaListarCientifico() {
		miVentanaListarCientifico.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCientifico(Cientifico miCientifico) {
		cientificoServ.validarRegistro(miCientifico);
	}
	
	public Cientifico buscarCientifico(String codigoCientifico) {
		return cientificoServ.validarConsulta(codigoCientifico);
	}
	
	public void modificarCientifico(Cientifico miCliente) {
		cientificoServ.validarModificacion(miCliente);
	}
	
	public void eliminarCientifico(String codigo) {
		cientificoServ.validarEliminacion(codigo);
	}
	
	public ArrayList<Cientifico> listarCientificos() {
		CientificoDao miCientificoDao = new CientificoDao();
		return miCientificoDao.listarCientificos();
	}


}
