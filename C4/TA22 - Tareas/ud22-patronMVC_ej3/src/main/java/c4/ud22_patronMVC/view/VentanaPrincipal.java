package c4.ud22_patronMVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import c4.ud22_patronMVC.controller.Asignado_AController;
import c4.ud22_patronMVC.controller.CientificoController;
import c4.ud22_patronMVC.controller.ProyectoController;
import javax.swing.SwingConstants;

public class VentanaPrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	//objeto Asignado_AController, ProyectoController, CientificoController que permite la relacion entre esta clase y las respectivas Controller
	private Asignado_AController asignado_AController; 
	private ProyectoController proyectoController; 
	private CientificoController cientificoController; 
	
	private JLabel labelTitulo;
	private JButton botonRegistrarCientifico, botonBuscarCientifico, botonListarCientifico, btnBuscarProyecto, btnRegistrarProyecto, botonListarProyecto,
					botonRegistrarAsignado_A, botonListarAsignado_A;
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana principal
	 */
	public VentanaPrincipal() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		labelTitulo = new JLabel();
		labelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitulo.setText("UD22 - Ejercicio 3");
		labelTitulo.setBounds(33, 11, 163, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 15));

		
		
		botonRegistrarCientifico = new JButton();
		botonRegistrarCientifico.setBounds(33, 52, 163, 25);
		botonRegistrarCientifico.setText("Registrar Cientifico");
		
		botonBuscarCientifico = new JButton();
		botonBuscarCientifico.setBounds(33, 88, 163, 25);
		botonBuscarCientifico.setText("Buscar Cientifico");
		
		botonListarCientifico = new JButton();
		botonListarCientifico.setBounds(33, 124, 163, 25);
		botonListarCientifico.setText("Listar Cientifico");
		
		
		
		btnRegistrarProyecto = new JButton();
		btnRegistrarProyecto.setText("Registrar Proyecto");
		btnRegistrarProyecto.setBounds(33, 189, 163, 25);
		
		btnBuscarProyecto = new JButton();
		btnBuscarProyecto.setText("Buscar Proyecto");
		btnBuscarProyecto.setBounds(33, 225, 163, 25);
		
		botonListarProyecto = new JButton();
		botonListarProyecto.setBounds(33, 261, 163, 25);
		botonListarProyecto.setText("Listar Proyecto");
		
		
		
		botonRegistrarAsignado_A = new JButton();
		botonRegistrarAsignado_A.setText("Registrar Asignado_A");
		botonRegistrarAsignado_A.setBounds(33, 322, 163, 25);
		
		botonListarAsignado_A = new JButton();
		botonListarAsignado_A.setBounds(33, 358, 163, 25);
		botonListarAsignado_A.setText("Listar Asignado_A");
		
		

		btnRegistrarProyecto.addActionListener(this);
		btnBuscarProyecto.addActionListener(this);
		botonRegistrarCientifico.addActionListener(this);
		botonBuscarCientifico.addActionListener(this);
		botonListarCientifico.addActionListener(this);
		botonListarProyecto.addActionListener(this);
		botonRegistrarAsignado_A.addActionListener(this);
		botonListarAsignado_A.addActionListener(this);
		
		
		
		getContentPane().add(botonListarCientifico);
		getContentPane().add(botonListarProyecto);
		getContentPane().add(btnBuscarProyecto);
		getContentPane().add(btnRegistrarProyecto);
		getContentPane().add(botonBuscarCientifico);
		getContentPane().add(botonRegistrarCientifico);
		getContentPane().add(botonRegistrarAsignado_A);
		getContentPane().add(botonListarAsignado_A);
		getContentPane().add(labelTitulo);

		
		
		setSize(238, 463);
		setTitle("Cientificos y proyectos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
		
	}


	public void setCoordinador(CientificoController cientificoController, ProyectoController proyectoController, Asignado_AController asignado_AController) {
		this.cientificoController=cientificoController;
		this.proyectoController=proyectoController;
		this.asignado_AController=asignado_AController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonRegistrarCientifico) {
			cientificoController.mostrarVentanaRegistroCientifico();			
		}
		if (e.getSource()==botonBuscarCientifico) {
			cientificoController.mostrarVentanaConsultaCientifico();			
		}
		if (e.getSource()==botonListarCientifico) {
			cientificoController.mostrarVentanaListarCientifico();
		}
		if (e.getSource()==btnRegistrarProyecto) {
			proyectoController.mostrarVentanaRegistroProyecto();			
		}
		if (e.getSource()==btnBuscarProyecto) {
			proyectoController.mostrarVentanaConsultaProyecto();			
		}
		if (e.getSource()==botonListarProyecto) {
			proyectoController.mostrarVentanaListarProyecto();
		}
		if (e.getSource()==botonRegistrarAsignado_A) {
			asignado_AController.mostrarVentanaRegistroAsignado_A();			
		}
		if (e.getSource()==botonListarAsignado_A) {
			asignado_AController.mostrarVentanaListarAsignado_A();
		}
		
	}
}
