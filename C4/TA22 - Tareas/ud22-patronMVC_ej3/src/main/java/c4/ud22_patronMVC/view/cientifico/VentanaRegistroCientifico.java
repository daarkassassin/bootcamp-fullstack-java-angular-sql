package c4.ud22_patronMVC.view.cientifico;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import c4.ud22_patronMVC.controller.CientificoController;
import c4.ud22_patronMVC.model.dto.Cientifico;

public class VentanaRegistroCientifico extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private CientificoController cientificoController; 
	
	private JButton botonGuardar, botonCancelar;
	private JLabel nombre, dni;
	private JTextField textNombre, textDni;
	
	public VentanaRegistroCientifico() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(10, 95, 120, 25);
		botonGuardar.setText("Registrar");

		botonCancelar = new JButton();
		botonCancelar.setBounds(140, 95, 120, 25);
		botonCancelar.setText("Cancelar");

		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);
		getContentPane().add(botonCancelar);
		getContentPane().add(botonGuardar);

		nombre = new JLabel();
		nombre.setText("Nombre");
		nombre.setBounds(10, 23, 80, 25);
		getContentPane().add(nombre);

		dni = new JLabel();
		dni.setText("Dni");
		dni.setBounds(10, 59, 80, 25);
		getContentPane().add(dni);

		textNombre = new JTextField();
		textNombre.setText("");
		textNombre.setBounds(70, 23, 190, 25);
		getContentPane().add(textNombre);

		textDni = new JTextField();
		textDni.setText("");
		textDni.setBounds(70, 59, 190, 25);
		getContentPane().add(textDni);
		
		limpiar();
		setSize(304, 190);
		setTitle("Registro de Cientificos");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

	}

	private void limpiar() {
	}

	public void setCoordinador(CientificoController cientificoController) {
		this.cientificoController = cientificoController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == botonGuardar) {
			try {
				Cientifico miCientifico = new Cientifico();
				miCientifico.setNomApels(textNombre.getText());
				miCientifico.setDni(textDni.getText());

				cientificoController.registrarCientifico(miCientifico);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error en el Ingreso de Datos", "Error", JOptionPane.ERROR_MESSAGE);
				System.out.println(ex);
			}
		}
		if (e.getSource() == botonCancelar) {
			this.dispose();
		}
	}

}
