package c4.ud22_patronMVC.AppMain;

import c4.ud22_patronMVC.controller.Asignado_AController;
import c4.ud22_patronMVC.controller.CientificoController;
import c4.ud22_patronMVC.controller.ProyectoController;
import c4.ud22_patronMVC.model.service.Asignado_AServ;
import c4.ud22_patronMVC.model.service.CientificoServ;
import c4.ud22_patronMVC.model.service.ProyectoServ;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.asignado_a.VentanaListarAsignado_A;
import c4.ud22_patronMVC.view.asignado_a.VentanaRegistroAsignado_A;
import c4.ud22_patronMVC.view.cientifico.VentanaBuscarCientifico;
import c4.ud22_patronMVC.view.cientifico.VentanaListarCientifico;
import c4.ud22_patronMVC.view.cientifico.VentanaRegistroCientifico;
import c4.ud22_patronMVC.view.proyecto.VentanaBuscarProyecto;
import c4.ud22_patronMVC.view.proyecto.VentanaListarProyecto;
import c4.ud22_patronMVC.view.proyecto.VentanaRegistroProyecto;

public class mainApp {

	// All Serv
	CientificoServ miCientificoServ;
	ProyectoServ miProyectoServ;
	Asignado_AServ miAsignado_AServ;

	// All Controller
	CientificoController cientificoController;
	ProyectoController proyectoController;
	Asignado_AController asignado_AController;

	// Ventana Principal
	VentanaPrincipal miVentana_Principal;

	// All Ventana Buscar
	VentanaBuscarCientifico miVentanaBuscarCientifico;
	VentanaBuscarProyecto miVentanaBuscarProyecto;

	// All Ventana Registrar
	VentanaRegistroCientifico miVentanaRegistroCientifico;
	VentanaRegistroProyecto miVentanaRegistroProyecto;
	VentanaRegistroAsignado_A miVentanaRegistroAsignado_A;

	// Ventanas Listar
	VentanaListarCientifico miVentanaListarCientifico;
	VentanaListarProyecto miVentanaListarProyecto;
	VentanaListarAsignado_A miVentanaListarAsignado_A;

	public static void main(String[] args) {
		mainApp miPrincipal = new mainApp();
		miPrincipal.iniciar();
	}

	/**
	 * Permite instanciar todas las clases con las que trabaja el sistema
	 */
	private void iniciar() {
		/* Se instancian las clases */
		// All Serv
		miCientificoServ = new CientificoServ();
		miProyectoServ = new ProyectoServ();
		miAsignado_AServ = new Asignado_AServ();

		// All Controller
		cientificoController = new CientificoController();
		proyectoController = new ProyectoController();
		asignado_AController = new Asignado_AController();

		// Ventana Principal
		miVentana_Principal = new VentanaPrincipal();

		// All Ventana Buscar
		miVentanaBuscarCientifico = new VentanaBuscarCientifico();
		miVentanaBuscarProyecto = new VentanaBuscarProyecto();

		// All Ventana Registrar
		miVentanaRegistroCientifico = new VentanaRegistroCientifico();
		miVentanaRegistroProyecto = new VentanaRegistroProyecto();
		miVentanaRegistroAsignado_A = new VentanaRegistroAsignado_A();

		// All Ventanas Listar
		miVentanaListarCientifico = new VentanaListarCientifico();
		miVentanaListarProyecto = new VentanaListarProyecto();
		miVentanaListarAsignado_A = new VentanaListarAsignado_A();

		/* Se establecen las relaciones entre clases */
		// Ventana principal
		miVentana_Principal.setCoordinador(cientificoController, proyectoController, asignado_AController);

		// All Ventana Buscar
		miVentanaBuscarCientifico.setCoordinador(cientificoController);
		miVentanaBuscarProyecto.setCoordinador(proyectoController);

		// All Ventana Registrar
		miVentanaRegistroCientifico.setCoordinador(cientificoController);
		miVentanaRegistroProyecto.setCoordinador(proyectoController);
		miVentanaRegistroAsignado_A.setCoordinador(asignado_AController, cientificoController, proyectoController);

		// All Ventanas Listar
		miVentanaListarCientifico.setCoordinador(cientificoController);
		miVentanaListarProyecto.setCoordinador(proyectoController);
		miVentanaListarAsignado_A.setCoordinador(asignado_AController);

		// All Serv
		miCientificoServ.setController(cientificoController);
		miProyectoServ.setController(proyectoController);
		miAsignado_AServ.setController(asignado_AController);

		/* Se establecen relaciones con la clase coordinador */
		// All Controller Set Ventana Principal
		cientificoController.setMiVentanaPrincipal(miVentana_Principal);
		proyectoController.setMiVentanaPrincipal(miVentana_Principal);
		asignado_AController.setMiVentanaPrincipal(miVentana_Principal);

		// All Controller Set Ventana Registro
		cientificoController.setMiVentanaRegistroCientifico(miVentanaRegistroCientifico);
		proyectoController.setMiVentanaRegistroProyecto(miVentanaRegistroProyecto);
		asignado_AController.setMiVentanaRegistroAsignado_A(miVentanaRegistroAsignado_A);

		// All Controller Set Ventana Buscar
		cientificoController.setMiVentanaBuscarCientifico(miVentanaBuscarCientifico);
		proyectoController.setMiVentanaBuscarProyecto(miVentanaBuscarProyecto);

		// All Controller Set Ventana Listar
		cientificoController.setMiVentanaListarCientifico(miVentanaListarCientifico);
		proyectoController.setMiVentanaListarProyecto(miVentanaListarProyecto);
		asignado_AController.setMiVentanaListarAsignado_A(miVentanaListarAsignado_A);

		// All Controller Set serv
		cientificoController.setCientificoServ(miCientificoServ);
		proyectoController.setProyectoServ(miProyectoServ);
		asignado_AController.setAsignado_AServ(miAsignado_AServ);

		/* Se hace visible la ventana principal */
		miVentana_Principal.setVisible(true);
	}

}
