package c4.ud22_patronMVC.view.cientifico;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import c4.ud22_patronMVC.controller.CientificoController;
import c4.ud22_patronMVC.model.dto.Cientifico;
import c4.ud22_patronMVC.model.service.CientificoServ;

public class VentanaListarCientifico extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private CientificoController cientificoController;

	private JButton botonVolver, botonListarCientificos;

	private JTable lista;
	JScrollPane panelLista;

	public VentanaListarCientifico() {

		botonVolver = new JButton();
		botonVolver.setBounds(352, 422, 120, 25);
		botonVolver.setText("Cancelar");

		botonListarCientificos = new JButton();
		botonListarCientificos.setBounds(10, 422, 120, 25);
		botonListarCientificos.setText("Listar Cientificos");

		lista = new JTable();
		// lista
		lista.setBounds(10, 11, 462, 400);

		panelLista = new JScrollPane(lista);
		panelLista.setBounds(10, 11, 462, 400);

		botonVolver.addActionListener(this);
		botonListarCientificos.addActionListener(this);

		getContentPane().add(botonVolver);
		getContentPane().add(botonListarCientificos);
		// getContentPane().add(lista);
		getContentPane().add(panelLista);

		setSize(500, 500);
		setTitle("Listar Clientes");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

	}

	public void setCoordinador(CientificoController cientificoController) {
		this.cientificoController=cientificoController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonVolver) {
			this.dispose();		
		}
		if (e.getSource() == botonListarCientificos) {
			
			ArrayList<Cientifico> listaClientes = cientificoController.listarCientificos();
			
			if (listaClientes != null) {
				muestraClientes(listaClientes);
			} else if (CientificoServ.consultaCientifico == true) {
				JOptionPane.showMessageDialog(null, "No hay Clientes", "Advertencia", JOptionPane.WARNING_MESSAGE);
			}
		}

	}
	
	private void muestraClientes(ArrayList<Cientifico> misCientificos) {

		// Crear la tabla con los datos del arraylist de clientes
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("dni");
	    model.addColumn("nom_apels");
		
		Iterator <Cientifico> iterator = misCientificos.iterator();
		Cientifico item;
		int contador = 0;
		while (iterator.hasNext()) {
			item = iterator.next();
	    	model.insertRow(contador, new Object[] { item.getDni(), item.getNomApels() });
	        contador++;
	    }
		
	    lista.setModel(model);
	    
	}

}
