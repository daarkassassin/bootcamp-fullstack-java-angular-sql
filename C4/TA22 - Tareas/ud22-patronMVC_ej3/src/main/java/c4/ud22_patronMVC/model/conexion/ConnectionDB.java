package c4.ud22_patronMVC.model.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionDB {
	
    private final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private String hostname;
    private String username;
    private String password;
    private String port;
    private Connection conn;
    
    //METODO CONSTRUCTOR
  	public ConnectionDB() {
  		this.hostname = "192.168.1.200";
  		this.username = "remote";
  		this.password = "Alumne1234@";
  		this.port = "3306";
  		this.conn = Connection_DB();
  	}
    
	//METODO QUE ABRE LA CONEXION CON SERVER MYSQL
    private Connection Connection_DB() {
        Connection connection = null;
        try {
        	String url = "jdbc:mysql://" + hostname + ":" + port + "?useTimezone=true&serverTimezone=UTC";
            Class.forName(DRIVER);
        	connection = DriverManager.getConnection(url, username, password);
            System.out.println("Server Conectado");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error: " + e);
        }
        return connection;
    }
    
    
    //METODO QUE CIERRA LA CONEXION CON SERVER MYSQL
  	public void closeConnection() {
  		try {
  			conn.close();
  			System.out.println("Server Desconectado");
  			
  		} catch (SQLException ex) {
  			System.out.println(ex.getMessage());
  			
  		}
  	}
  	
  	//METODO QUE CREA UNA BASE DE DATOS
  	public void createDB(String name) {
  		try {
  			String Query="CREATE DATABASE "+ name;
  			Statement st= conn.createStatement();
  			st.executeUpdate(Query);
  			System.out.println("DB creada con exito!");
  			
  		}catch(SQLException ex) {
  			System.out.println(ex.getMessage());
  		}	
  	}
  	
  	//METODO QUE EJECUTA UNA QUERY
  	public boolean executeQuery(String db,String query) {
  		try {
  			String Querydb = "USE `"+db+"`;";
  			Statement stdb= conn.createStatement();
  			stdb.executeUpdate(Querydb);
  			Statement st= conn.createStatement();
  			st.executeUpdate(query);
  			System.out.println("Query ejecutada con exito!");
  			return true;
  		}catch (SQLException ex){
  			System.out.println(ex.getMessage());
  			return false;
  		}
  	}
  	
  	//METODO QUE OBTIENE VALORES MYSQL
  	public ResultSet getValues(String db, String query) {
  		try {
  			String Querydb = "USE `"+db+"`;";
  			Statement stdb= conn.createStatement();
  			stdb.executeUpdate(Querydb);
  			Statement st = conn.createStatement();
  			ResultSet ResultSet = st.executeQuery(query);
  			return ResultSet;
  		} catch (SQLException ex) {
  			System.out.println(ex.getMessage());
  			return null;
  		}
  	
  	}
 
  	
}
