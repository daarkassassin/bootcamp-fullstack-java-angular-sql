package c4.ud22_patronMVC.model.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import c4.ud22_patronMVC.model.conexion.ConnectionDB;
import c4.ud22_patronMVC.model.dto.Proyecto;

public class ProyectoDao {

	public void registrarProyecto(Proyecto proyecto) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			// Ejecutar Query que insrta el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "insert into Proyecto(id_proyecto, nombre, horas) values ('" + proyecto.getId() + "', '" + proyecto.getNombre() + "', '" + proyecto.getHoras() + "');");

			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
				
			// Cerrar conexion
			conex.closeConnection();
			
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente", "Información",JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se ha registrado", "Información", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public ArrayList<Proyecto> listarProyecto() {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			ArrayList<Proyecto> listaProyectos = new ArrayList<Proyecto>();
			

			// Ejecutar Query que obtiene el proyecto, guardar los datos en el resultSet i
			// guardarlos despues en el objeto proyecto
			
			ResultSet res = conex.getValues("UD22-E3", "select * from Proyecto;");
			
			// Guardar los datos en el objeto proyecto
			while (res.next()) {
				existe = true;
				Proyecto proyecto = new Proyecto();
				proyecto.setId(res.getString("id_proyecto"));
				proyecto.setNombre(res.getString("nombre"));
				proyecto.setHoras(Integer.parseInt(res.getString("horas")));
				listaProyectos.add(proyecto);
			}
			// Cerrar conexion
			conex.closeConnection();
			
			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return listaProyectos;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public Proyecto buscarProyecto(String codigo) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			Proyecto proyecto = new Proyecto();

			// Ejecutar Query que obtiene el proyecto, guardar los datos en el resultSet i
			// guardarlos despues en el objeto proyecto
			ResultSet res = conex.getValues("UD22-E3", "SELECT * FROM Proyecto where id_proyecto = '" + codigo + "';");
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				proyecto.setId(res.getString("id_proyecto"));
				proyecto.setNombre(res.getString("nombre"));
				proyecto.setHoras(Integer.parseInt(res.getString("horas")));
			}
			// Cerrar conexion
			conex.closeConnection();

			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return proyecto;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}

	public void modificarProyecto(Proyecto proyecto) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;
			
			// Ejecutar Query que modifica el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "update Proyecto set nombre = '" + proyecto.getNombre() + "', horas = '" + proyecto.getHoras() + "' where id_proyecto = '" + proyecto.getId() + "';");

			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
				
			// Cerrar conexion
			conex.closeConnection();
			
			JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ", "Confirmación",JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Error al Modificar", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void eliminarProyecto(String codigo) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			// Ejecutar Query que modifica el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "DELETE FROM Proyecto WHERE id_proyecto='" + codigo + "'");
			
			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
			
			// Cerrar conexion
			conex.closeConnection();
			
            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
            
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}

}
