package c4.ud22_patronMVC.AppMain;

import c4.ud22_patronMVC.controller.ClienteController;
import c4.ud22_patronMVC.model.service.ClienteServ;
import c4.ud22_patronMVC.view.VentanaBuscar;
import c4.ud22_patronMVC.view.VentanaPrincipal;
import c4.ud22_patronMVC.view.VentanaRegistro;
import c4.ud22_patronMVC.view.VentanaListar;

public class mainApp {
	
	ClienteServ miclienteServ;
	VentanaPrincipal miVentanaPrincipal;
	VentanaBuscar miVentanaBuscar;
	VentanaRegistro miVentanaRegistro;
	ClienteController clienteController;
	
	//Ventanas Listar
	VentanaListar miVentanaListar;

	public static void main(String[] args) {
		mainApp miPrincipal=new mainApp();
		miPrincipal.iniciar();
	}

	/**
	 * Permite instanciar todas las clases con las que trabaja
	 * el sistema
	 */
	private void iniciar() {
		/*Se instancian las clases*/
		miVentanaPrincipal=new VentanaPrincipal();
		miVentanaRegistro=new VentanaRegistro();
		miVentanaBuscar= new VentanaBuscar();
		miclienteServ=new ClienteServ();
		clienteController= new ClienteController();
		
		//Ventana Listar
		miVentanaListar=new VentanaListar();
		
		
		
		/*Se establecen las relaciones entre clases*/
		//Ventana principal
		miVentanaPrincipal.setCoordinador(clienteController);
		
		//Cliente
		miVentanaRegistro.setCoordinador(clienteController);
		miVentanaBuscar.setCoordinador(clienteController);
		miVentanaListar.setCoordinador(clienteController);
		
		miclienteServ.setpersonaController(clienteController);
		
				/*Se establecen relaciones con la clase coordinador*/
		//Cliente
		clienteController.setMiVentanaPrincipal(miVentanaPrincipal);
		clienteController.setMiVentanaRegistro(miVentanaRegistro);
		clienteController.setMiVentanaBuscar(miVentanaBuscar);
		clienteController.setMiVentanaListar(miVentanaListar);
		
		clienteController.setClienteServ(miclienteServ);
		
		/*Se hace visible la ventana principal*/
		miVentanaPrincipal.setVisible(true);
	}

}
